﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(UIMeshButton))]
public class TabSwitcher : MonoBehaviour {

    public Color Normal;
    public Color Down;

    private UIMesh m_UIMesh;

    private void OnTapDownDelegate()
    {
        // m_UIMesh.ChangeColor(Down);
    }

    private void OnTapUpDelegate()
    {
        // m_UIMesh.ChangeColor(Normal);
    }

    private void OnTapUpAsButtonDelegate()
    {
        // m_UIMesh.ChangeColor(Normal);
    }

    void Awake()
    {
        m_UIMesh = GetComponent<UIMesh>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

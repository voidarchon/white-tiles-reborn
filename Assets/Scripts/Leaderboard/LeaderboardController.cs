﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LeaderboardController : MonoBehaviour {

    public GameObject PrefabRecordItem;

    private UIKineticListView2 m_UIKineticListView2;

    private float m_PixelSize;

    private void Initialize()
    {
        m_PixelSize = Camera.main.orthographicSize * 2.0f / Camera.main.pixelHeight;
        m_UIKineticListView2.TopMargin = 2 * m_PixelSize;
        m_UIKineticListView2.BotMargin = 2 * m_PixelSize;
        m_UIKineticListView2.Spacing = m_PixelSize;
        m_UIKineticListView2.ItemHeight = 0.9f;
    }

    private delegate void DoneDownloadAvatar(GameObject recordItem, Texture tex);

//     private void OnDoneDownload(GameObject recordItem, Texture tex)
//     {
//         recordItem.GetComponent<RecordItem>().AvatarGO
//             .GetComponent<MeshRenderer>().material.SetTexture("_MainTex", tex);
//     }

//     private IEnumerator DownloadAvatar(GameObject recordItem, string url)
//     {
//         WWW www = new WWW(url);
//         yield return www;
//         if (www.texture.width >= 150)
//             OnDoneDownload(recordItem, www.texture);
//         yield return null;
//     }

    public void AddRecordItem(int no, string avatarURL, string name, string scoreStr, bool highlight = false)
    {
        GameObject recordItem = Instantiate(PrefabRecordItem) as GameObject;
        if (highlight)
            recordItem.GetComponent<RecordItem>().Highlight(Color.cyan);
        recordItem.GetComponent<RecordItem>().SetInformation(no, avatarURL, name, scoreStr);
        m_UIKineticListView2.AddItem(recordItem);
        // StartCoroutine(DownloadAvatar(recordItem, avatarURL));
    }

    public void ShowLeaderboard()
    {
        m_UIKineticListView2.Show();
        // m_UIKineticListView.ListContainerGO.GetComponent<Rigidbody2D>().isKinematic = false;
    }

    void Awake()
    {
        m_UIKineticListView2 = GetComponent<UIKineticListView2>();
    }

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

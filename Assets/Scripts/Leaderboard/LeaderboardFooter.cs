﻿using UnityEngine;
using System.Collections;

public class LeaderboardFooter : MonoBehaviour {

    public GameObject AvatarGO;
    public GameObject NotifyTextGO;
    public GameObject BtnBackGO;

    private void OnDoneDownload(Texture tex)
    {
        AvatarGO.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", tex);
    }

    private IEnumerator DownloadAvatar(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.texture.width >= 150)
            OnDoneDownload(www.texture);
        yield return null;
    }

    public void SetInfo(string avatarURL, int rank)
    {
        if (string.IsNullOrEmpty(avatarURL))
            NotifyTextGO.GetComponent<TextMesh>().text = "FB not connected";
        else
        {
            StartCoroutine(DownloadAvatar(avatarURL));
            if (rank > 0)
                NotifyTextGO.GetComponent<TextMesh>().text = "You rank #" + rank.ToString();
            else NotifyTextGO.GetComponent<TextMesh>().text = "No highscore";
        }
    }

    private void Initialize()
    {
        float width = Camera.main.orthographicSize * 2.0f * Camera.main.aspect;
        float height = 0.9f;
        float pixelSize = width / Camera.main.pixelWidth;

        GetComponent<UIMesh>().Width = width;
        GetComponent<UIMesh>().Height = height;
        GetComponent<UIMesh>().GenerateMesh();
        transform.position = new Vector3(0.0f, - Camera.main.orthographicSize + height / 2.0f, -1.0f);
        AvatarGO.transform.localPosition = new Vector3(-(-width / 2.0f + 10 * pixelSize + 0.45f), 0.0f, -1.1f);
        NotifyTextGO.transform.localPosition = new Vector3(-(-width / 2.0f + 20 * pixelSize + 0.9f), 0.0f, -1.1f);
        BtnBackGO.transform.localPosition = new Vector3(-(width / 2.0f - 0.85f), 0.0f, -1.1f);
    }

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

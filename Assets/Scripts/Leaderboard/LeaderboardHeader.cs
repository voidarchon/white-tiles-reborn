﻿using UnityEngine;
using System.Collections;

public class LeaderboardHeader : MonoBehaviour {

    public GameObject LeaderboardIconGO;
    public GameObject BtnRecentGO;
    public GameObject BtnAlltimeGO;
    public GameObject LoadingIndicatorGO;

    private void Initialize()
    {
        float width = Camera.main.orthographicSize * 2.0f * Camera.main.aspect;
        float height = 0.9f;
        float pixelSize = width / Camera.main.pixelWidth;
        float mostLeft = - width / 2.0f;

        transform.position = new Vector3(0.0f, Camera.main.orthographicSize - height / 2.0f, -1.0f);

        // Resize the buttons
        float btnWidth = (width - 3 * pixelSize) / 2.0f;
        float btnHeight = 0.6f;
        BtnRecentGO.GetComponent<UIMesh>().Width = btnWidth;
        // BtnRecentGO.GetComponent<UIMesh>().Height = btnHeight;
        BtnRecentGO.GetComponent<UIMesh>().GenerateMesh();
        BtnRecentGO.GetComponent<UITappable>().SetTapRegion(new Vector2(btnWidth, btnHeight));

        BtnAlltimeGO.GetComponent<UIMesh>().Width = btnWidth;
        // BtnAlltimeGO.GetComponent<UIMesh>().Height = btnHeight;
        BtnAlltimeGO.GetComponent<UIMesh>().GenerateMesh();
        BtnAlltimeGO.GetComponent<UITappable>().SetTapRegion(new Vector2(btnWidth, btnHeight));
        
        // reposition the buttons
        Vector3 pos = BtnRecentGO.transform.localPosition;
        pos.x = mostLeft + pixelSize + btnWidth / 2.0f;
        BtnRecentGO.transform.localPosition = pos;

        pos = BtnAlltimeGO.transform.localPosition;
        pos.x = mostLeft + pixelSize + btnWidth / 2.0f + btnWidth + pixelSize;
        BtnAlltimeGO.transform.localPosition = pos;

        // LoadingIndicatorGO.transform.localPosition = new Vector3(width / 2.0f - 2.0f - 0.5f, 0.0f, -1.1f);
        LoadingIndicatorGO.transform.position = Vector3.zero;
    }

    public void ShowLoadingIndicator()
    {
        LoadingIndicatorGO.SetActive(true);
    }

    public void HideLoadingIndicator()
    {
        LoadingIndicatorGO.SetActive(false);
    }

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

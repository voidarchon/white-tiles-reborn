﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class LeaderboardLogic : MonoBehaviour {

    public LeaderboardController LeaderboardListViewGO;
    public LeaderboardController LeaderboardAlltimeGO;
    public LeaderboardHeader LeaderboardHeaderGO;
    public LeaderboardFooter LeaderboardFooterGO;
    public GameObject BtnRecentGO;
    public GameObject BtnAlltimeGO;
    public MessageBox MessageBoxGO;
    public SpriteRenderer LBIConGO;

    [Header("LB Information")]
    public string RecentBoardName;
    public string AlltimeBoardName;
    public string GameMode;
    public string AppId;
    public enum ScoreType { ScoreType_Time, ScoreType_Point };
    public ScoreType ScoreValueType;

    [Header("Header LB Icons")]
    public Sprite LBClassic;
    public Sprite LBArcade;
    public Sprite LBBlink;
    public Sprite LBFreeze;
    public Sprite LBZen;
    public Sprite LBFlip;
    public Sprite LBDual;
    public Sprite LBMirror;

    [Header("demo only")]
    public GameObject SwitchBgGO;
    public GameObject SwitchTextGO;
    private bool m_IsRecent;
    private bool m_DoneSlide;

    private string m_PlayerAvatarURL;
    private int m_PlayerRecentRank;
    private int m_PlayerAlltimeRank;

    private LBFacebookInfo m_FBInfo;

    private void OnBackClicked()
    {
        Application.LoadLevel("_MainMenu_");
    }

    #region switch between 2 score boards
    private IEnumerator SlideLeftRoutine()
    {
        m_DoneSlide = false;
        float destX = -8.0f;
        float speed = 25.0f;
        while (LeaderboardListViewGO.transform.position.x != destX)
        {
            Vector3 tslVct = new Vector3(-speed * Time.deltaTime, 0.0f, 0.0f);
            if (tslVct.x + LeaderboardListViewGO.transform.position.x < -8.0f)
                tslVct.x = -8.0f - LeaderboardListViewGO.transform.position.x;
            LeaderboardListViewGO.transform.Translate(tslVct);
            LeaderboardAlltimeGO.transform.Translate(tslVct);
            yield return null;
        }
        m_DoneSlide = true;
        LeaderboardAlltimeGO.GetComponent<UIKineticListView2>().SetEnable(true);
        LeaderboardFooterGO.SetInfo(m_PlayerAvatarURL, m_PlayerAlltimeRank);
        yield return null;
    }
    private IEnumerator SlideRightRoutine()
    {
        m_DoneSlide = false;
        float destX = 0.0f;
        float speed = 25.0f;
        while (LeaderboardListViewGO.transform.position.x != destX)
        {
            Vector3 tslVct = new Vector3(speed * Time.deltaTime, 0.0f, 0.0f);
            if (tslVct.x + LeaderboardListViewGO.transform.position.x > 0.0f)
                tslVct.x = 0.0f - LeaderboardListViewGO.transform.position.x;
            LeaderboardListViewGO.transform.Translate(tslVct);
            LeaderboardAlltimeGO.transform.Translate(tslVct);
            yield return null;
        }
        m_DoneSlide = true;
        LeaderboardListViewGO.GetComponent<UIKineticListView2>().SetEnable(true);
        LeaderboardFooterGO.SetInfo(m_PlayerAvatarURL, m_PlayerRecentRank);
        yield return null;
    }

    private void OnRecentClicked()
    {
        if (!m_DoneSlide)
            return;
        if (!m_IsRecent)
        {
            SwitchBgGO.GetComponent<UIMesh>().ChangeColor(new Color(0.188f, 0.565f, 1.0f, 1.0f));
            SwitchTextGO.GetComponent<UILabel>().SetText("RECENT");
            LeaderboardAlltimeGO.GetComponent<UIKineticListView2>().SetEnable(false);
            StartCoroutine(SlideRightRoutine());
            m_IsRecent = true;
            BtnRecentGO.GetComponent<UIMesh>().ChangeColor(BtnRecentGO.GetComponent<TabSwitcher>().Down);
            BtnAlltimeGO.GetComponent<UIMesh>().ChangeColor(BtnAlltimeGO.GetComponent<TabSwitcher>().Normal);
        }
    }

    private void OnAlltimeClicked()
    {
        if (!m_DoneSlide)
            return;
        if (m_IsRecent)
        {
            SwitchBgGO.GetComponent<UIMesh>().ChangeColor(new Color(1.0f, 0.208f, 0.486f, 1.0f));
            SwitchTextGO.GetComponent<UILabel>().SetText("ALLTIME");
            LeaderboardListViewGO.GetComponent<UIKineticListView2>().SetEnable(false);
            StartCoroutine(SlideLeftRoutine());
            m_IsRecent = false;
            BtnRecentGO.GetComponent<UIMesh>().ChangeColor(BtnRecentGO.GetComponent<TabSwitcher>().Normal);
            BtnAlltimeGO.GetComponent<UIMesh>().ChangeColor(BtnAlltimeGO.GetComponent<TabSwitcher>().Down);
        }
    }
    /*
    private void OnSwitchClicked()
    {
        if (!m_DoneSlide)
            return;
        if (m_IsRecent)
        {
            SwitchBgGO.GetComponent<UIMesh>().ChangeColor(new Color(1.0f, 0.208f, 0.486f, 1.0f));
            SwitchTextGO.GetComponent<UILabel>().SetText("ALLTIME");
            LeaderboardListViewGO.GetComponent<UIKineticListView2>().SetEnable(false);
            StartCoroutine(SlideLeftRoutine());
            m_IsRecent = false;
        }
        else
        {
            SwitchBgGO.GetComponent<UIMesh>().ChangeColor(new Color(0.188f, 0.565f, 1.0f, 1.0f));
            SwitchTextGO.GetComponent<UILabel>().SetText("RECENT");
            LeaderboardAlltimeGO.GetComponent<UIKineticListView2>().SetEnable(false);
            StartCoroutine(SlideRightRoutine());
            m_IsRecent = true;
        }
    }
    */
    #endregion

    #region get newest score info from score server (auto check fb info)
    private void DoneRecentRequest(string returnJson, bool error, string errorName)
    {
        if (!error)
        {
            Debug.Log("[Get RecentScore] " + returnJson);
            Dictionary<string, object> startStr = MiniJSON.Json.Deserialize(returnJson) as Dictionary<string, object>;
            List<object> users = startStr["users"] as List<object>;
            Dictionary<string, object> aboutMe = startStr["me"] as Dictionary<string, object>;
            foreach (object obj in users)
            {
                Dictionary<string, object> user = obj as Dictionary<string, object>;
                int rank = int.Parse(user["rank"].ToString()) + 1;
                int score = int.Parse(user["score"].ToString());
                string scoreStr = "";
                if (ScoreValueType == ScoreType.ScoreType_Time)
                    scoreStr = string.Format("{0:0.0000}\"", (float)(-score) / 10000.0f);
                else scoreStr = score.ToString();
                string name = (string)user["name"];
                string avatarURL = "https://graph.facebook.com/" + user["fid"] + "/picture?width=150&height=150";
                LeaderboardListViewGO.AddRecordItem(rank, avatarURL, name, scoreStr);
            }
            LeaderboardHeaderGO.HideLoadingIndicator();
            LeaderboardListViewGO.ShowLeaderboard();

            // m_PlayerRecentRank
            if (aboutMe != null)
                m_PlayerRecentRank = int.Parse(aboutMe["rank"].ToString()) + 1;
            else m_PlayerRecentRank = -1;
            LeaderboardFooterGO.SetInfo(m_PlayerAvatarURL, m_PlayerRecentRank);
        }
        else
        {
            Debug.Log("[Get RecentScore] " + errorName);
        }
    }
    private void DoneAlltimeRequest(string returnJson, bool error, string errorName)
    {
        if (!error)
        {
            Debug.Log("[Get AlltimeScore] " + returnJson);
            Dictionary<string, object> startStr = MiniJSON.Json.Deserialize(returnJson) as Dictionary<string, object>;
            List<object> users = startStr["users"] as List<object>;
            Dictionary<string, object> aboutMe = startStr["me"] as Dictionary<string, object>;
            foreach (object obj in users)
            {
                Dictionary<string, object> user = obj as Dictionary<string, object>;
                int rank = int.Parse(user["rank"].ToString()) + 1;
                int score = int.Parse(user["score"].ToString());
                string scoreStr = "";
                if (ScoreValueType == ScoreType.ScoreType_Time)
                    scoreStr = string.Format("{0:0.0000}\"", (float)(-score) / 10000.0f);
                else scoreStr = score.ToString();
                string name = (string)user["name"];
                string avatarURL = "https://graph.facebook.com/" + user["fid"] + "/picture?width=150&height=150";
                LeaderboardAlltimeGO.AddRecordItem(rank, avatarURL, name, scoreStr);
            }
            LeaderboardAlltimeGO.ShowLeaderboard();
            LeaderboardAlltimeGO.GetComponent<UIKineticListView2>().SetEnable(false);

            // m_PlayerAlltimeRank
            if (aboutMe != null)
                m_PlayerAlltimeRank = int.Parse(aboutMe["rank"].ToString()) + 1;
            else m_PlayerAlltimeRank = -1;

        }
        else
        {
            Debug.Log("[Get AlltimeScore] " + errorName);
        }
    }

    private void RequestRecentScores()
    {
        if (string.IsNullOrEmpty(m_FBInfo.Id))
        {
            ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score/topscore",
                LBJsonStringBuilder.Instance.BuildGetScoreStr("android", 
                "-1", 
                RecentBoardName, 
                "-1", 
                AppId, 30),
                DoneRecentRequest);
            m_PlayerAvatarURL = "";
            m_PlayerRecentRank = 0;
        }
        else
        {
            ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score/topscore",
                LBJsonStringBuilder.Instance.BuildGetScoreStr("android", 
                SystemInfo.deviceUniqueIdentifier, 
                RecentBoardName, 
                m_FBInfo.Id, 
                AppId, 
                30),
                DoneRecentRequest);
            m_PlayerAvatarURL = "https://graph.facebook.com/" + m_FBInfo.Id + "/picture?width=150&height=150";
        }
    }
    private void RequestAlltimeScores()
    {
        if (string.IsNullOrEmpty(m_FBInfo.Id))
        {
            ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score/topscore",
                LBJsonStringBuilder.Instance.BuildGetScoreStr("android", 
                "-1", 
                AlltimeBoardName, 
                "-1", 
                AppId, 30),
                DoneAlltimeRequest);
            m_PlayerAvatarURL = "";
            m_PlayerAlltimeRank = 0;
        }
        else
        {
            ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score/topscore",
                LBJsonStringBuilder.Instance.BuildGetScoreStr("android", 
                SystemInfo.deviceUniqueIdentifier, 
                AlltimeBoardName, 
                m_FBInfo.Id, 
                AppId, 30),
                DoneAlltimeRequest);
            m_PlayerAvatarURL = "https://graph.facebook.com/" + m_FBInfo.Id + "/picture?width=150&height=150";
        }
    }
    #endregion

    #region update score to score server
    private void DoneUpdatingAlltimeScore(string returnJson, bool error, string errorName)
    {
        if (!error)
        {
            Debug.Log("[Update Alltime Score] " + returnJson);
            RequestAlltimeScores();
        }
        else
        {
            Debug.Log("[Update Alltime Score] " + errorName);
        }
    }

    private void DoneUpdatingRecentScore(string returnJson, bool error, string errorName)
    {
        if (!error)
        {
            Debug.Log("[Update Recent Score] " + returnJson);
            RequestRecentScores();
        }
        else
        {
            Debug.Log("[Update Recent Score] " + errorName);
        }
    }

    private void RequestUpdateScoreAlltime()
    {
        int score = HighscoreServ.Instance.GetAlltimeScore(GameMode);
        if (ScoreValueType == ScoreType.ScoreType_Time)
        {
            if (score != 0)
                score = -score;
            else
            {
                RequestAlltimeScores();
                return;
            }
        }
        ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score",
            LBJsonStringBuilder.Instance.BuildPostScoreStr("android", AppId,
            SystemInfo.deviceUniqueIdentifier, AlltimeBoardName, -1,
            m_FBInfo.Id, score), DoneUpdatingAlltimeScore);
    }

    private void RequestUpdateScoreRecent()
    {
        int score = HighscoreServ.Instance.GetRecentScore(GameMode + "_recent");
        if (ScoreValueType == ScoreType.ScoreType_Time)
        {
            if (score != 0)
                score = -score;
            else
            {
                RequestRecentScores();
                return;
            }
        }
        ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score",
            LBJsonStringBuilder.Instance.BuildPostScoreStr("android", AppId,
            SystemInfo.deviceUniqueIdentifier, RecentBoardName, 86400000,
            m_FBInfo.Id, score), DoneUpdatingRecentScore);
    }
    #endregion

    #region sign in to score server
    private void DoneServerSignIn(string returnJson, bool error, string errorName)
    {
        if (!error)
        {
            Debug.Log("[Server SignIn] " + returnJson);
            RequestUpdateScoreAlltime();
            RequestUpdateScoreRecent();
        }
        else
        {
            Debug.Log("[Server SignIn] " + errorName);
        }
    }

    private void RequestServerSignIn()
    {
        ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/account/facebook/signin",
            LBJsonStringBuilder.Instance.BuildSignInStr("android", 
            AppId, 
            SystemInfo.deviceUniqueIdentifier,
            m_FBInfo), DoneServerSignIn);
    }
    #endregion

    private void OnDoneGettingFBInfo(LBFacebookInfo info)
    {
//         m_FBInfo = new LBFacebookInfo("Nhật",
//             "Hồng",
//             "443683145734781",
//             "male",
//             "https://www.facebook.com/app_scoped_user_id/443683145734781/",
//             "Hồng Nhật");
//         RequestServerSignIn();

        m_FBInfo = info;
        Debug.Log("Done getting Facebook info");
        Debug.Log("fid= " + m_FBInfo.Id);
        RequestServerSignIn();
    }

    private void OnDoneLoginDelegate(FacebookServ.LoginResult result)
    {
        if (result == FacebookServ.LoginResult.Login_Cancel
            || result == FacebookServ.LoginResult.Login_Fail)
        {
            // request score no facebook id
            m_FBInfo = new LBFacebookInfo("", "", "", "", "", "");
            RequestRecentScores();
            RequestAlltimeScores();
        }
        else
        {
            FacebookServ.Instance.GetInformation(OnDoneGettingFBInfo);
            // TEST: get information success
            // OnDoneGettingFBInfo(null);
        }
    }

    private void OnConnectFBYes()
    {
        LeaderboardHeaderGO.ShowLoadingIndicator();
        
        FacebookServ.Instance.LogIn(OnDoneLoginDelegate);
        // TEST: login success
        // OnDoneLoginDelegate(FacebookServ.LoginResult.Login_Success);
    }
    private void OnConnectFBNo()
    {
        // OnBackClicked();
        m_FBInfo = new LBFacebookInfo("", "", "", "", "", "");
        RequestRecentScores();
        RequestAlltimeScores();
    }

    private void NoInternetYes()
    {
        OnBackClicked();
    }

    private void OnInternetFailed()
    {
        // message box show
        Debug.Log("no internet");
        MessageBoxGO.gameObject.SetActive(true);
        MessageBoxGO.OnResultYesDelegate = NoInternetYes;
        MessageBoxGO.OnResultNoDelegate = null;
        MessageBoxGO.Show("No internet connection. \nBack to Main Menu?");
    }
    private void OnInternetAvailable()
    {
        Debug.Log("internet available");
        if (!FacebookServ.Instance.IsLoggedIn())
        {
            LeaderboardHeaderGO.HideLoadingIndicator();
            MessageBoxGO.gameObject.SetActive(true);
            MessageBoxGO.OnResultYesDelegate = OnConnectFBYes;
            MessageBoxGO.OnResultNoDelegate = OnConnectFBNo;
            MessageBoxGO.Show("Connect to Facebook?");
        }
        else
        {
            // get information + callback
            OnDoneLoginDelegate(FacebookServ.LoginResult.Login_Success);
        }
    }
    private IEnumerator InternetCheckingRoutine()
    {
        WWW www;
        www = new WWW("http://www.google.com");
        yield return www;
        Debug.Log(www.text);

        if (!string.IsNullOrEmpty(www.error))
        {
            OnInternetFailed();
        }
        else
        {
            OnInternetAvailable();
        }
    }

    private void SetModeInfo(string gameMode)
    {
        if (string.Compare(gameMode, "arcade4k") == 0
            || string.Compare(gameMode, "arcade5k") == 0
            || string.Compare(gameMode, "arcade6k") == 0
            || string.Compare(gameMode, "arcade8k") == 0)
        {
            LBIConGO.sprite = LBArcade;
        }
        else if (string.Compare(gameMode, "flip4") == 0
            || string.Compare(gameMode, "flip4master") == 0
            || string.Compare(gameMode, "flip6") == 0
            || string.Compare(gameMode, "flip6master") == 0)
        {
            // TODO: change this
            LBIConGO.sprite = LBFlip;
        }
        else if (string.Compare(gameMode, "zen7") == 0
            || string.Compare(gameMode, "zen9") == 0
            || string.Compare(gameMode, "zen12") == 0
            || string.Compare(gameMode, "zen15") == 0)
        {
            // TODO: change this
            LBIConGO.sprite = LBZen;
        }
        else if (string.Compare(gameMode, "classic20") == 0
            || string.Compare(gameMode, "classic35") == 0
            || string.Compare(gameMode, "classic50") == 0
            || string.Compare(gameMode, "classic75") == 0)
        {
            LBIConGO.sprite = LBClassic;
        }
        else if ((string.Compare(gameMode, "blink") == 0))
        {
            LBIConGO.sprite = LBBlink;
        }
        else if ((string.Compare(gameMode, "freeze") == 0))
        {
            LBIConGO.sprite = LBFreeze;
        }
        else if (string.Compare(gameMode, "dual") == 0)
        {
            // TODO: change this
            LBIConGO.sprite = LBDual;
        }
        else if (string.Compare(gameMode, "mirror") == 0)
        {
            // TODO: change this
            LBIConGO.sprite = LBMirror;
        }
    }

    private void Initialize()
    {
        RecentBoardName = PlayerPrefs.GetString("ToLB_RecentBoardName");
        Debug.Log(RecentBoardName);
        AlltimeBoardName = PlayerPrefs.GetString("ToLB_AlltimeBoardName");
        Debug.Log(AlltimeBoardName);
        GameMode = PlayerPrefs.GetString("ToLB_GameMode");
        Debug.Log(GameMode);

        AnalyticsServ.LogScreen("Leaderboard " + GameMode);

        SetModeInfo(GameMode);

        if (string.Compare(PlayerPrefs.GetString("ToLB_ScoreType"), "time") == 0)
        {
            ScoreValueType = ScoreType.ScoreType_Time;
            Debug.Log("LB: ScoreType_Time");
        }
        else
        {
            ScoreValueType = ScoreType.ScoreType_Point;
            Debug.Log("LB: ScoreType_Point");
        }
        BtnRecentGO.GetComponent<UIMesh>().ChangeColor(BtnRecentGO.GetComponent<TabSwitcher>().Down);
        BtnAlltimeGO.GetComponent<UIMesh>().ChangeColor(BtnAlltimeGO.GetComponent<TabSwitcher>().Normal);
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        LeaderboardHeaderGO.ShowLoadingIndicator();
        Initialize();
        m_IsRecent = true;
        m_DoneSlide = true;
        StartCoroutine(InternetCheckingRoutine());
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnBackClicked();
	}
}
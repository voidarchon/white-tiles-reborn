﻿using UnityEngine;
using System.Collections;

public class RecordItem : MonoBehaviour {

    public GameObject NoGO;
    public GameObject NoTextGO;
    public SpriteRenderer NoBgGO;
    // public GameObject AvatarGO;
    public GameObject NameGO;
    public GameObject ScoreStrGO;

    public Sprite[] TopBgs;
    public Sprite NormalBg;

    private void Initialize()
    {
        float width = Camera.main.orthographicSize * 2.0f * Camera.main.aspect;
        float height = 0.9f;
        float pixelSize = width / Camera.main.pixelWidth;

        NoGO.transform.localPosition = new Vector3(-width / 2.0f + 0.4f, 0.0f, -0.1f);
        // AvatarGO.transform.localPosition = new Vector3(-width / 2.0f + 1.2f + 2 * pixelSize, 0.0f, -0.1f);
        NameGO.transform.localPosition = new Vector3(-width / 2.0f + 1.7f + 4 * pixelSize - 0.6f, 0.0f, -0.1f);
        ScoreStrGO.transform.localPosition = new Vector3(width / 2.0f - 4 * pixelSize, 0.0f, -0.3f);
       //  ScoreStrBgGO.transform.localPosition = new Vector3(width / 2.0f - 1.0f, 0.0f, -0.2f);
    }

    public void Highlight(Color color)
    {
    }

    public void SetInformation(int no, string avatarURL, string name, string scoreStr)
    {
        NoTextGO.GetComponent<UILabel>().SetText(no.ToString());
        if (no <= 3)
            NoBgGO.sprite = TopBgs[no - 1];
        else
            NoBgGO.sprite = NormalBg;
        if (name.Length > 12)
            name = name.Substring(0, 12) + "...";
        NameGO.GetComponent<UILabel>().SetText(name);
        ScoreStrGO.GetComponent<UILabel>().SetText(scoreStr);
    }

	// Use this for initialization
	void Start () {
        Initialize();
        // SetInformation(2, "", "abc", "123");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

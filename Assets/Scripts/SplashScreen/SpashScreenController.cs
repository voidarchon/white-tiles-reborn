﻿using UnityEngine;
using System.Collections;

public class SpashScreenController : MonoBehaviour {

    public GameObject SplashImageGO;
	public float Delay;

    private void ActiveSplashImage()
    {
        SplashImageGO.SetActive(true);
    }

	// Use this for initialization
	void Start () {
        Screen.fullScreen = true;
        Application.targetFrameRate = 60;
        PlayerPrefs.SetInt("game_is_loaded", 0);
        Invoke("ActiveSplashImage", Delay);
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
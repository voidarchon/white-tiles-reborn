﻿using UnityEngine;
using System.Collections;

public class SettingsDialogController : MonoBehaviour {

    [Header("Dialog object references")]
    public UISwitch SoundSwitchGO;
    public UISwitch FBConnectSwitchGO;
    public UILabel FBNameGO;
    public GameObject FBImageGO;
    public Texture DefaultAvatar;

    private bool m_SoundSetting;

    private void OnToggleSoundOff()
    {
        Debug.Log("sound off");
        PlayerPrefs.SetInt("game_settings_sound", 0);
        GameSettingsManager.Instance.ChangeBoolSetting("sound", false);
    }

    private void OnToggleSoundOn()
    {
        Debug.Log("sound on");
        PlayerPrefs.SetInt("game_settings_sound", 1);
        GameSettingsManager.Instance.ChangeBoolSetting("sound", true);
    }

    private void OnDoneLogin(FacebookServ.LoginResult result)
    {
        if (result == FacebookServ.LoginResult.Login_Success)
            FacebookServ.Instance.GetInformation(OnDoneGettingInfo);
        else
        {
            FBConnectSwitchGO.SetSwitchState(UISwitch.SwitchState.Switch_Off);
            FBNameGO.SetText("not connected");
        }
    }

    private void OnToggleFacebookLogIn()
    {
        Debug.Log("log in");
        FacebookServ.Instance.LogIn(OnDoneLogin);
    }

    private void OnToggleFacebookLogOut()
    {
        Debug.Log("log out");
        FacebookServ.Instance.LogOut();
        FBImageGO.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", DefaultAvatar);
        FBNameGO.SetText("not connected");
    }

    private IEnumerator DownloadAvatarRoutine(GameObject imageGO, string url)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.texture.width >= 150)
            imageGO.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", www.texture);
        yield return null;
    }

    private void OnDoneGettingInfo(LBFacebookInfo info)
    {
        FBNameGO.SetText(info.FirstName);
        StartCoroutine(DownloadAvatarRoutine(FBImageGO,
            "https://graph.facebook.com/" + info.Id + "/picture?width=150&height=150"));
    }

    private void Initialize()
    {
        // get sound settings
        m_SoundSetting = PlayerPrefs.GetInt("game_settings_sound") == 1 ? true : false;
        if (m_SoundSetting)
        {
            SoundSwitchGO.SetSwitchState(UISwitch.SwitchState.Switch_On);
            PlayerPrefs.SetInt("game_settings_sound", 1);
        }
        else
        {
            SoundSwitchGO.SetSwitchState(UISwitch.SwitchState.Switch_Off);
            PlayerPrefs.SetInt("game_settings_sound", 0);
        }
        // get login status
        if (FacebookServ.Instance.IsLoggedIn())
        {
            FBConnectSwitchGO.SetSwitchState(UISwitch.SwitchState.Switch_On);
            FacebookServ.Instance.GetInformation(OnDoneGettingInfo);
        }
        else
        {
            FBConnectSwitchGO.SetSwitchState(UISwitch.SwitchState.Switch_Off);
            FBNameGO.SetText("not connected");
            FBImageGO.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", DefaultAvatar);
        }
    }

    void OnEnable()
    {
        Initialize();
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

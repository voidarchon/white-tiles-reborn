﻿using UnityEngine;
using System.Collections;

public class ObjectsPoolController : MonoBehaviour {

    public ObjectsPool ObjectsPoolGO;

    private void OnPoolDoneInit()
    {
        GameObject obj = ObjectsPoolGO.GetObject();
        obj.SetActive(true);
        obj.transform.parent = transform;
        obj.GetComponent<TestObj>().TestFunc();
        obj.GetComponent<PooledObject>().ThrowBackToPool();
    }

	// Use this for initialization
	void Start () {
        ObjectsPoolGO.OnDoneInitEventListener += OnPoolDoneInit;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

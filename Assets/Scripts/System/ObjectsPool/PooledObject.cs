﻿using UnityEngine;
using System.Collections;

public class PooledObject : MonoBehaviour {

    private GameObject m_ThePool;

    public void SetPool(GameObject goThePool)
    {
        m_ThePool = goThePool;
        transform.parent = m_ThePool.transform;
    }

    public void ThrowBackToPool()
    {
        transform.parent = m_ThePool.transform;
        gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectsPool : MonoBehaviour {

    [Header("Objects Pool Configuration")]
    public GameObject PrefabPooledObject;
    public int ObjectNumber;
    public bool InitOnStart;

    // OnDoneInit event callback function
    public delegate void OnDoneInitEventDelegate();
    public OnDoneInitEventDelegate OnDoneInitEventListener;

    private List<GameObject> m_ThePool;
    private int m_NextPoolObj;

    private void OnDoneInit()
    {
        if (OnDoneInitEventListener != null)
            OnDoneInitEventListener();
    }

    public void InitializePool()
    {
        m_ThePool = new List<GameObject>(ObjectNumber);
        for (int i = 0; i < ObjectNumber; i++)
        {
            GameObject obj = Instantiate(PrefabPooledObject) as GameObject;
            obj.GetComponent<PooledObject>().SetPool(gameObject);
            obj.SetActive(false);
            m_ThePool.Add(obj);
        }
        m_NextPoolObj = 0;
        OnDoneInit();
    }

	public GameObject PeekNextObject()
	{
		return m_ThePool [m_NextPoolObj];
	}

    public GameObject GetObject()
    {
        GameObject retObj = m_ThePool[m_NextPoolObj];
        m_NextPoolObj = (m_NextPoolObj + 1) % ObjectNumber;
        return retObj;
    }

	// Use this for initialization
	void Start () {
        if (InitOnStart)
            InitializePool();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

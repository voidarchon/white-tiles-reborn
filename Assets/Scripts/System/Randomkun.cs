﻿using UnityEngine;
using System.Collections;

public class Randomkun {
	private static Randomkun m_Instance;

	public static Randomkun GetInstance() {
		if (m_Instance == null) {
			m_Instance = new Randomkun();
			return m_Instance;
		}
		return m_Instance;
	}

	private int[] m_ShuffleArray;
	private int m_CurrBound;

	private bool m_IsInit;

	private Randomkun() { m_IsInit = false; }

	public void Initialize(int range) {
		if (!m_IsInit) {
			m_ShuffleArray = new int[range];
			for (int i = 0; i < range; i++)
				m_ShuffleArray [i] = i;
			m_CurrBound = range;
			m_IsInit = true;
		}
	}

	public void ForgetEverything() {
		m_IsInit = false;
	}

	public void Reset()
	{
		m_CurrBound = m_ShuffleArray.Length;
	}

	public int GetRandomInt()
	{
		int r = Random.Range (0, m_CurrBound);
		int retVal = m_ShuffleArray [r];
		int tmp;
		tmp = m_ShuffleArray [m_CurrBound - 1];
		m_ShuffleArray [m_CurrBound - 1] = m_ShuffleArray [r];
		m_ShuffleArray [r] = tmp;
		m_CurrBound--;
		return retVal;
	}
}

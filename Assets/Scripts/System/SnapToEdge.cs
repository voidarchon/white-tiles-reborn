﻿using UnityEngine;
using System.Collections;

public class SnapToEdge : MonoBehaviour {

    public float Width;

    void Awake()
    {
        Vector3 pos = transform.position;
        float dest = Camera.main.orthographicSize * Camera.main.aspect;
        pos.x = dest - Width / 2.0f;
        
        transform.position = pos;
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

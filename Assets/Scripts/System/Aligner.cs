﻿using UnityEngine;
using System.Collections;

public class Aligner : MonoBehaviour {

    public float Offset;
    public enum EdgeSide { Left, Right };
    public EdgeSide Side;

    private float m_HorzHalfSize;

	// Use this for initialization
	void Start () {
        m_HorzHalfSize = Camera.main.orthographicSize * Camera.main.aspect;
        switch (Side)
        {
            case EdgeSide.Left:
                {
                    Vector3 currPos = transform.position;
                    currPos.x = - m_HorzHalfSize + Offset;
                    transform.position = currPos;
                    break;
                }
            case EdgeSide.Right:
                {
                    Vector3 currPos = transform.position;
                    currPos.x = m_HorzHalfSize - Offset;
                    transform.position = currPos;
                    break;
                }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

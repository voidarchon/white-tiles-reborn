﻿using UnityEngine;
using System.Collections;

public class UIKineticListView : MonoBehaviour {

    public SpringJoint2D TopSpringGO;
    public SpringJoint2D BotSpringGO;
    public Transform ListContainerGO;

    [Header("View Size")]
    public float Width;
    public float Height;

    [Header("Config")]
    public bool DisableInteraction;
    // public bool IsFocus;

    [Header("simulation")]
    public float ListHeight;

    private bool m_TopEnable;
    private bool m_BotEnable;
    private bool m_ListIsOverLength;

    private float m_HalfHeight;
    private float m_TopLimit;
    private float m_BotLimit;

    private Vector3 m_LastMousePos;
    private float m_MinDist = 0.005f;

    public bool StaticList;

    private bool m_ListIsShown;
    private bool m_ListReady;

    public void Initialize()
    {
        // ListContainerGO.GetComponent<Rigidbody2D>().isKinematic = true;
        TopSpringGO.anchor = new Vector2(0.0f, Height / 2.0f);
        BotSpringGO.anchor = new Vector2(0.0f, -Height / 2.0f);

        m_BotEnable = false;
        m_HalfHeight = ListHeight / 2.0f;

        m_TopLimit = TopSpringGO.anchor.y - m_HalfHeight;
        m_TopEnable = true;
        TopSpringGO.connectedAnchor = new Vector2(0.0f, m_HalfHeight);

        if (ListHeight >= Height)
        {
            m_BotLimit = BotSpringGO.anchor.y + m_HalfHeight;
            m_ListIsOverLength = true;
            m_BotEnable = false;
            BotSpringGO.distance = m_MinDist;
            BotSpringGO.connectedAnchor = new Vector2(0.0f, -m_HalfHeight);
        }
        else
        {
            m_BotLimit = m_TopLimit;
            m_ListIsOverLength = false;
            m_BotEnable = true;
            BotSpringGO.distance = m_TopLimit - BotSpringGO.anchor.y - m_HalfHeight;
            BotSpringGO.connectedAnchor = new Vector2(0.0f, - m_HalfHeight);
        }

        ListContainerGO.transform.localPosition = new Vector3(0.0f, m_TopLimit, ListContainerGO.transform.position.z);
        ListContainerGO.GetComponent<Rigidbody2D>().isKinematic = false;
    }
    
    private void UpdateSpringStatus()
    {
        m_TopEnable = false;
        m_BotEnable = false;
        if (ListContainerGO.transform.localPosition.y <= m_TopLimit)
            m_TopEnable = true;
        if (ListContainerGO.transform.localPosition.y >= m_BotLimit)
            m_BotEnable = true;
    }

    private void SetSpringStatus()
    {
        TopSpringGO.gameObject.SetActive(m_TopEnable);
        BotSpringGO.gameObject.SetActive(m_BotEnable);
    }

    public void ShowList()
    {
        m_ListIsShown = true;
        m_ListReady = true;
        // IsFocus = false;
        Initialize();
        UpdateSpringStatus();
    }

    void Awake()
    {
        m_ListIsShown = false;
        m_ListReady = false;
    }

	// Use this for initialization
	void Start () {
        if (StaticList)
            ShowList();
	}

    private void OnMouseDown()
    {
//       Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//         if (mousePos.x >= transform.position.x - Width / 2.0f
//             && mousePos.x <= transform.position.x + Width / 2.0f
//             && mousePos.y >= transform.position.y - Height / 2.0f
//             && mousePos.y <= transform.position.y + Height / 2.0f)
//             IsFocus = true;
//         else return;

        TopSpringGO.gameObject.SetActive(false);
        BotSpringGO.gameObject.SetActive(false);
        ListContainerGO.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    private void OnMouseMove(Vector3 deltaPosition)
    {
//         if (!IsFocus)
//             return;
        float deltaPosY = deltaPosition.y;

        if (m_TopEnable && deltaPosY < 0)
        {
            float A = Mathf.Abs(deltaPosY * 15.0f);
            float k = TopSpringGO.frequency;
            float x1 = k * Mathf.Abs(ListContainerGO.transform.localPosition.y - m_TopLimit);
            float s = Mathf.Sqrt(2 * A / k + x1 * x1) - x1;
            ListContainerGO.Translate(new Vector3(0.0f, -s, 0.0f));
        }
        else if (m_BotEnable && deltaPosY > 0)
        {
            float A = Mathf.Abs(deltaPosY * 15.0f);
            float k = BotSpringGO.frequency;
            float x1 = k * Mathf.Abs(ListContainerGO.transform.localPosition.y - m_BotLimit);
            float s = Mathf.Sqrt(2 * A / k + x1 * x1) - x1;
            ListContainerGO.Translate(new Vector3(0.0f, s, 0.0f));
        }
        else
        {
            ListContainerGO.Translate(new Vector3(0.0f, deltaPosY, 0.0f));
        }
        UpdateSpringStatus();
    }

    private void OnMouseUp(Vector3 deltaPosition)
    {
//         if (!IsFocus)
//             return;
//         else
//         {
            float deltaPosY = deltaPosition.y;
            ListContainerGO.GetComponent<Rigidbody2D>().AddForce(new Vector3(0.0f, deltaPosY * 100.0f, 0.0f), ForceMode2D.Impulse);
            UpdateSpringStatus();
            SetSpringStatus();
//             IsFocus = false;
//         }
    }

    void FixedUpdate()
    {
        if (m_ListIsShown)
        {
            if (ListContainerGO.GetComponent<Rigidbody2D>().velocity.magnitude != 0)
            {
                UpdateSpringStatus();
                SetSpringStatus();
            }
        }
    }

	// Update is called once per frame
    void Update()
    {
        if (DisableInteraction || !m_ListReady)
            return;
#if UNITY_EDITOR
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            m_LastMousePos = mousePos;
            OnMouseDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            OnMouseUp(mousePos - m_LastMousePos);
            m_LastMousePos = mousePos;
        }

        if (Input.GetMouseButton(0) && mousePos != m_LastMousePos)
        {
            OnMouseMove(mousePos - m_LastMousePos);
            m_LastMousePos = mousePos;
        }
#endif
#if !UNITY_EDITOR
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (i > 0)
                return;
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                m_LastMousePos = touchPos;
                OnMouseDown();
            }
            if (Input.GetTouch(i).phase == TouchPhase.Moved)
            {
                OnMouseMove(touchPos - m_LastMousePos);
                m_LastMousePos = touchPos;
            }
            if (Input.GetTouch(i).phase == TouchPhase.Ended)
            {
                OnMouseUp(touchPos - m_LastMousePos);
                m_LastMousePos = touchPos;
            }
        }
#endif
    }
}

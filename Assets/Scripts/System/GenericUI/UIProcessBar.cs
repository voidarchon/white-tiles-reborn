﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshRenderer))]
public class UIProcessBar : MonoBehaviour {

    [Header("Config")]
    public Color ForegroundColor;
    public Color BackgroundColor;
    public float Length;
    public float Width;

    [Header("Process")]
    public float BarPercent;

    private float m_SeperatePos;
    private Vector3[] m_Vertices;
    private Vector2[] m_UVs;
    private int[] m_Incides;
    private Mesh m_Mesh;

    private MeshFilter m_MeshFilter;
    private MeshRenderer m_MeshRenderer;

    // generate mesh
    private void GenerateMesh()
    {
        float z = transform.localPosition.z;
        m_SeperatePos = Length * BarPercent - Length / 2.0f;
        m_Vertices = new Vector3[] {
            new Vector3(-Length/2.0f, -Width/2.0f, z),
            new Vector3(-Length/2.0f, Width / 2.0f, z),
            new Vector3(Length/2.0f, Width/2.0f, z),
            new Vector3(Length/2.0f, -Width/2.0f, z)
        };

        m_UVs = new Vector2[] {
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(1.0f, 0.0f)
        };

        m_Incides = new int[] { 0, 1, 2, 2, 3, 0 };
        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UVs;
        m_Mesh.triangles = m_Incides;
        m_MeshFilter.mesh = m_Mesh;
    }

    public void SetForeGroundColor(Color color)
    {
        ForegroundColor = color;
        m_MeshRenderer.material.SetColor("_FGRColor", color);
    }

    public void SetBackGroundColor(Color color)
    {
        BackgroundColor = color;
        m_MeshRenderer.material.SetColor("_BGRColor", color);
    }

    public void SetPercent(float percent)
    {
        BarPercent = percent;
        m_MeshRenderer.material.SetFloat("_Percent", percent);
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshRenderer = GetComponent<MeshRenderer>();
        m_MeshRenderer.material.SetColor("_FGRColor", ForegroundColor);
        m_MeshRenderer.material.SetColor("_BGRColor", BackgroundColor);
        m_MeshRenderer.material.SetFloat("_Percent", BarPercent);
    }

	// Use this for initialization
	void Start () {
        GenerateMesh();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
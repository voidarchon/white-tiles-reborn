﻿using UnityEngine;
using System.Collections;

public class UISwitch : MonoBehaviour {

    public string OnText;
    public string OffText;

    public Color OnColor;
    public Color OffColor;

    public UILabel TextGO;

    public enum SwitchState { Switch_On, Switch_Off };
    public SwitchState m_CurrentState;

    [Header("Messages")]
    public GameObject TargetGameObject;
    public string SwitchOnMessageName;
    public string SwitchOffMessageName;

    private UIMesh m_UIMesh;

    private void OnDoSwitch()
    {
        if (m_CurrentState == SwitchState.Switch_On)
        {
            m_CurrentState = SwitchState.Switch_Off;
            TextGO.SetText(OffText);
            m_UIMesh.ChangeColor(OffColor);
            if (TargetGameObject != null && !string.IsNullOrEmpty(SwitchOffMessageName))
                TargetGameObject.SendMessage(SwitchOffMessageName);
        }
        else
        {
            m_CurrentState = SwitchState.Switch_On;
            TextGO.SetText(OnText);
            m_UIMesh.ChangeColor(OnColor);
            if (TargetGameObject != null && !string.IsNullOrEmpty(SwitchOnMessageName))
                TargetGameObject.SendMessage(SwitchOnMessageName);
        }
    }

    public void SetSwitchState(SwitchState newState)
    {
        m_CurrentState = newState;
        if (newState == SwitchState.Switch_Off)
        {
            TextGO.SetText(OffText);
            GetComponent<UIMesh>().ChangeColor(OffColor);
        }
        else
        {
            TextGO.SetText(OnText);
            GetComponent<UIMesh>().ChangeColor(OnColor);
        }
    }

    void Awake()
    {
        m_UIMesh = GetComponent<UIMesh>();
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

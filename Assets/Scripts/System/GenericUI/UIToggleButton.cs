﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIMesh))]
[RequireComponent(typeof(UITappable))]
public class UIToggleButton : MonoBehaviour {

    public enum ToggleState {State_On, State_Off};

    public delegate void OnSwitchStateListener(ToggleState state);
    public OnSwitchStateListener OnSwitchStateHandler;
    public ToggleState ButtonState;
    public Color OnColor;
    public Color OffColor;

    public void SetState(ToggleState newState)
    {
        ButtonState = newState;
        if (ButtonState == ToggleState.State_On)
            GetComponent<UIMesh>().ChangeColor(OnColor);
        else GetComponent<UIMesh>().ChangeColor(OffColor);
    }

    private void OnTapUpAsButtonDelegate()
    {
        if (ButtonState == ToggleState.State_On)
        {
            ButtonState = ToggleState.State_Off;
            if (OnSwitchStateHandler != null)
                OnSwitchStateHandler(ToggleState.State_Off);
            GetComponent<UIMesh>().ChangeColor(OffColor);
        }
        else
        {
            ButtonState = ToggleState.State_On;
            if (OnSwitchStateHandler != null)
                OnSwitchStateHandler(ToggleState.State_On);
            GetComponent<UIMesh>().ChangeColor(OnColor);
        }
    }
    
	// Use this for initialization
	void Start () {
        SetState(ButtonState);
        UIManager.Instance.AddUIObject(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIManager : Singleton<UIManager> {

    public Transform UIContainer;
    private LinkedList<GameObject> m_UIObjects;
    private bool m_UIHitted;

    private int m_FingerId;

    public void AddUIObject(GameObject uiObject)
    {
        // uiObject.transform.parent = UIContainer;
        m_UIObjects.AddLast(uiObject);
    }
#if !UNITY_EDITOR
    public void OnTapDown(GameObject uiObject, int fingerId)
    {
        if (!m_UIHitted)
        {
            m_UIHitted = true;
            m_FingerId = fingerId;
            uiObject.SendMessage("OnTapDown");
        }
    }

    public void OnTapUp(int fingerId)
    {
        if (m_UIHitted && m_FingerId == fingerId)
        {
            LinkedListNode<GameObject> uiObject = m_UIObjects.First;
            while (uiObject != null)
            {
                uiObject.Value.SendMessage("OnTapUp", SendMessageOptions.DontRequireReceiver);
                uiObject = uiObject.Next;
            }
            m_UIHitted = false;
        }
    }
#endif

    public void DirtyReset()
    {
        m_UIHitted = false;
    }

#if UNITY_EDITOR
    public void OnTapDown(GameObject uiObject)
    {
        m_UIHitted = true;
        uiObject.SendMessage("OnTapDown");
    }

    public void OnTapUp()
    {
        if (m_UIHitted)
        {
            LinkedListNode<GameObject> uiObject = m_UIObjects.First;
            while (uiObject != null)
            {
                uiObject.Value.SendMessage("OnTapUp", SendMessageOptions.DontRequireReceiver);
                uiObject = uiObject.Next;
            }
            m_UIHitted = false;

        }
    }
#endif

    void Awake()
    {
        m_UIObjects = new LinkedList<GameObject>();
        m_UIHitted = false;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

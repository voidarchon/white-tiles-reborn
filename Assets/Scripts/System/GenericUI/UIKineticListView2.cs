﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (UIKineticListViewTapController))]
public class UIKineticListView2 : MonoBehaviour {

    public SpringJoint2D TopSpringGO;
    public SpringJoint2D BotSpringGO;
    public Transform ListContainerGO;

    [Header("View Size")]
    public float Width;
    public float Height;

    [Header("debug")]
    public float TopMargin;
    public float BotMargin;
    public float Spacing;
    public float ItemHeight;
    public float ListHeight;

    public bool IsEnable = false;

    private List<GameObject> m_Items;
    private bool m_TopEnable;
    private bool m_BotEnable;

    private float m_HalfHeight;
    private float m_TopLimit;
    private float m_BotLimit;

    private float m_MinDist = 0.005f;

    private UIKineticListViewTapController m_UIKineticListViewTapController;

    private void Initialize()
    {
        // set springs's anchors
        TopSpringGO.anchor = new Vector2(0.0f, Height / 2.0f);
        BotSpringGO.anchor = new Vector2(0.0f, -Height / 2.0f);

        // physics properties
        m_HalfHeight = ListHeight / 2.0f;
        m_TopLimit = TopSpringGO.anchor.y - m_HalfHeight;
        TopSpringGO.connectedAnchor = new Vector2(0.0f, m_HalfHeight);

        // first-time show up, topspring is always enabled, bot is disabled.
        m_BotEnable = false;
        m_TopEnable = true;

        // the list is too long to display in one screen.
        if (ListHeight >= Height)
        {
            m_BotLimit = BotSpringGO.anchor.y + m_HalfHeight;
            m_BotEnable = false;
            BotSpringGO.distance = m_MinDist;
            BotSpringGO.connectedAnchor = new Vector2(0.0f, -m_HalfHeight);
        }
        else
        {
            m_BotLimit = m_TopLimit;
            m_BotEnable = true;
            BotSpringGO.distance = m_TopLimit - BotSpringGO.anchor.y - m_HalfHeight;
            BotSpringGO.connectedAnchor = new Vector2(0.0f, -m_HalfHeight);
        }
        ListContainerGO.transform.localPosition = new Vector3(0.0f, m_TopLimit, ListContainerGO.transform.position.z);
        ListContainerGO.GetComponent<BoxCollider2D>().size = new Vector2(5.0f, ListHeight);

        // update parts status
        ListContainerGO.gameObject.SetActive(true);
        UpdateSpringStatus();
    }

    public void SetEnable(bool enable)
    {
        IsEnable = enable;
        m_UIKineticListViewTapController.IsEnable = enable;
    }

    private void UpdateSpringStatus()
    {
        m_TopEnable = false;
        m_BotEnable = false;
        if (ListContainerGO.transform.localPosition.y <= m_TopLimit)
            m_TopEnable = true;
        if (ListContainerGO.transform.localPosition.y >= m_BotLimit)
            m_BotEnable = true;
    }

    private void SetSpringStatus()
    {
        TopSpringGO.gameObject.SetActive(m_TopEnable);
        BotSpringGO.gameObject.SetActive(m_BotEnable);
    }

    private void KineticListViewMouseDown()
    {
        TopSpringGO.gameObject.SetActive(false);
        BotSpringGO.gameObject.SetActive(false);
        ListContainerGO.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    private void KineticListViewMouseMove(Vector3 deltaPosition)
    {
        float deltaPosY = deltaPosition.y;

        if (m_TopEnable && deltaPosY < 0)
        {
            float A = Mathf.Abs(deltaPosY * 15.0f);
            float k = TopSpringGO.frequency;
            float x1 = k * Mathf.Abs(ListContainerGO.transform.localPosition.y - m_TopLimit);
            float s = Mathf.Sqrt(2 * A / k + x1 * x1) - x1;
            ListContainerGO.Translate(new Vector3(0.0f, -s, 0.0f));
        }
        else if (m_BotEnable && deltaPosY > 0)
        {
            float A = Mathf.Abs(deltaPosY * 15.0f);
            float k = BotSpringGO.frequency;
            float x1 = k * Mathf.Abs(ListContainerGO.transform.localPosition.y - m_BotLimit);
            float s = Mathf.Sqrt(2 * A / k + x1 * x1) - x1;
            ListContainerGO.Translate(new Vector3(0.0f, s, 0.0f));
        }
        else
        {
            ListContainerGO.Translate(new Vector3(0.0f, deltaPosY, 0.0f));
        }
        UpdateSpringStatus();
    }

    private void KineticListViewMouseUp(Vector3 deltaPosition)
    {
        float deltaPosY = deltaPosition.y;
        ListContainerGO.GetComponent<Rigidbody2D>().AddForce(new Vector3(0.0f, deltaPosY * 100.0f, 0.0f), ForceMode2D.Impulse);
        UpdateSpringStatus();
        SetSpringStatus();
    }

    /// <summary>
    /// do this before showing if you want to add something into list
    /// </summary>
    /// <param name="item"></param>
    /// <param name="itemHeight"></param>
    /// <param name="topMargin"></param>
    /// <param name="botMargin"></param>
    /// <param name="spacing"></param>
    public void AddItem(GameObject item)
    {
        m_Items.Add(item);
        item.transform.parent = ListContainerGO;
    }

    private void PositionItems()
    {
        for (int i = 0; i < m_Items.Count; i++)
        {
            Vector3 pos = new Vector3();
            pos.x = 0.0f;
            pos.z = -0.1f;

            pos.y = ListHeight / 2.0f - TopMargin - ItemHeight / 2.0f
                - i * (ItemHeight + Spacing);
            m_Items[i].transform.localPosition = pos;
        }
    }

    private void ComputeListHeight()
    {
        ListHeight = TopMargin + BotMargin + m_Items.Count * ItemHeight
            + (m_Items.Count - 1) * Spacing;
    }

    public void Show()
    {
        if (m_Items.Count == 0)
        {
            Debug.Log("list empty, cannot show");
        }
        else
        {
            ComputeListHeight();
            PositionItems();
            Initialize();
            SetEnable(true);
        }
    }

    public void ResetView()
    {
        TopSpringGO.gameObject.SetActive(false);
        BotSpringGO.gameObject.SetActive(false);
        ListContainerGO.gameObject.SetActive(false);
        Initialize();
        SetEnable(true);
    }

    void Awake()
    {
        m_UIKineticListViewTapController = GetComponent<UIKineticListViewTapController>();
        m_Items = new List<GameObject>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        if (gameObject.activeInHierarchy)
        {
            if (ListContainerGO.GetComponent<Rigidbody2D>().velocity.magnitude != 0)
            {
                UpdateSpringStatus();
                SetSpringStatus();
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (UISprite))]
[RequireComponent (typeof (UITappable))]
public class UISpriteToggleButton : MonoBehaviour
{

    public enum ToggleState { State_On, State_Off };
    public delegate void OnSwitchStateListener(ToggleState state);
    public OnSwitchStateListener OnSwitchStateHandler;
    public ToggleState ButtonState;

    public Sprite OnSprite;
    public Sprite OffSprite;

    public void SetState(ToggleState newState)
    {
        ButtonState = newState;
        if (ButtonState == ToggleState.State_On)
            GetComponent<UISprite>().SetSprite(OnSprite);
        else GetComponent<UISprite>().SetSprite(OffSprite);
    }

    private void OnTapUpAsButtonDelegate()
    {
        if (ButtonState == ToggleState.State_On)
        {
            ButtonState = ToggleState.State_Off;
            if (OnSwitchStateHandler != null)
                OnSwitchStateHandler(ToggleState.State_Off);
            GetComponent<UISprite>().SetSprite(OffSprite);
        }
        else
        {
            ButtonState = ToggleState.State_On;
            if (OnSwitchStateHandler != null)
                OnSwitchStateHandler(ToggleState.State_On);
            GetComponent<UISprite>().SetSprite(OnSprite);
        }
    }

	// Use this for initialization
	void Start () {
        SetState(ButtonState);
        UIManager.Instance.AddUIObject(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

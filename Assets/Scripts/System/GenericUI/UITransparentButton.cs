﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (UITappable))]
public class UITransparentButton : MonoBehaviour {

    [Header("Messages")]
    public GameObject TargetGameObject;
    public string TapUpMessageName;
    public string TapDownMessageName;
    public string TapUpAsButtonMessageName;

    private void OnTapUpDelegate()
    {
        if (TargetGameObject != null)
            if (!string.IsNullOrEmpty(TapUpMessageName))
                TargetGameObject.SendMessage(TapUpMessageName);
    }

    private void OnTapDownDelegate()
    {
        if (TargetGameObject != null)
            if (!string.IsNullOrEmpty(TapDownMessageName))
                TargetGameObject.SendMessage(TapDownMessageName);
    }

    private void OnTapUpAsButtonDelegate()
    {
        if (TargetGameObject != null)
            if (!string.IsNullOrEmpty(TapUpAsButtonMessageName))
                TargetGameObject.SendMessage(TapUpAsButtonMessageName);
    }

	// Use this for initialization
	void Start () {
        UIManager.Instance.AddUIObject(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

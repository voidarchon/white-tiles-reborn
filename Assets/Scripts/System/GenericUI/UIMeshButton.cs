﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (UIMesh))]
[RequireComponent (typeof (UITappable))]
public class UIMeshButton : MonoBehaviour {
    public delegate void OnTapActionListener();
    public OnTapActionListener OnTapUpHandler;
    public OnTapActionListener OnTapDownHandler;
    public OnTapActionListener OnTapUpAsButtonHandler;

    [Header("Messages")]
    public GameObject TargetGameObject;
    public string TapUpMessageName;
    public string TapDownMessageName;
    public string TapUpAsButtonMessageName;

    private void OnTapUpDelegate() {
        if (OnTapUpHandler != null)
            OnTapUpHandler();
        if (TargetGameObject != null)
            if (!string.IsNullOrEmpty(TapUpMessageName))
                TargetGameObject.SendMessage(TapUpMessageName);
    }

    private void OnTapDownDelegate() {
        if (OnTapDownHandler != null)
            OnTapDownHandler();
        if (TargetGameObject != null)
            if (!string.IsNullOrEmpty(TapDownMessageName))
                TargetGameObject.SendMessage(TapDownMessageName);
    }

    private void OnTapUpAsButtonDelegate() {
        if (OnTapUpAsButtonHandler != null)
            OnTapUpAsButtonHandler();
        if (TargetGameObject != null)
            if (!string.IsNullOrEmpty(TapUpAsButtonMessageName))
                TargetGameObject.SendMessage(TapUpAsButtonMessageName);
    }

	// Use this for initialization
	void Start () {
        UIManager.Instance.AddUIObject(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SpriteRenderer))]
public class UISprite : MonoBehaviour {

    private SpriteRenderer m_SpriteRenderer;

    public void SetSprite(Sprite spr)
    {
        if (m_SpriteRenderer == null)
            m_SpriteRenderer = GetComponent<SpriteRenderer>();

        m_SpriteRenderer.sprite = spr;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// TODO: new version: use BroadcastMessage
/// </summary>
[RequireComponent(typeof (BoxCollider2D))]
public class UITappable : MonoBehaviour {

    private Vector2 m_TapRegion;
    private BoxCollider2D m_BoxCollider2D;

    private bool m_IsFocus;
    public bool IsEnabled = true;

    public delegate void EventListenerDelegate();
    public EventListenerDelegate TapUpEventListener;
    public EventListenerDelegate TapDownEventListener;
    public EventListenerDelegate TapUsAsButtonEventListener;

    // TODO: 2 "up" printed on a single mouseup
    private void OnTapUp()
    {
        if (!IsEnabled)
            return;
        if (m_IsFocus)
        {
            BroadcastMessage("OnTapUpDelegate", SendMessageOptions.DontRequireReceiver);
            if (TapUpEventListener != null)
                TapUpEventListener();

            Vector2 mousePosW = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (mousePosW.x >= transform.position.x - m_TapRegion.x / 2.0f
                && mousePosW.x <= transform.position.x + m_TapRegion.x / 2.0f
                && mousePosW.y >= transform.position.y - m_TapRegion.y / 2.0f
                && mousePosW.y <= transform.position.y + m_TapRegion.y / 2.0f)
                OnTapUpAsButton();

            m_IsFocus = false;
        }
    }

    private void OnTapDown()
    {
        if (!IsEnabled)
            return;
        m_IsFocus = true;
        BroadcastMessage("OnTapDownDelegate", SendMessageOptions.DontRequireReceiver);
        if (TapDownEventListener != null)
            TapDownEventListener();
    }

    private void OnTapUpAsButton()
    {
        if (m_IsFocus && IsEnabled)
        {
            BroadcastMessage("OnTapUpAsButtonDelegate", SendMessageOptions.DontRequireReceiver);
            if (TapUsAsButtonEventListener != null)
                TapUsAsButtonEventListener();
            m_IsFocus = false;
        }
    }

    public void SetTapRegion(Vector2 region)
    {
        m_TapRegion = region;
        if (m_BoxCollider2D == null)
            m_BoxCollider2D = GetComponent<BoxCollider2D>();
        m_BoxCollider2D.size = m_TapRegion;
    }

    void Awake()
    {
        m_BoxCollider2D = GetComponent<BoxCollider2D>();
        m_TapRegion = m_BoxCollider2D.size * transform.localScale.x;

        m_IsFocus = false;
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (TextMesh))]
public class UILabel : MonoBehaviour {

    protected TextMesh m_TextMesh;

    public void SetText(string text)
    {
        if (m_TextMesh == null)
            m_TextMesh = GetComponent<TextMesh>();
        m_TextMesh.text = text;
    }

    void Awake()
    {
        if (m_TextMesh == null)
        m_TextMesh = GetComponent<TextMesh>();
    }

	// Use this for initialization
	void Start () {
        UIManager.Instance.AddUIObject(gameObject);
        GetComponent<Renderer>().material.renderQueue = 3002;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

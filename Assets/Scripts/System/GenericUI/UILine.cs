﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (MeshRenderer))]
[RequireComponent (typeof (MeshFilter))]
public class UILine : MonoBehaviour {
    public Vector2 From;
    private Vector2 m_LastFrom;
    public Vector2 To;
    private Vector2 m_LastTo;
    public float Width;
    private float m_Length;
    public Color LineColor;

    // mesh data
    private MeshFilter m_MeshFilter;
    private Mesh m_Mesh;
    private Vector3[] m_Vertices;
    private Vector2[] m_UVs;
    private int[] m_Indices;
    private Color[] m_VertexColors;

    private void CleanOldMeshData()
    {
        if (m_Mesh != null)
            Object.Destroy(m_Mesh);
    }

    public void GenerateMesh()
    {
        CleanOldMeshData();

        Vector2 u = From - To;

        float length = u.magnitude;
        Quaternion rot = Quaternion.FromToRotation(
            new Vector3(1.0f, 0.0f, 0.0f),
            new Vector3(Mathf.Abs(u.x), u.y, 0.0f));
//         if (From.x < To.x)
//         {
//             Quaternion rot2 = Quaternion.FromToRotation(
//                 new Vector3(0.0f, 0.0f, 1.0f),
//                 new Vector3(0.0f, 0.0f, -1.0f));
//             rot = rot * rot2;
//             Debug.Log(rot);
//         }

        m_Vertices = new Vector3[] {
            rot * new Vector3(-length / 2.0f, -Width / 2.0f, 0.0f), 
            rot * new Vector3(-length / 2.0f, Width / 2.0f, 0.0f), 
            rot * new Vector3(length / 2.0f, Width / 2.0f, 0.0f), 
            rot * new Vector3(length / 2.0f, -Width / 2.0f, 0.0f)
        };

        m_UVs = new Vector2[] {
            new Vector2(0.0f, 0.0f), 
            new Vector2(0.0f, 1.0f), 
            new Vector2(1.0f, 1.0f), 
            new Vector2(1.0f, 0.0f)
        };
        m_Indices = new int[] { 0, 1, 2, 2, 3, 0 };
        m_VertexColors = new Color[4];
        for (int i = 0; i < 4; i++)
            m_VertexColors[i] = LineColor;

        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UVs;
        m_Mesh.triangles = m_Indices;
        m_Mesh.colors = m_VertexColors;

        if (m_MeshFilter == null)
            m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshFilter.mesh = m_Mesh;

        Vector2 tslVct = (From + To) / 2.0f;
        transform.position = new Vector3(tslVct.x, tslVct.y, transform.position.z);
    }

    public void ChangeColor(Color color)
    {
        if (m_Mesh == null)
            GenerateMesh();
        LineColor = color;
        for (int i = 0; i < 4; i++)
            m_VertexColors[i] = LineColor;
        if (m_MeshFilter == null)
            m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshFilter.mesh.colors = m_VertexColors;
    }

    public void RefreshVertices()
    {
        Vector2 u = From - To;

        float length = u.magnitude;
        Quaternion rot = Quaternion.FromToRotation(
            new Vector3(1.0f, 0.0f, 0.0f),
            new Vector3(Mathf.Abs(u.x), u.y, 0.0f));

        m_Vertices[0] = rot * new Vector3(-length / 2.0f, -Width / 2.0f, 0.0f);
        m_Vertices[1] = rot * new Vector3(-length / 2.0f, Width / 2.0f, 0.0f); 
        m_Vertices[2] = rot * new Vector3(length / 2.0f, Width / 2.0f, 0.0f);
        m_Vertices[3] = rot * new Vector3(length / 2.0f, -Width / 2.0f, 0.0f);
        Debug.Log(rot);
        m_Mesh.vertices = m_Vertices;

        Vector2 tslVct = (From + To) / 2.0f;
        transform.position = new Vector3(tslVct.x, tslVct.y, transform.position.z);
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        if (m_MeshFilter == null)
            m_MeshFilter = GetComponent<MeshFilter>();
    }

    // Use this for initialization
    void Start()
    {
        if (m_Mesh == null)
            GenerateMesh();
        m_LastFrom = From;
        m_LastTo = To;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_LastFrom != From || m_LastTo != To)
        {
            RefreshVertices();
            m_LastFrom = From;
            m_LastTo = To;
        }
    }
}

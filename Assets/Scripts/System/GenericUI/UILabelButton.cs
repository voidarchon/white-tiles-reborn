﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (UITappable))]
public class UILabelButton : UILabel {

    private UITappable m_UITappable;

    void Awake()
    {
        m_TextMesh = GetComponent<TextMesh>();
        m_UITappable = GetComponent<UITappable>();
    }

	// Use this for initialization
	void Start () {
        // UIManager.Instance.AddUIObject(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

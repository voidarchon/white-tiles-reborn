﻿using UnityEngine;
using System.Collections;

public class UIKineticListViewTapController : MonoBehaviour {

    public bool IsEnable = false;
    public float TopY;
    public float BotY;

    private Vector3 m_LastMousePos;

    private bool m_Focus;

	// Use this for initialization
	void Start () {
        m_Focus = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!IsEnable)
            return;
#if UNITY_EDITOR
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0) && mousePos.y >= BotY && mousePos.y <= TopY)
        {
            m_Focus = true;
            m_LastMousePos = mousePos;
            BroadcastMessage("KineticListViewMouseDown");
        }
        if (Input.GetMouseButtonUp(0) && m_Focus)
        {
            // OnMouseUp(mousePos - m_LastMousePos);
            BroadcastMessage("KineticListViewMouseUp", mousePos - m_LastMousePos);
            m_LastMousePos = mousePos;
            m_Focus = false;
        }

        if (Input.GetMouseButton(0) && mousePos != m_LastMousePos && m_Focus)
        {
            // OnMouseMove(mousePos - m_LastMousePos);
            BroadcastMessage("KineticListViewMouseMove", mousePos - m_LastMousePos);
            m_LastMousePos = mousePos;
        }
#endif
#if !UNITY_EDITOR
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (i > 0)
                return;
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
            if (Input.GetTouch(i).phase == TouchPhase.Began && touchPos.y >= BotY && touchPos.y <= TopY)
            {
                m_Focus = true;
                m_LastMousePos = touchPos;
                // OnMouseDown();
                BroadcastMessage("KineticListViewMouseDown");
            }
            if (Input.GetTouch(i).phase == TouchPhase.Moved && m_Focus)
            {
                // OnMouseMove(touchPos - m_LastMousePos);
                BroadcastMessage("KineticListViewMouseMove", touchPos - m_LastMousePos);
                m_LastMousePos = touchPos;
            }
            if (Input.GetTouch(i).phase == TouchPhase.Ended && m_Focus)
            {
                // OnMouseUp(touchPos - m_LastMousePos);
                BroadcastMessage("KineticListViewMouseUp", touchPos - m_LastMousePos);
                m_LastMousePos = touchPos;
                m_Focus = false;
            }
        }
#endif
    }
}

﻿using UnityEngine;
using System.Collections;

public class LBSwitch : MonoBehaviour {

    private Animator m_Animator;
    public enum LBStatus { LBStatus_Alltime, LBStatus_Recent };
    public LBStatus ToggleStatus;

    private void OnTapUpAsButtonDelegate()
    {
        if (ToggleStatus == LBStatus.LBStatus_Alltime)
        {
            ToggleStatus = LBStatus.LBStatus_Recent;
            m_Animator.SetBool("ToggleAlltime", false);
            m_Animator.SetBool("ToggleRecent", true);
        }
        else
        {
            ToggleStatus = LBStatus.LBStatus_Alltime;
            m_Animator.SetBool("ToggleRecent", false);
            m_Animator.SetBool("ToggleAlltime", true);
        }
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

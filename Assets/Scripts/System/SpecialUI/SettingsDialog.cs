﻿using UnityEngine;
using System.Collections;

public class SettingsDialog : MonoBehaviour {

    private Animator m_Animator;
    public delegate void OnBackDelegate();
    public OnBackDelegate OnBackListener;

    private void SelfDisable()
    {
        gameObject.SetActive(false);
        if (OnBackListener != null)
            OnBackListener();
    }

    private void OnBackClicked()
    {
        m_Animator.SetBool("LeadOut", true);
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

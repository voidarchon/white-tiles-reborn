﻿using UnityEngine;
using System.Collections;

public class SettingsRibbon : MonoBehaviour {

    public UISpriteToggleButton SoundButtonGO;
    public UISpriteToggleButton FacebookButtonGO;
    public UISpriteToggleButton SettingsButtonGO;
    public MessageBox MessageBoxGO;

//     public UILabel FBNameGO;
//     public GameObject FBImageGO;
//     public Texture DefaultAvatar;

    private Animator m_Animator;
    private bool m_SoundSetting;

    #region tap_handlers
    private void OnSettingsButtonSwitched(UISpriteToggleButton.ToggleState newState)
    {
        if (newState == UISpriteToggleButton.ToggleState.State_On)
        {
            m_Animator.SetTrigger("DoLeadIn");
        }
        else
            m_Animator.SetTrigger("DoLeadOut");
    }
    private void OnSoundButtonSwitched(UISpriteToggleButton.ToggleState newState)
    {
        if (newState == UISpriteToggleButton.ToggleState.State_On)
        {
            Debug.Log("sound on");
            PlayerPrefs.SetInt("game_settings_sound", 1);
            GameSettingsManager.Instance.ChangeBoolSetting("sound", true);
        }
        else
        {
            Debug.Log("sound off");
            PlayerPrefs.SetInt("game_settings_sound", 0);
            GameSettingsManager.Instance.ChangeBoolSetting("sound", false);
        }
    }

    private void OnDoneLogin(FacebookServ.LoginResult result)
    {
        if (result == FacebookServ.LoginResult.Login_Fail ||
            result == FacebookServ.LoginResult.Login_Cancel)
        {
            Debug.Log("error log in facebook");
            FacebookButtonGO.SetState(UISpriteToggleButton.ToggleState.State_Off);
        }
        else {
            Debug.Log("success");
        }
    }

    private void YesResult()
    {
        FacebookServ.Instance.LogOut();
    }
    private void NoResult()
    {
        FacebookButtonGO.SetState(UISpriteToggleButton.ToggleState.State_On);
    }
    private void OnFacebookButtonSwitched(UISpriteToggleButton.ToggleState newState)
    {
        if (newState == UISpriteToggleButton.ToggleState.State_On)
        {
            Debug.Log("facebook on");
            FacebookServ.Instance.LogIn(OnDoneLogin);
        }
        else
        {
            Debug.Log("facebook off");
            if (MessageBoxGO != null)
            {
                MessageBoxGO.OnResultNoDelegate = NoResult;
                MessageBoxGO.OnResultYesDelegate = YesResult;
                MessageBoxGO.ResetPosition();
                MessageBoxGO.gameObject.SetActive(true);
                MessageBoxGO.Show("Log out Facebook?");
            }
            else FacebookServ.Instance.LogOut();
        }
    }
    #endregion

    /// <summary>
    /// Coroutine for downloading Facebook Avatar.
    /// </summary>
    /// <param name="imageGO"></param>
    /// <param name="url"></param>
    /// <returns></returns>
    private IEnumerator DownloadAvatarRoutine(GameObject imageGO, string url)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.texture.width >= 150)
            imageGO.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", www.texture);
        yield return null;
    }

    /// <summary>
    /// Callback for Facebook's GetInformation
    /// </summary>
    /// <param name="info"></param>
    private void OnDoneGettingInfo(LBFacebookInfo info)
    {
//         FBNameGO.SetText(info.FirstName);
//         StartCoroutine(DownloadAvatarRoutine(FBImageGO,
//             "https://graph.facebook.com/" + info.Id + "/picture?width=150&height=150"));
    }

    private void Initialize()
    {
        SoundButtonGO.OnSwitchStateHandler += OnSoundButtonSwitched;
        SettingsButtonGO.OnSwitchStateHandler += OnSettingsButtonSwitched;
        FacebookButtonGO.OnSwitchStateHandler += OnFacebookButtonSwitched;

        // get sound settings
        m_SoundSetting = PlayerPrefs.GetInt("game_settings_sound") == 1 ? true : false;
        if (m_SoundSetting)
        {
            SoundButtonGO.SetState(UISpriteToggleButton.ToggleState.State_On);
            PlayerPrefs.SetInt("game_settings_sound", 1);
        }
        else
        {
            SoundButtonGO.SetState(UISpriteToggleButton.ToggleState.State_Off);
            PlayerPrefs.SetInt("game_settings_sound", 0);
        }
        // get facebook login status
        if (FacebookServ.Instance.IsLoggedIn())         // if is logged in
        {
            FacebookButtonGO.SetState(UISpriteToggleButton.ToggleState.State_On);
            // get infomation, set callback when done.
            // set default image for FBConnectBtn (Facebook Icon - full light)
            FacebookServ.Instance.GetInformation(OnDoneGettingInfo);
        }
        else
        {
            FacebookButtonGO.SetState(UISpriteToggleButton.ToggleState.State_Off);
            // if not, 
            // hide FBName GameObject, set default image for FBConnecBtn (Facebook Icon - grayed out)

            // FBNameGO.SetText("not connected");
            // FBImageGO.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", DefaultAvatar);
        }
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

	void Start () {
        // right from start of the scene, 
        // initialize infos from settings xml and facebook status by native code SDK
        Initialize();
	}
	
	void Update () {
	
	}
}
﻿using UnityEngine;
using System.Collections;

public class InformationBox : MonoBehaviour {

    public delegate void OnResultListener();
    public OnResultListener OnResultOKDelegate;

    private Animator m_Animator;

    private void SelfDisable()
    {
        gameObject.SetActive(false);
    }

    private void OnResultOK()
    {
        Debug.Log("result: yes");
        if (OnResultOKDelegate != null)
            OnResultOKDelegate();
        m_Animator.SetBool("LeadOut", true);
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

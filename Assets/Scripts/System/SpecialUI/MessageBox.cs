﻿using UnityEngine;
using System.Collections;

public class MessageBox : MonoBehaviour {
    [Header("MessageBox items")]
    public GameObject MessageGO;
    public TextMesh YesTextGO;
    public TextMesh NoTextGO;

    private Animator m_Animator;

    public delegate void OnResultListener();
    public OnResultListener OnResultYesDelegate;
    public OnResultListener OnResultNoDelegate;

    private void SelfDisable()
    {
        gameObject.SetActive(false);
    }

    public void Show(string msg, string yesStr = "Yes", string noStr = "No")
    {
        MessageGO.GetComponent<TextMesh>().text = msg;
        YesTextGO.text = yesStr;
        NoTextGO.text = noStr;
    }
    // TODO: set a dialog result variable => depend of its value, choose action
    // to perform in SelfDisable();
    private void OnResultYes()
    {
        if (OnResultYesDelegate != null)
            OnResultYesDelegate();
        m_Animator.SetBool("LeadOut", true);
    }

    private void OnResultNo()
    {
        if (OnResultNoDelegate != null)
            OnResultNoDelegate();
        gameObject.SetActive(false);
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    public void ResetPosition()
    {
        transform.localPosition = new Vector3(0.0f, 0.0f, transform.localPosition.z);
    }

	// Use this for initialization
	void Start () {
        // Show("this is an message", "co", "khong");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

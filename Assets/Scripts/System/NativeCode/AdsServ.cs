﻿using UnityEngine;
using System.Collections;

public class AdsServ : Singleton<AdsServ> {
    public void DoRating()
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("AndroidDoRating", new object[] { "not impl" });
#endif
#if UNITY_IOS
		NativeCodeCall.IOSDoRating();
#endif
#if UNITY_WP8
        NativeCodeCall.WPDoRating();
#endif
    }

    public void ShowAppOpen()
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("ADSAndroidShowAppOpen", new object[] { "not impl" });
#endif
#if UNITY_IOS
		NativeCodeCall.ADSIOSShowAppOpen();
#endif
#if UNITY_WP8
        NativeCodeCall.ADSWPShowAppOpen();
#endif
    }

    public void ShowMoreApps()
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("ADSAndroidShowMoreApps", new object[] { "not impl" });
#endif
#if UNITY_IOS
		NativeCodeCall.ADSIOSShowMoreApps();
#endif
#if UNITY_WP8
        NativeCodeCall.ADSWPShowMoreApps();
#endif
    }

    public void ShowPopup()
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("ADSAndroidShowPopup", new object[] { "not impl" });
#endif
#if UNITY_IOS
		NativeCodeCall.ADSIOSShowPopup();
#endif
#if UNITY_WP8
        NativeCodeCall.ADSWPShowPopup();
#endif
    }

    public void ShowPopupWithCountChecking()
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("ADSAndroidShowPopupWithCountChecking", new object[] { "not impl" });
#endif
#if UNITY_IOS
		NativeCodeCall.ADSIOSShowPopupWithCountChecking();
#endif
#if UNITY_WP8
        NativeCodeCall.ADSWPShowPopupWithCountChecking();
#endif
    }
}
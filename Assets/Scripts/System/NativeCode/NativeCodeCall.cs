﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class NativeCodeCall {

#if UNITY_IOS

#if !UNITY_EDITOR
	// facebook
	[DllImport("__Internal")]
	public static extern string FBIOSGetFirstName();
	[DllImport("__Internal")]
	public static extern string FBIOSGetLastName();
	[DllImport("__Internal")]
	public static extern string FBIOSGetGender();
	[DllImport("__Internal")]
	public static extern string FBIOSGetLink();
	[DllImport("__Internal")]
	public static extern string FBIOSGetId();
	[DllImport("__Internal")]
	public static extern string FBIOSGetFullName();

	[DllImport("__Internal")]
	public static extern void FBIOSLogIn(string goName, string successCallback, string failedCallback, string cancelCallback);
	[DllImport("__Internal")]
	public static extern bool FBIOSIsLoggedIn();
	[DllImport("__Internal")]
	public static extern void FBIOSLogOut();
	[DllImport("__Internal")]
	public static extern void FBIOSGetInformation(string goName, string doneCallback);

	[DllImport("__Internal")]
	public static extern void FBIOSShareLinkContent(string link, string linkCaption, string linkDescription,
	                                                string goName, string successCallback, string failedCallback, string cancelCallback);
	// google analytics
	[DllImport("__Internal")]
	public static extern void GAIOSLogScreen(string screenName);

	// ads
	[DllImport("__Internal")]
	public static extern void ADSIOSShowPopup();
	[DllImport("__Internal")]
	public static extern void ADSIOSShowPopupWithCountChecking();
	[DllImport("__Internal")]
	public static extern void IOSDoRating ();
	[DllImport("__Internal")]
	public static extern void ADSIOSShowAppOpen ();
	[DllImport("__Internal")]
	public static extern void ADSIOSShowMoreApps ();
#endif

#if UNITY_EDITOR
	// facebook
	public static string FBIOSGetFirstName() {
		return "";
	}
	public static string FBIOSGetLastName() {
		return "";
	}
	public static string FBIOSGetFullName() {
		return "";
	}
	public static string FBIOSGetId() {
		return "";
	}
	public static string FBIOSGetLink() {
		return "";
	}
	public static string FBIOSGetGender() {
		return "";
	}
	public static void FBIOSLogIn(string goName, string successCallback, string failedCallback, string cancelCallback) {}
	public static bool FBIOSIsLoggedIn() { return false; }
	public static void FBIOSLogOut() {}
	public static void FBIOSGetInformation(string goName, string doneCallback) {}
	public static void FBIOSShareLinkContent(string link, string linkCaption, string linkDescription,
	                                         string goName, string successCallback, string failedCallback, string cancelCallback) {}
	// google analytics
	public static void GAIOSLogScreen(string screenName) {}

	// ads
	public static void ADSIOSShowPopup() {}
	public static void ADSIOSShowPopupWithCountChecking() {}
	public static void IOSDoRating () {}
	public static void ADSIOSShowAppOpen () {}
	public static void ADSIOSShowMoreApps () {}

#endif

#endif

#if UNITY_WP8
#if !UNITY_EDITOR
    // facebook
    public static Func<string> FBWPGetFullName;
    public static Func<string> FBWPGetId;
    public static Func<string> FBWPGetLink;
    public static Func<string> FBWPGetGender;
    public static Func<string> FBWPGetFirstName;
    public static Func<string> FBWPGetLastName;

    public static Action<Action, Action, Action> FBWPLogIn;
    public static Func<bool> FBWPIsLoggedIn;
    public static Action FBWPLogOut;
    public static Action<Action> FBWPGetInformation;
    public static Action<string, string, string> FBWPShareLinkContent;

    // google analytics
    public static Action<string> GAWPLogScreen;

    // ads
    public static Action ADSWPShowPopup;
    public static Action ADSWPShowPopupWithCountChecking;
    public static Action WPDoRating;
    public static Action ADSWPShowAppOpen;
    public static Action ADSWPShowMoreApps;
#endif

#if UNITY_EDITOR
    // facebook
    public static string FBWPGetFullName()
    {
        return "";
    }
    public static string FBWPGetId()
    {
        return "";
    }
    public static string FBWPGetLink()
    {
        return "";
    }
    public static string FBWPGetGender()
    {
        return "";
    }
    public static string FBWPGetFirstName()
    {
        return "";
    }
    public static string FBWPGetLastName()
    {
        return "";
    }

    public static void FBWPLogIn(Action onSucess, Action onCancel, Action onFailed)
    {

    }
    public static bool FBWPIsLoggedIn()
    {
        return false;
    }
    public static void FBWPLogOut()
    {

    }
    public static void FBWPGetInformation(Action onDone)
    {

    }
    public static void FBWPShareLinkContent(
        string link, string linkCaption, string linkDescription)
    {

    }

    // google analytics
    public static void GAWPLogScreen(string screenName) { }

    // ads
    public static void ADSWPShowPopup() { }
    public static void ADSWPShowPopupWithCountChecking() { }
    public static void WPDoRating() { }
    public static void ADSWPShowAppOpen() { }
    public static void ADSWPShowMoreApps() { }
#endif
#endif

#if UNITY_ANDROID
    public static void CallAndroidMethod(string methodName, object[] param)
    {
#if !UNITY_EDITOR
        using (var clsUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var objActivity = clsUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                objActivity.Call(methodName, param);
            }
        }
#endif
    }

    public static bool GetBoolValue(string methodName, object[] param)
    {
#if !UNITY_EDITOR
        using (var clsUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var objActivity = clsUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                return objActivity.Call<bool>(methodName);
            }
        }
#endif
        return false;
    }

    public static string GetStringValue(string methodName, object[] param)
    {
#if !UNITY_EDITOR
        using (var clsUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (var objActivity = clsUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                return objActivity.Call<string>(methodName);
            }
        }
#endif
        return "";
    }

#endif

}
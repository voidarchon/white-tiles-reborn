﻿using UnityEngine;
using System.Collections;

public class AnalyticsServ {

	public static void LogScreen(string screenName)
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("GAAndroidLogScreen", new object[] { screenName });
#endif
#if UNITY_IOS
		NativeCodeCall.GAIOSLogScreen(screenName);
#endif
#if UNITY_WP8
        NativeCodeCall.GAWPLogScreen(screenName);
#endif
    }
}
﻿using UnityEngine;
using System.Collections;

public class FacebookServ : Singleton<FacebookServ>
{

    #region Login result callbacks
    public enum LoginResult { Login_Fail, Login_Success, Login_Cancel };
    public delegate void DoneLoginDelegate(LoginResult result);
    private DoneLoginDelegate m_DoneLoginDelegate;

    private void OnLoginSuccess()
    {
        if (m_DoneLoginDelegate != null)
            m_DoneLoginDelegate(LoginResult.Login_Success);
    }

    private void OnLoginFail()
    {
        if (m_DoneLoginDelegate != null)
            m_DoneLoginDelegate(LoginResult.Login_Fail);
    }

    private void OnLoginCancel()
    {
        if (m_DoneLoginDelegate != null)
            m_DoneLoginDelegate(LoginResult.Login_Cancel);
    }
    #endregion

    public void LogIn(DoneLoginDelegate doneLoginDelegate)
    {
#if UNITY_ANDROID
        m_DoneLoginDelegate = doneLoginDelegate;
        NativeCodeCall.CallAndroidMethod("FBAndroidLogIn", new object[] {
            gameObject.name, "OnLoginSuccess", "OnLoginFail", "OnLoginCancel"
        });
#endif
#if UNITY_IOS
		m_DoneLoginDelegate = doneLoginDelegate;
		NativeCodeCall.FBIOSLogIn(gameObject.name, "OnLoginSuccess", "OnLoginFail", "OnLoginCancel");
#endif
#if UNITY_WP8
        m_DoneLoginDelegate = doneLoginDelegate;
        NativeCodeCall.FBWPLogIn(OnLoginSuccess, OnLoginCancel, OnLoginFail);
#endif
	}

    public bool IsLoggedIn()
    {
#if UNITY_ANDROID
        return NativeCodeCall.GetBoolValue("FBAndroidIsLoggedIn", null);
#endif
#if UNITY_IOS
		return NativeCodeCall.FBIOSIsLoggedIn();
#endif
#if UNITY_WP8
        return NativeCodeCall.FBWPIsLoggedIn();
#endif
    }

    public void LogOut()
    {
#if UNITY_ANDROID
        NativeCodeCall.CallAndroidMethod("FBAndroidLogOut", new object[] {
            "", "", "", ""
        });
#endif
#if UNITY_IOS
		NativeCodeCall.FBIOSLogOut();
#endif
#if UNITY_WP8
        NativeCodeCall.FBWPLogOut();
#endif
    }

    #region GetInformation callback
    public delegate void DoneGettingFBInformationDelegate(LBFacebookInfo info);
    private DoneGettingFBInformationDelegate m_DoneGettingInfoDelegate;

    private void OnDoneGettingInfo()
    {
#if UNITY_ANDROID
        string firstName = NativeCodeCall.GetStringValue("FBAndroidGetFirstName", null);
        string lastName = NativeCodeCall.GetStringValue("FBAndroidGetLastName", null);
        string id = NativeCodeCall.GetStringValue("FBAndroidGetId", null);
        string gender = NativeCodeCall.GetStringValue("FBAndroidGetGender", null);
        string link = NativeCodeCall.GetStringValue("FBAndroidGetLink", null);
        string fullName = NativeCodeCall.GetStringValue("FBAndroidGetFullName", null);
#endif
#if UNITY_IOS
		string firstName = NativeCodeCall.FBIOSGetFirstName();
		string lastName = NativeCodeCall.FBIOSGetLastName();
		string id = NativeCodeCall.FBIOSGetId();
		string gender = NativeCodeCall.FBIOSGetGender();
		string link = NativeCodeCall.FBIOSGetLink();
		string fullName = NativeCodeCall.FBIOSGetFullName();
#endif
#if UNITY_WP8
        string firstName = NativeCodeCall.FBWPGetFirstName();
        string lastName = NativeCodeCall.FBWPGetLastName();
        string id = NativeCodeCall.FBWPGetId();
        string gender = NativeCodeCall.FBWPGetGender();
        string link = NativeCodeCall.FBWPGetLink();
        string fullName = NativeCodeCall.FBWPGetFullName();
#endif
        LBFacebookInfo info = new LBFacebookInfo(
            firstName,
            lastName,
            id,
            gender,
            link,
            fullName);
        if (m_DoneGettingInfoDelegate != null)
            m_DoneGettingInfoDelegate(info);
    }
    #endregion

    public void GetInformation(DoneGettingFBInformationDelegate onDone)
    {
#if UNITY_ANDROID
        m_DoneGettingInfoDelegate = onDone;
        NativeCodeCall.CallAndroidMethod("FBAndroidGetInformation", new object[] {
            gameObject.name, "OnDoneGettingInfo"
        });
#endif
#if UNITY_IOS
		m_DoneGettingInfoDelegate = onDone;
		NativeCodeCall.FBIOSGetInformation(gameObject.name, "OnDoneGettingInfo");
#endif
#if UNITY_WP8
        m_DoneGettingInfoDelegate = onDone;
        NativeCodeCall.FBWPGetInformation(OnDoneGettingInfo);
#endif
    }

    #region Share result callbacks
    public enum ShareResult { Share_Fail, Share_Success, Share_Cancel };
    public delegate void DoneShareDelegate(ShareResult result);
    private DoneShareDelegate m_DoneShareDelegate;

    private void OnShareSuccess()
    {
        if (m_DoneShareDelegate != null)
            m_DoneShareDelegate(ShareResult.Share_Success);
    }

    private void OnShareFail()
    {
        if (m_DoneShareDelegate != null)
            m_DoneShareDelegate(ShareResult.Share_Fail);
    }

    private void OnShareCancel()
    {
        if (m_DoneShareDelegate != null)
            m_DoneShareDelegate(ShareResult.Share_Cancel);
    }
    #endregion

    public void ShareImage()
    {

    }

    public void ShareLinkContent(string link, string linkCaption, string linkDescription,
        DoneShareDelegate doneShareDelegate)
    {
#if UNITY_ANDROID
        m_DoneShareDelegate = doneShareDelegate;
        NativeCodeCall.CallAndroidMethod("FBAndroidShareLinkContent", new object[] { 
            link, linkCaption, linkDescription,
            gameObject.name, "OnShareSuccess", "OnShareFail", "OnShareCancel"
        });
#endif
#if UNITY_IOS
		m_DoneShareDelegate = doneShareDelegate;
		NativeCodeCall.FBIOSShareLinkContent(link, linkCaption, linkDescription,
		                                     gameObject.name, "OnShareSuccess", "OnShareFail", "OnShareCancel");
#endif
#if UNITY_WP8
        m_DoneShareDelegate = doneShareDelegate;
        NativeCodeCall.FBWPShareLinkContent(link, linkCaption, linkDescription);
            // OnShareSuccess, OnShareCancel, OnShareFail);
#endif
    }

    public void AppInvite()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }
}
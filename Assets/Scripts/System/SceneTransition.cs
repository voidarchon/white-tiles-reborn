﻿using UnityEngine;
using System.Collections;

public class SceneTransition : MonoBehaviour {

    public bool DoneLeadInSelfDestruct;
    public bool DoneLeadOutSelfDestruct;

    public delegate void OnDoneAnimationListener();
    public OnDoneAnimationListener OnDoneLeadInListener;
    public OnDoneAnimationListener OnDoneLeadOutListener;

    private MeshRenderer m_MeshRenderer;
    private Animator m_Animator;

    private void OnDoneLeadIn()
    {
        if (OnDoneLeadInListener != null)
            OnDoneLeadInListener();
        if (DoneLeadInSelfDestruct)
            Object.Destroy(gameObject);
    }

    private void OnDoneLeadOut()
    {
        if (OnDoneLeadOutListener != null)
            OnDoneLeadOutListener();
        if (DoneLeadOutSelfDestruct)
            Object.Destroy(gameObject);
    }

    /// <summary>
    /// Enable the Transition object before calling this.
    /// </summary>
    /// <param name="leadOutColor"></param>
    public void BeginLeadOut(Color leadOutColor)
    {
        m_MeshRenderer.material.SetColor("_Color", leadOutColor);
        m_Animator.SetBool("LeadOut", true);
    }

    /// <summary>
    /// Enable the Transition object before calling this.
    /// </summary>
    /// <param name="leadInColor"></param>
    public void BeginLeadIn(Color leadInColor)
    {
        // m_Animator.enabled = false;
        // m_MeshRenderer.material.SetColor("_Color", leadInColor);
        m_MeshRenderer.material.SetFloat("_Threshold", 1.0f);
        m_Animator.SetBool("LeadIn", true);
    }

    void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
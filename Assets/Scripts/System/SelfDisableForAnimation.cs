﻿using UnityEngine;
using System.Collections;

public class SelfDisableForAnimation : MonoBehaviour {

    private void SelfDisable()
    {
        gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class TouchHandlerComponent : MonoBehaviour {
    public bool EnableInteraction = true;
    public bool EnableDrag = false;
    public float DragTopY;
    public float DragBotY;
    private Vector3 m_OrgMousePos;
    private Vector2 m_OrgTouchPos;
    private Vector2 m_OrgTouchPosW;
    private bool m_IsDragging;
    private bool m_GoodDragStart;
    private int m_Direction;        // 1 = left | -1 = right
    public delegate void SlideDelegate();
    public SlideDelegate OnSlideLeft;
    public SlideDelegate OnSlideRight;

    [Header("Touches number")]
    public int TouchNumber;
	public bool TouchAnimation;

    public void EnableDragging(float topY, float botY)
    {
        EnableDrag = true;
        DragTopY = topY;
        DragBotY = botY;
    }

    public void DisableDragging()
    {
        EnableDrag = false;
    }

	// Use this for initialization
	void Start () {
        m_IsDragging = false;
        m_GoodDragStart = false;
        m_Direction = 0;
	}
	
	// Update is called once per frame
    void Update()
    {
        if (!EnableInteraction)
            return;
#if !UNITY_EDITOR
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (i > TouchNumber - 1)
                return;
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                bool uiHitted = false;
                m_OrgTouchPos = Input.GetTouch(i).position;
                Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
                m_OrgTouchPosW = touchPos;
				if (TouchAnimation)
					BroadcastMessage("OnTouchAnimation", touchPos);
                RaycastHit2D hittedObj = Physics2D.Raycast(touchPos, Vector2.zero, 100.0f);
                if (hittedObj.collider != null)
                {
                    if (string.Compare(hittedObj.collider.gameObject.tag, "UI") != 0)
                        hittedObj.collider.gameObject.SendMessage("OnTap");
                    else
                    {
                        UIManager.Instance.OnTapDown(hittedObj.collider.gameObject,
                            Input.GetTouch(i).fingerId);
                        uiHitted = true;
                    }
                }
            }
            if (EnableDrag)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Moved)
                {
                    Vector2 newTouchPos = Input.GetTouch(i).position;
                    Vector2 touchPos = Camera.main.ScreenToWorldPoint(newTouchPos);
                    if (m_OrgTouchPosW.y >= DragBotY && m_OrgTouchPosW.y <= DragTopY)
                    {
                        if (Mathf.Abs(touchPos.x - m_OrgTouchPosW.x) >= 1.0f)
                        {
                            m_IsDragging = true;
                            if (touchPos.x < m_OrgTouchPosW.x)
                                m_Direction = 1;
                            else m_Direction = -1;
                        }
                        else 
                        {
                            m_IsDragging = false;
                        }
                    }
                    /*
                    Vector2 newTouchPos = Input.GetTouch(i).position;
                    Vector2 touchPos = Camera.main.ScreenToWorldPoint(newTouchPos);
                    if (touchPos.y >= DragBotY && touchPos.y <= DragTopY)
                    {
                        UIManager.Instance.DirtyReset();
                        // dragging
                        m_IsDragging = true;
                        if (newTouchPos.x < m_OrgTouchPos.x)
                            m_Direction = 1;
                        else if (newTouchPos.x > m_OrgTouchPos.x)
                            m_Direction = -1;
                        m_OrgTouchPos = newTouchPos;
                    }
                     */
                }
            }

            if (Input.GetTouch(i).phase == TouchPhase.Ended)
            {
                if (m_IsDragging)
                {
                    UIManager.Instance.DirtyReset();
                    if (m_Direction > 0)
                    {
                        if (OnSlideLeft != null) OnSlideLeft();
                    }
                    else if (m_Direction < 0)
                    {
                        if (OnSlideRight != null) OnSlideRight();
                    }
                    m_IsDragging = false;
                }
                UIManager.Instance.OnTapUp(Input.GetTouch(i).fingerId);
            }
        }
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            bool uiHitted = false;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			if (TouchAnimation)
				BroadcastMessage("OnTouchAnimation", mousePos);

            RaycastHit2D hittedObj = Physics2D.Raycast(mousePos, Vector2.zero, 100.0f);
            if (hittedObj.collider != null)
            {
                if (string.Compare(hittedObj.collider.gameObject.tag, "UI") != 0)
                {
                    hittedObj.collider.gameObject.SendMessage("OnTap", SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    UIManager.Instance.OnTapDown(hittedObj.collider.gameObject);
                    uiHitted = true;
                }
            }
        }
        if (EnableDrag)
            if (Input.GetMouseButtonDown(0))
            {
                m_OrgMousePos = Input.mousePosition;
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(m_OrgMousePos);
                if (mousePos.y >= DragBotY && mousePos.y <= DragTopY)
                    m_GoodDragStart = true;
            }

            if (m_GoodDragStart &&
                Input.GetMouseButton(0) && 
                Input.mousePosition != m_OrgMousePos)
            {
                UIManager.Instance.DirtyReset();
                // dragging
                m_IsDragging = true;
                if (Input.mousePosition.x < m_OrgMousePos.x)
                    m_Direction = 1;
                else if (Input.mousePosition.x > m_OrgMousePos.x)
                    m_Direction = -1;
                m_OrgMousePos = Input.mousePosition;
            }

        if (Input.GetMouseButtonUp(0))
        {
            m_GoodDragStart = false;
            if (m_IsDragging)
            {
                if (m_Direction > 0)
                {
                    if (OnSlideLeft != null) OnSlideLeft();
                }
                else if (m_Direction < 0)
                {
                    if (OnSlideRight != null) OnSlideRight();
                }
                m_IsDragging = false;
            }
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hittedObj = Physics2D.Raycast(mousePos, Vector2.zero, 100.0f);
            UIManager.Instance.OnTapUp();
        }
#endif
    }
}

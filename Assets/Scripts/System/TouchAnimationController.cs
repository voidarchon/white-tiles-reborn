﻿using UnityEngine;
using System.Collections;

public class TouchAnimationController : MonoBehaviour {

	private ObjectsPool m_ObjectsPool;

	private void OnTouchAnimation(Vector2 mousePos)
	{
		GameObject go = m_ObjectsPool.GetObject ();
		go.transform.position = new Vector3 (mousePos.x, mousePos.y, go.transform.position.z);
		go.SetActive (true);
	}

	void Awake() {
		m_ObjectsPool = GetComponent<ObjectsPool> ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

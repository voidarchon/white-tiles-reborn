﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NotificationCenter {
    private static NotificationCenter m_Instance;

    private NotificationCenter()
    {
        m_NotificationListenerList = new Dictionary<string, NotificationListener>();
    }
    public static NotificationCenter GetInstance()
    {
        if (m_Instance == null)
        {
            m_Instance = new NotificationCenter();
            return m_Instance;
        }
        return m_Instance;
    }

	public void ClearAllNotificationsAndListeners()
	{
		m_NotificationListenerList.Clear ();
	}

    public delegate void NotificationListener(GameObject sender, object data);
    private Dictionary<string, NotificationListener> m_NotificationListenerList;

    /// <summary>
    /// Should be called in Awake()
    /// </summary>
    /// <param name="notificationName">Name of register notification.</param>
    public void RegisterNotification(string notificationName)
    {
        if (!m_NotificationListenerList.ContainsKey(notificationName))
            m_NotificationListenerList.Add(notificationName, null);
        else
        {
            // Debug.Log("already have that notificationName");
        }
    }

    /// <summary>
    /// Should be called in Start()
    /// </summary>
    /// <param name="notificationName"></param>
    /// <param name="listener"></param>
    public void RegisterListener(string notificationName, NotificationListener listener)
    {
        if (!m_NotificationListenerList.ContainsKey(notificationName))
        {
            Debug.Log("no such notificationName");
            return;
        }
        else
        {
            m_NotificationListenerList[notificationName] += listener;
        }
    }

    public void InvokeNotification(GameObject sender, string nofitcationName, object data)
    {
        if (m_NotificationListenerList[nofitcationName] != null)
        {
            m_NotificationListenerList[nofitcationName](sender, data);
        }
        else
        {
            Debug.Log("no listener " + nofitcationName);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
#if !UNITY_WP8
using System.Xml;
#endif
#if UNITY_WP8
using System.Linq;
using System.Xml.Linq;
// using System.Xml.XPath;
#endif
using System;
using System.IO;
using System.Text;

public class GameSettingsManager
{
    private static GameSettingsManager m_Instance;
    private GameSettingsManager() { }
    public static GameSettingsManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new GameSettingsManager();
                return m_Instance;
            }
            return m_Instance;
        }
    }

    private string m_FilePath;
#if !UNITY_WP8
    private XmlDocument m_Document;
#endif
#if UNITY_WP8
    private XDocument m_Document;
#endif
    private string m_DefaultContent;
    private string m_Version;

    public void Initialize(string fileName, string defaultContent, string version)
    {
        m_DefaultContent = defaultContent;
        m_Version = version;
        m_FilePath = Path.Combine(Application.persistentDataPath, fileName);
#if !UNITY_WP8
        m_Document = new XmlDocument();
#endif
#if UNITY_WP8
        m_Document = new XDocument();
#endif
        LoadFile();
        Validate();
    }

    private void ReadContent()
    {
        Debug.Log("[GameSettingsManager] Attemping to read file content...");
        FileStream fileStream = null;
#if NETFX_CORE
        fileStream = File.Open(m_FilePath);
#else
        fileStream = File.Open(m_FilePath, FileMode.Open);
#endif
#if !UNITY_WP8
        m_Document.Load(fileStream);
#endif
#if UNITY_WP8
        m_Document = XDocument.Load(fileStream);
#endif
        fileStream.Close();
        fileStream.Dispose();
        Debug.Log("[GameSettingsManager] Done reading.");
    }

    private void CreateNewlyFile()
    {
        FileStream fileStream = null;
        Debug.Log("[GameSettingsManager] Attemping to create newly gamesettings file...");
#if NETFX_CORE
        fileStream = File.Open(m_FilePath);
#else
        fileStream = File.Open(m_FilePath, FileMode.Create);
#endif
        StreamWriter sw = new StreamWriter(fileStream, Encoding.UTF8);
        sw.Write(m_DefaultContent);
        sw.Close();
        fileStream.Close();
        fileStream.Dispose();
        Debug.Log("[GameSettingsManager] Done creating new gamesettings file.");
    }

    private void LoadFile()
    {
        Debug.Log("[GameSettingsManager] Attemping to load file...");
        try
        {
            ReadContent();
        }
        catch
        {
            Debug.LogWarning("[GameSettingsManager] File not found.");
            CreateNewlyFile();
            ReadContent();
        }
    }

    private void Validate()
    {
        Debug.Log("[GameSettingsManager] Validating...");
        if (!ValidateVersion())
        {
            Debug.Log("[GameSettingsManager] Version is out of date.");
            CreateNewlyFile();
            ReadContent();
        }
    }

    private bool ValidateVersion()
    {
#if !UNITY_WP8
        string fileVersion = m_Document.SelectSingleNode("/root/version").InnerText;
#endif
#if UNITY_WP8
        // string fileVersion = m_Document.XPathSelectElement("./root/version").Value;
        string fileVersion = m_Document.Descendants("root").Descendants("version").First().Value;
#endif
        Debug.Log("[GameSettingsManager] Version: " + fileVersion);
        return (string.Compare(fileVersion, m_Version) == 0);
    }

    private void UpdateFile()
    {
        Debug.Log("[HighscoreServ] Updating file content...");
        FileStream fileStream = null;
#if NETFX_CORE
        fileStream = File.Open(m_FilePath);
#else
        fileStream = File.Open(m_FilePath, FileMode.Create);
#endif
        StreamWriter sw = new StreamWriter(fileStream, Encoding.UTF8);
#if !UNITY_WP8
        sw.Write(m_Document.OuterXml);
#endif
#if UNITY_WP8
        sw.Write(m_Document.ToString());
#endif
        sw.Close();
        fileStream.Close();
        fileStream.Dispose();
        Debug.Log("[HighscoreServ] Update done.");
    }

    public bool GetBoolSetting(string name)
    {
        Debug.Log("[GameSettingsManager] Getting a bool settings named " + name + " ...");
        bool retVal = false;
#if !UNITY_WP8
        XmlNodeList settingsNodes = m_Document.SelectSingleNode("/root/settings").ChildNodes;
        foreach (XmlNode node in settingsNodes)
        {
            if (string.Compare(node.Name, name) == 0)
            {
                retVal = bool.Parse(node.InnerText);
                return retVal;
            }
        }
#endif
#if UNITY_WP8
        var settingsNodes = m_Document.Descendants("root").Descendants("settings").Descendants();
        foreach (XElement node in settingsNodes)
        {
            if (string.Compare(node.Name.LocalName, name) == 0)
            {
                retVal = bool.Parse(node.Value);
                Debug.Log("[GameSettingsManager] Value got: " + retVal);
                return retVal;
            }
        }
#endif
        Debug.Log("[GameSettingsManager] Error, no such gamesettings node.");
        return retVal;
    }

    public void ChangeBoolSetting(string name, bool newVal)
    {
        Debug.Log("[GameSettingsManager] Setting a new value for setting named " + name + " ...");
#if !UNITY_WP8
        XmlNodeList settingsNodes = m_Document.SelectSingleNode("/root/settings").ChildNodes;
        foreach (XmlNode node in settingsNodes)
        {
            if (string.Compare(node.Name, name) == 0)
            {
                node.InnerText = newVal.ToString();
                break;
            }
        }
#endif
#if UNITY_WP8
        var settingsNodes = m_Document.Descendants("root").Descendants("settings").Descendants();
        foreach (XElement node in settingsNodes)
        {
            if (string.Compare(node.Name.LocalName, name) == 0)
            {
                node.Value = newVal.ToString();
                break;
            }
        }
#endif
        UpdateFile();
        Debug.Log("[GameSettingsManager] Done.");
    }
}

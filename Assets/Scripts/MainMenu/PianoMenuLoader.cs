﻿using UnityEngine;
using System.Collections;

public class PianoMenuLoader : MonoBehaviour {

    [Header("Highscore")]
    public string HighscoreDataPath;
    [TextArea(5, 10)]
    public string DefaultContent;
    public string Version;

    [Header("Game Settings")]
    public string SettingsFileName;
    [TextArea(5, 10)]
    public string SettingsDefaultContent;
    public string SettingsVersion;

    public GameObject LoadingIndicatorGO;

    private void LoadGameSettings()
    {
        GameSettingsManager.Instance.Initialize(SettingsFileName, SettingsDefaultContent, SettingsVersion);
        if (GameSettingsManager.Instance.GetBoolSetting("sound"))
        {
            PlayerPrefs.SetInt("game_settings_sound", 1);
        }
        else
        {
            PlayerPrefs.SetInt("game_settings_sound", 0);
        }
    }

    private void SignalDoneLoading()
    {
        PlayerPrefs.SetInt("game_is_loaded", 1);
        BroadcastMessage("Initialize");
    }

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetInt("game_is_loaded") == 0)
        {
            LoadingIndicatorGO.SetActive(true);
            LBSettingsManager.Instance.Initialize();
            HighscoreDataPath = Application.persistentDataPath + "/highscore.xml";

            HighscoreServ.Instance.Initialize(HighscoreDataPath, DefaultContent, Version);
            LoadGameSettings();

            // pretent we have a maximum timeout when initializeing LBSettingsManager.
            Invoke("SignalDoneLoading", 2.0f);
        }
        else
        {
            BroadcastMessage("Initialize");
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
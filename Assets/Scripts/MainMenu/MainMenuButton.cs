﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (UIMeshButton))]
public class MainMenuButton : MonoBehaviour {

    [Header("Colors")]
    public GameObject SelectIndicatorGO;

    private UIMesh m_UIMesh;
    private UIMeshButton m_UIMeshButton;

    private void OnTapUpColorChange()
    {
        SelectIndicatorGO.SetActive(false);
    }

    private void OnTapDownColorChange()
    {
        SelectIndicatorGO.SetActive(true);
    }

    private void OnTapUpAsButtonColorChange()
    {
        SelectIndicatorGO.SetActive(false);
    }

    void Awake()
    {
        m_UIMesh = GetComponent<UIMesh>();
        m_UIMeshButton = GetComponent<UIMeshButton>();
        m_UIMeshButton.OnTapDownHandler += OnTapDownColorChange;
        m_UIMeshButton.OnTapUpHandler += OnTapUpColorChange;
        m_UIMeshButton.OnTapUpAsButtonHandler += OnTapUpAsButtonColorChange;
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class MainMenuLogic : MonoBehaviour {

    [Header("GO references")]
    public GameObject SettingsDialogGO;
    public UIKineticListView GameModeListMenuGO;

    private void OnOpenSettingsDialog()
    {
        GameModeListMenuGO.DisableInteraction = true;
        SettingsDialogGO.SetActive(true);
    }

    private void OnMenuClassicClicked()
    {
        // PlayerPrefs.SetInt("classic_mode_maxrow", 10);
        Application.LoadLevel("_PlayScene_ClassicMode_");
    }

    private void OnMenuArcadeClicked()
    {
        PlayerPrefs.SetString("ArcadeMode", "4k");
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }

    private void OnMenu6LanesClicked()
    {
        PlayerPrefs.SetString("ArcadeMode", "6k");
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }

    private void OpenArcade6kLeaderBoard()
    {
        PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.Arcade6kRecentBoardName);
        PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.Arcade6kAllTimeBoardName);
        PlayerPrefs.SetString("ToLB_GameMode", "arcade6k");
        PlayerPrefs.SetString("ToLB_ScoreType", "point");
        Application.LoadLevel("_Leaderboard_");
    }

    private void OpenArcade4kLeaderBoard()
    {
        PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.Arcade4kRecentBoardName);
        PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.Arcade4kAllTimeBoardName);
        PlayerPrefs.SetString("ToLB_GameMode", "arcade4k");
        PlayerPrefs.SetString("ToLB_ScoreType", "point");
        Application.LoadLevel("_Leaderboard_");
    }

    private void OpenClassicLeaderboard()
    {
        PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.ClassicRecentBoardName);
        PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.ClassicAllTimeBoardName);
        PlayerPrefs.SetString("ToLB_GameMode", "classic");
        PlayerPrefs.SetString("ToLB_ScoreType", "time");
        Application.LoadLevel("_Leaderboard_");
    }

    private void SettingsDialog_BackClicked()
    {
        GameModeListMenuGO.DisableInteraction = false;
    }

	// Use this for initialization
	void Start () {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        SettingsDialogGO.GetComponent<SettingsDialog>().OnBackListener = SettingsDialog_BackClicked;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

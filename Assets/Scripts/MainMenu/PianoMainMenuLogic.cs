﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PianoMenuItem
{
    [TextArea(1, 2)]
    public string Text;
    public string EventName;
    public bool Switchable;
    public Color TileFirstColor;
    public Color TileSecondColor;
    public Color TextColor;
}

[System.Serializable]
public struct PianoMenuLevel
{
    public string MenuLevelName;
    public PianoMenuItem[] MenuItems;
}

public class PianoMainMenuLogic : Singleton<PianoMainMenuLogic> {

    public GameObject PrefabMenuItem;
    public MenuItemTilesController MenuItemTilesControllerGO;
    public TouchHandlerComponent TouchHandlerGO;
    public MessageBox MessageBoxGO;
    public GameObject NavigationIndicatorGO;
    public GameObject NavigationToLeftGO;
    public GameObject NavigationToRightGO;

    public IconRegionController PlaceholderGO;
    public MenuItemTilesController MainMenu_Level1GO;
    public MenuItemTilesController[] PlayMenu_Level2GO;
    public int CurrentLevel2MenuId = 0;
    public MenuItemTilesController ClassicMenu_Level3GO;
    public MenuItemTilesController ArcadeMenu_Level3GO;
    public MenuItemTilesController FlipMenu_Level3GO;
    public MenuItemTilesController ZenMenu_Level3GO;
    public GameObject UILoadingIndicatorGO;
    public GameObject SettingsRibbonRootGO;

    public float MenuMovingTime;
    public PianoMenuLevel MenuLevel1;
    public PianoMenuLevel PlayMenuLevel2;
    public PianoMenuLevel PlayMenuLevel2_2;
    public PianoMenuLevel ClassicMenuLevel3;
    public PianoMenuLevel ArcadeMenuLevel3;
    public PianoMenuLevel FlipMenuLevel3;
    public PianoMenuLevel ZenMenuLevel3;
    
    private int m_CurrentLevel;
    private MenuItemTilesController[] m_CurrentState;
    private float m_TileHeight;

    #region MenuLevel1
    private void MainMenu_Play_Tapped()
    {
        Debug.Log("MainMenu_Play_Tapped()");
        if (m_CurrentLevel == 0)
        {
            PlayMenu_Level2GO[CurrentLevel2MenuId].gameObject.SetActive(true);
            m_CurrentState[m_CurrentLevel + 1] = PlayMenu_Level2GO[CurrentLevel2MenuId];
            ChangeMenuLevel(1);
            m_CurrentLevel = 1;
        }
        else {
            ChangeMenuLevel(0);
            m_CurrentLevel = 0;
            m_CurrentState[m_CurrentLevel].ResetColors();
        }
    }

    private void RatingTappedDelay()
    {
        Debug.Log("MainMenu_Rating_Tapped()");
        AdsServ.Instance.DoRating();
    }
    private void MainMenu_Rating_Tapped()
    {
        if (m_CurrentLevel == 0)
        {
            RatingTappedDelay();
        }
        else
        {
            // invoke with delay
            ChangeMenuLevel(0);
            m_CurrentLevel = 0;
            m_CurrentState[m_CurrentLevel].ResetColors();
            Invoke("RatingTappedDelay", MenuMovingTime);
        }
    }

    private void MoreGamesTappedDelay()
    {
        Debug.Log("MainMenu_MoreGames_Tapped()");
        // Application.OpenURL(LBSettingsManager.Instance.GetMoreGamesURL());
        AdsServ.Instance.ShowMoreApps();
    }
    private void MainMenu_MoreGames_Tapped()
    {
        
        if (m_CurrentLevel == 0)
        {
            MoreGamesTappedDelay();
        }
        else
        {
            // invoke with delay
            ChangeMenuLevel(0);
            m_CurrentLevel = 0;
            m_CurrentState[m_CurrentLevel].ResetColors();
            Invoke("MoreGamesTappedDelay", MenuMovingTime);
        }
    }


    private void YesResult()
    {
        Application.Quit();
    }
    private void ExitTappedDelay()
    {
        Debug.Log("MainMenu_Exit_Tapped()");
        MessageBoxGO.OnResultYesDelegate = YesResult;
        MessageBoxGO.gameObject.SetActive(true);
        MessageBoxGO.Show("Quit Application?");
    }
    private void MainMenu_Exit_Tapped()
    {
        if (m_CurrentLevel == 0)
        {
            ExitTappedDelay();
        }
        else
        {
            // invoke with delay
            ChangeMenuLevel(0);
            m_CurrentLevel = 0;
            m_CurrentState[m_CurrentLevel].ResetColors();
            Invoke("ExitTappedDelay", MenuMovingTime);
        }
    }
    #endregion

    #region MenuLevel2
    public void MenuLevel2SlideLeft()
    {
        Debug.Log("MenuLevel2SlideLeft");
        NavigationToRightGO.SetActive(false);
        NavigationToLeftGO.SetActive(true);
        if (CurrentLevel2MenuId + 1 <= 1 && m_CurrentLevel == 1)
        {
            TouchHandlerGO.EnableInteraction = false;
            Invoke("ReEnableInteraction", MenuMovingTime);
            PlayMenu_Level2GO[CurrentLevel2MenuId].ShiftMenuLeft(true, false);
            PlayMenu_Level2GO[CurrentLevel2MenuId + 1].gameObject.SetActive(true);
            PlayMenu_Level2GO[CurrentLevel2MenuId+1].ShiftMenuLeft(false, true);
            CurrentLevel2MenuId++;
            m_CurrentState[m_CurrentLevel] = PlayMenu_Level2GO[CurrentLevel2MenuId];
        }
    }

    public void MenuLevel2SlideRight()
    {
        Debug.Log("MenuLevel2SlideRight");
        NavigationToRightGO.SetActive(true);
        NavigationToLeftGO.SetActive(false);
        if (CurrentLevel2MenuId - 1 >= 0 && m_CurrentLevel == 1)
        {
            TouchHandlerGO.EnableInteraction = false;
            Invoke("ReEnableInteraction", MenuMovingTime);
            PlayMenu_Level2GO[CurrentLevel2MenuId].ShiftMenuRight(true, false);
            PlayMenu_Level2GO[CurrentLevel2MenuId - 1].gameObject.SetActive(true);
            PlayMenu_Level2GO[CurrentLevel2MenuId - 1].ShiftMenuRight(false, true);
            CurrentLevel2MenuId--;
            m_CurrentState[m_CurrentLevel] = PlayMenu_Level2GO[CurrentLevel2MenuId];
        }
    }

    private void PlayMenu_Classic_Tapped()
    {
        Debug.Log("PlayMenu_Classic_Tapped()");
        if (m_CurrentLevel == 1)
        {
            ClassicMenu_Level3GO.gameObject.SetActive(true);
            m_CurrentState[m_CurrentLevel + 1] = ClassicMenu_Level3GO;
            ChangeMenuLevel(2);
            m_CurrentLevel = 2;
        }
        else if (m_CurrentLevel == 2)
        {
            if (!ClassicMenu_Level3GO.gameObject.activeInHierarchy)
            {
                m_CurrentState[m_CurrentLevel].ShiftMenuDown(1, true);
                ClassicMenu_Level3GO.gameObject.SetActive(true);
                m_CurrentState[m_CurrentLevel] = ClassicMenu_Level3GO;
            }
            else
            {
                ChangeMenuLevel(1);
                m_CurrentLevel = 1;
                m_CurrentState[m_CurrentLevel].ResetColors();
            }
        }
    }

    private void PlayMenu_Arcade_Tapped()
    {
        Debug.Log("PlayMenu_Arcade_Tapped()");
        if (m_CurrentLevel == 1)
        {
            ArcadeMenu_Level3GO.gameObject.SetActive(true);
            m_CurrentState[m_CurrentLevel + 1] = ArcadeMenu_Level3GO;
            ChangeMenuLevel(2);
            m_CurrentLevel = 2;
        }
        else if (m_CurrentLevel == 2)
        {
            if (!ArcadeMenu_Level3GO.gameObject.activeInHierarchy)
            {
                m_CurrentState[m_CurrentLevel].ShiftMenuDown(1, true);
                ArcadeMenu_Level3GO.gameObject.SetActive(true);
                m_CurrentState[m_CurrentLevel] = ArcadeMenu_Level3GO;
            }
            else
            {
                ChangeMenuLevel(1);
                m_CurrentLevel = 1;
                m_CurrentState[m_CurrentLevel].ResetColors();
            }
        }
    }

    private void BlinkTappedDelay()
    {
        AnalyticsServ.LogScreen("Play BlinkMode");
        PlayerPrefs.SetString("ArcadeMode", "4k");
        PlayerPrefs.SetInt("IsMaster", 0);
        PlayerPrefs.SetInt("HasLightning", 1);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }
    private void PlayMenu_Blink_Tapped()
    {
        if (m_CurrentLevel == 2)
        {
            ChangeMenuLevel(1);
            m_CurrentLevel = 1;
            // m_CurrentState[m_CurrentLevel].ResetColors();
            Invoke("BlinkTappedDelay", MenuMovingTime);
        }
        else BlinkTappedDelay();
    }

    private void FreezeTappedDelay()
    {
        AnalyticsServ.LogScreen("Play FreezeMode");
        Application.LoadLevel("_PlayScene_FrozenMode_");
    }
    private void PlayMenu_Freeze_Tapped()
    {
        if (m_CurrentLevel == 2)
        {
            ChangeMenuLevel(1);
            m_CurrentLevel = 1;
           //  m_CurrentState[m_CurrentLevel].ResetColors();
            Invoke("FreezeTappedDelay", MenuMovingTime);
        }
        else FreezeTappedDelay();
    }
    private void PlayMenu_Flip_Tapped()
    {
        Debug.Log("PlayMenu_Flip_Tapped()");
        if (m_CurrentLevel == 1)
        {
            FlipMenu_Level3GO.gameObject.SetActive(true);
            m_CurrentState[m_CurrentLevel + 1] = FlipMenu_Level3GO;
            ChangeMenuLevel(2);
            m_CurrentLevel = 2;
        }
        else if (m_CurrentLevel == 2)
        {
            if (!FlipMenu_Level3GO.gameObject.activeInHierarchy)
            {
                m_CurrentState[m_CurrentLevel].ShiftMenuDown(1, true);
                FlipMenu_Level3GO.gameObject.SetActive(true);
                m_CurrentState[m_CurrentLevel] = FlipMenu_Level3GO;
            }
            else
            {
                ChangeMenuLevel(1);
                m_CurrentLevel = 1;
                m_CurrentState[m_CurrentLevel].ResetColors();
            }
        }
    }
    private void PlayMenu_Zen_Tapped()
    {
        Debug.Log("PlayMenu_Zen_Tapped()");
        if (m_CurrentLevel == 1)
        {
            ZenMenu_Level3GO.gameObject.SetActive(true);
            m_CurrentState[m_CurrentLevel + 1] = ZenMenu_Level3GO;
            ChangeMenuLevel(2);
            m_CurrentLevel = 2;
        }
        else if (m_CurrentLevel == 2)
        {
            if (!ZenMenu_Level3GO.gameObject.activeInHierarchy)
            {
                m_CurrentState[m_CurrentLevel].ShiftMenuDown(1, true);
                ZenMenu_Level3GO.gameObject.SetActive(true);
                m_CurrentState[m_CurrentLevel] = ZenMenu_Level3GO;
            }
            else
            {
                ChangeMenuLevel(1);
                m_CurrentLevel = 1;
                m_CurrentState[m_CurrentLevel].ResetColors();
            }
        }
    }
    #endregion

    #region ClassicMode
    private void ClassicMenu_20_Tapped()
    {
        PlayerPrefs.SetInt("classic_mode_maxrow", 20);
        Application.LoadLevel("_PlayScene_ClassicMode_");
    }

    private void ClassicMenu_35_Tapped()
    {
        PlayerPrefs.SetInt("classic_mode_maxrow", 35);
        Application.LoadLevel("_PlayScene_ClassicMode_");
    }

    private void ClassicMenu_50_Tapped()
    {
        PlayerPrefs.SetInt("classic_mode_maxrow", 50);
        Application.LoadLevel("_PlayScene_ClassicMode_");
    }

    private void ClassicMenu_75_Tapped()
    {
        PlayerPrefs.SetInt("classic_mode_maxrow", 75);
        Application.LoadLevel("_PlayScene_ClassicMode_");
    }
    #endregion

    #region ArcadeMode
    private void ArcadeMenu_4k_Tapped()
    {
        AnalyticsServ.LogScreen("Play Arcade4kMode");
        PlayerPrefs.SetString("ArcadeMode", "4k");
        PlayerPrefs.SetInt("IsMaster", 0);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }

    private void ArcadeMenu_5k_Tapped()
    {
        AnalyticsServ.LogScreen("Play Arcade4kMasterMode");
        PlayerPrefs.SetString("ArcadeMode", "4k");
        PlayerPrefs.SetInt("IsMaster", 1);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }

    private void ArcadeMenu_6k_Tapped()
    {
        AnalyticsServ.LogScreen("Play Arcade6kMode");
        PlayerPrefs.SetString("ArcadeMode", "6k");
        PlayerPrefs.SetInt("IsMaster", 0);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }

    private void ArcadeMenu_8k_Tapped()
    {
        AnalyticsServ.LogScreen("Play Arcade6kMasterMode");
        PlayerPrefs.SetString("ArcadeMode", "6k");
        PlayerPrefs.SetInt("IsMaster", 1);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }
    #endregion

    #region FlipMode
    private void FlipMenu_Flip4_Tapped()
    {
        Debug.Log("FlipMenu_Flip4_Tapped");
        AnalyticsServ.LogScreen("Play Flip4Mode");
        PlayerPrefs.SetString("ArcadeMode", "4k");
        PlayerPrefs.SetInt("IsMaster", 0);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 1);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }
    private void FlipMenu_Flip4Master_Tapped()
    {
        Debug.Log("FlipMenu_Flip4Master_Tapped");
        AnalyticsServ.LogScreen("Play Flip4MasterMode");
        PlayerPrefs.SetString("ArcadeMode", "4k");
        PlayerPrefs.SetInt("IsMaster", 1);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 1);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }
    private void FlipMenu_Flip6_Tapped()
    {
        Debug.Log("FlipMenu_Flip6_Tapped");
        AnalyticsServ.LogScreen("Play Flip6Mode");
        PlayerPrefs.SetString("ArcadeMode", "6k");
        PlayerPrefs.SetInt("IsMaster", 0);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 1);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }
    private void FlipMenu_Flip6Master_Tapped()
    {
        Debug.Log("FlipMenu_Flip6Master_Tapped");
        AnalyticsServ.LogScreen("Play Flip6MasterMode");
        PlayerPrefs.SetString("ArcadeMode", "6k");
        PlayerPrefs.SetInt("IsMaster", 1);
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 1);
        Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
    }
    #endregion

    #region MirrorMode
    private void MirrorTappedDelay()
    {
        AnalyticsServ.LogScreen("Play MirrorMode");
        Application.LoadLevel("_PlayScene_MirrorMode_");
    }
    private void PlayMenu_Mirror_Tapped()
    {
        if (m_CurrentLevel == 2)
        {
            ChangeMenuLevel(1);
            m_CurrentLevel = 1;
            //  m_CurrentState[m_CurrentLevel].ResetColors();
            Invoke("MirrorTappedDelay", MenuMovingTime);
        }
        else MirrorTappedDelay();
    }
    #endregion

    #region DualMode
    private void DualTappedDelay()
    {
        AnalyticsServ.LogScreen("Play DualMode");
        Application.LoadLevel("_PlayScene_Parallel_");
    }
    private void PlayMenu_Dual_Tapped()
    {
        if (m_CurrentLevel == 2)
        {
            ChangeMenuLevel(1);
            m_CurrentLevel = 1;
            Invoke("DualTappedDelay", MenuMovingTime);
        }
        else DualTappedDelay();
    }
    #endregion

    #region ZenMode
    private void ZenMenu_Zen7_Tapped()
    {
        Debug.Log("ZenMenu_Zen7_Tapped");
        AnalyticsServ.LogScreen("Play Zen7Mode");
        PlayerPrefs.SetInt("ZenPlayTime", 7);
        Application.LoadLevel("_PlayScene_ZenMode_");
    }

    private void ZenMenu_Zen9_Tapped()
    {
        Debug.Log("ZenMenu_Zen9_Tapped");
        AnalyticsServ.LogScreen("Play Zen9Mode");
        PlayerPrefs.SetInt("ZenPlayTime", 9);
        Application.LoadLevel("_PlayScene_ZenMode_");
    }

    private void ZenMenu_Zen12_Tapped()
    {
        Debug.Log("ZenMenu_Zen12_Tapped");
        AnalyticsServ.LogScreen("Play Zen12Mode");
        PlayerPrefs.SetInt("ZenPlayTime", 12);
        Application.LoadLevel("_PlayScene_ZenMode_");
    }

    private void ZenMenu_Zen15_Tapped()
    {
        Debug.Log("ZenMenu_Zen15_Tapped");
        AnalyticsServ.LogScreen("Play Zen15Mode");
        PlayerPrefs.SetInt("ZenPlayTime", 15);
        Application.LoadLevel("_PlayScene_ZenMode_");
    }
    #endregion

    private void ReEnableInteraction()
    {
        TouchHandlerGO.EnableInteraction = true;
    }

    private void ShowIndicator()
    {
        NavigationIndicatorGO.SetActive(true);
    }

    private void ChangeMenuLevel(int level)
    {
        TouchHandlerGO.EnableInteraction = false;
        Invoke("ReEnableInteraction", MenuMovingTime);
        if (level == 1)
            Invoke("ShowIndicator", MenuMovingTime);
        else NavigationIndicatorGO.SetActive(false);
        switch (level)
        {
            case 0:
                {
                    m_CurrentState[0].IsFocus = true;
                    TouchHandlerGO.DisableDragging();
                    if (m_CurrentLevel == 1)
                    {
                        PlaceholderGO.ShiftDown(1 * m_TileHeight);
                        m_CurrentState[1].ShiftMenuDown(1, true);
                        m_CurrentState[0].ShiftMenuDown(1, false);
                    }
                    else if (m_CurrentLevel == 2)
                    {
                        PlaceholderGO.ShiftDown(2 * m_TileHeight);
                        m_CurrentState[2].ShiftMenuDown(2, true);
                        m_CurrentState[1].ShiftMenuDown(2, true);
                        m_CurrentState[0].ShiftMenuDown(2, false);
                    }
                    break;
                }
            case 1:
                {
                    m_CurrentState[1].IsFocus = true;
                    TouchHandlerGO.EnableDragging(-5.0f + m_TileHeight, -5.0f);
                    if (m_CurrentLevel == 0)
                    {
                        PlaceholderGO.ShiftUp(1 * m_TileHeight);
                        m_CurrentState[0].ShiftMenuUp(1);
                    }
                    else if (m_CurrentLevel == 2)
                    {
                        PlaceholderGO.ShiftDown(1 * m_TileHeight);
                        m_CurrentState[2].ShiftMenuDown(1, true);
                        m_CurrentState[1].ShiftMenuDown(1, false);
                        m_CurrentState[0].ShiftMenuDown(1, false);
                    }
                    break;
                }
            case 2:
                {
                    m_CurrentState[2].IsFocus = true;
                    // TouchHandlerGO.EnableDragging(-5.0f + m_TileHeight * 2.0f, -5.0f + m_TileHeight);
                    TouchHandlerGO.DisableDragging();
                    if (m_CurrentLevel == 1)
                    {
                        PlaceholderGO.ShiftUp(1 * m_TileHeight);
                        m_CurrentState[0].ShiftMenuUp(1);
                        m_CurrentState[1].ShiftMenuUp(1);
                    }
                    break;
                }
        }
    }

    private void CreateMenu(MenuItemTilesController target, PianoMenuLevel menu, bool hidden)
    {
        for (int i = 0; i < menu.MenuItems.Length; i++)
        {
            GameObject obj = Instantiate(PrefabMenuItem) as GameObject;
            obj.GetComponent<UITransparentButton>().TargetGameObject = gameObject;
            obj.GetComponent<UITransparentButton>().TapUpAsButtonMessageName = menu.MenuItems[i].EventName;
            obj.GetComponent<MenuItemTile>().SetColor(menu.MenuItems[i].TileFirstColor, menu.MenuItems[i].TileSecondColor,
                menu.MenuItems[i].TextColor);
            obj.GetComponent<MenuItemTile>().SetOrgColor(menu.MenuItems[i].TileFirstColor, menu.MenuItems[i].TileSecondColor);
            obj.GetComponent<MenuItemTile>().SetText(menu.MenuItems[i].Text);
            obj.GetComponent<MenuItemTile>().Switchable = menu.MenuItems[i].Switchable;
            obj.transform.parent = target.transform;
            target.AddItem(0, obj.GetComponent<MenuItemTile>());
        }

        if (hidden)
            target.gameObject.SetActive(false);
        else target.gameObject.SetActive(true);
    }

    private void Initialize()
    {
        TouchHandlerGO.OnSlideLeft = MenuLevel2SlideLeft;
        TouchHandlerGO.OnSlideRight = MenuLevel2SlideRight;
        m_CurrentState = new MenuItemTilesController[3];
        // PlayMenu_Level2GO = new MenuItemTilesController[2];
        CreateMenu(MainMenu_Level1GO, MenuLevel1, true);
        CreateMenu(PlayMenu_Level2GO[0], PlayMenuLevel2, true);
        CreateMenu(PlayMenu_Level2GO[1], PlayMenuLevel2_2, true);
        CreateMenu(ClassicMenu_Level3GO, ClassicMenuLevel3, true);
        CreateMenu(ArcadeMenu_Level3GO, ArcadeMenuLevel3, true);
        CreateMenu(FlipMenu_Level3GO, FlipMenuLevel3, true);
        CreateMenu(ZenMenu_Level3GO, ZenMenuLevel3, true);

        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float pixelSize = floatHeight / Camera.main.pixelHeight;
        m_TileHeight = (floatHeight - (4 + 1) * pixelSize) / 4;

        Vector3 naviPos = NavigationIndicatorGO.transform.localPosition;
        naviPos.y = - 5.0f + m_TileHeight * 0.75f + pixelSize;
        NavigationIndicatorGO.transform.localPosition = naviPos;

        Invoke("LeadIn", 1.0f);
        Invoke("ShowAppOpen", 1.0f + MenuMovingTime);
    }

    private void LeadIn()
    {
        m_CurrentLevel = 0;
        m_CurrentState[0] = MainMenu_Level1GO;
        MainMenu_Level1GO.IsFocus = true;
        UILoadingIndicatorGO.SetActive(false);
        SettingsRibbonRootGO.SetActive(true);
        MainMenu_Level1GO.gameObject.SetActive(true);
        PlaceholderGO.ShiftUp(1 * m_TileHeight);
    }

    private void ShowAppOpen()
    {
        Debug.Log("ShowAppOpen()");
        AdsServ.Instance.ShowAppOpen();
    }

    void Awake() {
        PlayerPrefs.SetInt("ReverseActivated", 0);
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        AnalyticsServ.LogScreen("MainMenu");
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            MainMenu_Exit_Tapped();
	}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuItemTilesController : MonoBehaviour {

    public float MovingTime;
    public AnimationCurve MovingCurve;

    public bool IsFocus = false;
    private List<MenuItemTile> m_MenuItemTiles;

    private float m_MostLeft;
    private float m_StartY;
    private float m_PixelSize;
    private float m_TileHeight;
    private float m_TileWidth;

    public void ResetColors()
    {
        for (int i = 0; i < m_MenuItemTiles.Count; i++)
        {
            m_MenuItemTiles[i].SetColor(m_MenuItemTiles[i].TileOrgFirstColor,
                        m_MenuItemTiles[i].TileOrgSecondColor,
                        m_MenuItemTiles[i].TextColor);
        }
    }

    private void OnMenuTileClicked(object[] param)
    {
        MenuItemTile itemTile = (MenuItemTile)param[0];
        bool switchable = (bool)param[1];
        if (IsFocus)
        {
            for (int i = 0; i < m_MenuItemTiles.Count; i++)
            {
                if (m_MenuItemTiles[i] != itemTile)
                {
                    if (switchable)
                    {
                        Color newColor = m_MenuItemTiles[i].TileOrgSecondColor;
                        m_MenuItemTiles[i].SetColor(newColor, newColor,
                            m_MenuItemTiles[i].TextColor);
                    }
                    // m_MenuItemTiles[i].GetComponent<UITappable>().IsEnabled = false;
                }
            }
            IsFocus = false;
        }
        else if (switchable)
        {
            for (int i = 0; i < m_MenuItemTiles.Count; i++)
            {
                if (m_MenuItemTiles[i] != itemTile)
                {
                    m_MenuItemTiles[i].SetColor(m_MenuItemTiles[i].TileOrgSecondColor,
                        m_MenuItemTiles[i].TileOrgSecondColor,
                        m_MenuItemTiles[i].TextColor);
                }
                else
                {
                    m_MenuItemTiles[i].SetColor(m_MenuItemTiles[i].TileOrgFirstColor,
                        m_MenuItemTiles[i].TileOrgSecondColor,
                        m_MenuItemTiles[i].TextColor);
                }
            }
            // IsFocus = true;
        }
    }

    private IEnumerator ShiftMenuUpRoutine(int lineCount)
    {
        yield return new WaitForEndOfFrame();
        float startTime = Time.time;
        float t = 0;
        float startY = transform.position.y;
        float destY = startY + lineCount * (m_TileHeight + m_PixelSize);
        Vector3 pos = transform.position;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            pos.y = startY * (1 - v) + destY * v;
            transform.position = pos;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator ShiftMenuDownRoutine(int lineCount, bool disableWhenDone)
    {
        yield return new WaitForEndOfFrame();
        float startTime = Time.time;
        float t = 0;
        float startY = transform.position.y;
        float destY = startY - lineCount * (m_TileHeight + m_PixelSize);
        Vector3 pos = transform.position;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            pos.y = startY * (1 - v) + destY * v;
            transform.position = pos;
            yield return null;
        }
        if (disableWhenDone)
            gameObject.SetActive(false);
        yield return null;
    }

    private IEnumerator ShiftMenuLeftRoutine(bool disableWhenDone, bool trueIsIn)
    {
        yield return new WaitForEndOfFrame();
        float startTime = Time.time;
        float t = 0;
        float startX;
        if (trueIsIn)
            startX = Camera.main.orthographicSize * Camera.main.aspect * 2.0f;
        else startX = 0.0f;
        transform.localPosition = new Vector3(startX, -5.0f + m_TileHeight  + m_PixelSize, transform.localPosition.z);
        float destX = startX - Camera.main.orthographicSize * Camera.main.aspect * 2.0f;
        Vector3 pos = transform.localPosition;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            pos.x = startX * (1 - v) + destX * v;
            transform.localPosition = pos;
            yield return null;
        }
        if (disableWhenDone)
            gameObject.SetActive(false);
        yield return null;
    }

    private IEnumerator ShiftMenuRightRoutine(bool disableWhenDone, bool trueIsIn)
    {
        yield return new WaitForEndOfFrame();
        float startTime = Time.time;
        float t = 0;
        float startX;
        if (trueIsIn)
            startX = -Camera.main.orthographicSize * Camera.main.aspect * 2.0f;
        else startX = 0.0f;
        transform.localPosition = new Vector3(startX, -5.0f + m_TileHeight + m_PixelSize, transform.localPosition.z);
        float destX = startX + Camera.main.orthographicSize * Camera.main.aspect * 2.0f;
        Vector3 pos = transform.localPosition;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            pos.x = startX * (1 - v) + destX * v;
            transform.localPosition = pos;
            yield return null;
        }
        if (disableWhenDone)
            gameObject.SetActive(false);
        yield return null;
    }

    public void AddItem(int level, MenuItemTile itemTile)
    {
        if (m_MenuItemTiles == null)
            m_MenuItemTiles = new List<MenuItemTile>();
        
        m_MenuItemTiles.Add(itemTile);
        itemTile.ControllerGO = this;
    }

    private void CalculateConstraints()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        m_PixelSize = floatHeight / Camera.main.pixelHeight;
        m_MostLeft = -floatWidth / 2.0f;

        m_TileHeight = (floatHeight - (4 + 1) * m_PixelSize) / 4;
        m_TileWidth = (floatWidth - (4 + 1) * m_PixelSize) / 4;
        m_StartY = - m_TileHeight / 2.0f;
    }

    private void PositionMenuItems()
    {
        int line = 0;
        int c = 0;
        for (int j = 0; j < m_MenuItemTiles.Count; j++)
        {
            if (c == 4)
            {
                c = 0;
                line++;
            }

            MenuItemTile item = m_MenuItemTiles[j];
            item.gameObject.transform.localPosition = new Vector3(
                m_MostLeft + m_PixelSize + m_TileWidth / 2.0f + c * (m_TileWidth + m_PixelSize),
                m_StartY - line * (m_TileHeight + m_PixelSize),
                0.0f
            );
            c++;
        }
    }

    private void Initialize()
    {
        CalculateConstraints();
        PositionMenuItems();
    }

    public void ShiftMenuUp(int lineCount)
    {
        StartCoroutine(ShiftMenuUpRoutine(lineCount));
    }

    public void ShiftMenuDown(int lineCount, bool disableWhenDone)
    {
        StartCoroutine(ShiftMenuDownRoutine(lineCount, disableWhenDone));
    }

    public void ShiftMenuLeft(bool disableWhenDone, bool trueIsIn)
    {
        StartCoroutine(ShiftMenuLeftRoutine(disableWhenDone, trueIsIn));
    }

    public void ShiftMenuRight(bool disableWhenDone, bool trueIsIn)
    {
        StartCoroutine(ShiftMenuRightRoutine(disableWhenDone, trueIsIn));
    }

    void OnDisable()
    {
        for (int i = 0; i < m_MenuItemTiles.Count; i++)
        {
            Color firstColor = m_MenuItemTiles[i].TileOrgFirstColor;
            Color secondColor = m_MenuItemTiles[i].TileOrgSecondColor;
            m_MenuItemTiles[i].SetColor(firstColor, secondColor, m_MenuItemTiles[i].TextColor);
            m_MenuItemTiles[i].GetComponent<UITappable>().IsEnabled = true;
        }
    }

    void OnEnable()
    {
        transform.position = new Vector3(0.0f, -5.0f, 0.0f);
        ShiftMenuUp(1);
    }

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {

	}
}
﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MenuItemTileMesh))]
[RequireComponent (typeof(UITransparentButton))]
public class MenuItemTile : MonoBehaviour {
    public TextMesh TextGO;
    public MenuItemTilesController ControllerGO;

    [Header("Screen configurations")]
    public int TilePerRow;
    public int RowPerScreen;

    public Color TileOrgFirstColor;
    public Color TileFirstColor;
    public Color TileOrgSecondColor;
    public Color TileSecondColor;
    public Color TextColor;
    public bool Switchable = false;

    private MenuItemTileMesh m_Tile;

    private float m_PixelSize;
    private float m_ScreenMostLeft;
    private float m_TileHeight;
    private float m_TileWidth;

    public delegate void OnMenuTappedDelegate(MenuItemTile tappedMenuTile);
    public OnMenuTappedDelegate OnMenuTappedListener;

    private void ComputeTileSize()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;
        m_PixelSize = pixelSize;
        m_ScreenMostLeft = -floatWidth / 2.0f;

        m_TileHeight = (floatHeight - (RowPerScreen + 1) * pixelSize) / RowPerScreen;
        m_TileWidth = (floatWidth - (TilePerRow + 1) * pixelSize) / TilePerRow;

        GetComponent<UITappable>().SetTapRegion(new Vector2(m_TileWidth, m_TileHeight));
    }

    public void SetColor(Color firstColor, Color secondColor, Color textColor)
    {
        TileFirstColor = firstColor;
        TileSecondColor = secondColor;
        TextColor = textColor;
        if (m_Tile == null)
            m_Tile = GetComponent<MenuItemTileMesh>();
        m_Tile.SetColor(firstColor, secondColor);
    }

    public void SetOrgColor(Color firstColor, Color secondColor)
    {
        TileOrgFirstColor = firstColor;
        TileOrgSecondColor = secondColor;
    }

    public void SetText(string text)
    {
        TextGO.text = text;
    }

    private void OnTapUpAsButtonDelegate()
    {
        if (OnMenuTappedListener != null)
            OnMenuTappedListener(this);
        // if (Switchable)
        {
            if (ControllerGO != null)
                ControllerGO.gameObject.SendMessage("OnMenuTileClicked", new object[] {this, Switchable});
        }
    }

    void Awake()
    {
        m_Tile = GetComponent<MenuItemTileMesh>();
        ComputeTileSize();
        m_Tile.Width = m_TileWidth;
        m_Tile.Height = m_TileHeight;
    }

	// Use this for initialization
	void Start () {
        m_Tile.GenerateTileMesh(TileFirstColor, TileSecondColor);
        TextGO.color = TextColor;
	}
	
	// Update is called once per frame
	void Update () {
	}
}

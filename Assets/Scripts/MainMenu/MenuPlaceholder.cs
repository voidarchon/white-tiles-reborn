﻿using UnityEngine;
using System.Collections;

public class MenuPlaceholder : MonoBehaviour {

    public float MovingTime;
    public AnimationCurve MovingCurve;

    private float m_PixelSize;
    private float m_TileHeight;
    private float m_TileWidth;

    private IEnumerator ShiftUpRoutine(int lineCount)
    {
        yield return new WaitForEndOfFrame();
        float startTime = Time.time;
        float t = 0;
        float startY = transform.position.y;
        float destY = startY + lineCount * (m_TileHeight + m_PixelSize);
        Vector3 pos = transform.position;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            pos.y = startY * (1 - v) + destY * v;
            transform.position = pos;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator ShiftDownRoutine(int lineCount)
    {
        yield return new WaitForEndOfFrame();
        float startTime = Time.time;
        float t = 0;
        float startY = transform.position.y;
        float destY = startY - lineCount * (m_TileHeight + m_PixelSize);
        Vector3 pos = transform.position;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            pos.y = startY * (1 - v) + destY * v;
            transform.position = pos;
            yield return null;
        }
        yield return null;
    }

    private void ComputeConstraints()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;
        m_PixelSize = pixelSize;

        m_TileHeight = (floatHeight - (4 + 1) * pixelSize) / 4;
        m_TileWidth = (floatWidth - (4 + 1) * pixelSize) / 4;
    }

    private void SelfPosition()
    {
        transform.position = new Vector3(0.0f, 2.0f * m_PixelSize + m_TileHeight, 0.0f);
    }

    public void ShiftUp(int lineCount)
    {
        StartCoroutine(ShiftUpRoutine(lineCount));
    }

    public void ShiftDown(int lineCount)
    {
        StartCoroutine(ShiftDownRoutine(lineCount));
    }


    void Awake()
    {
        ComputeConstraints();
        SelfPosition();
    }

	// Use this for initialization
	void Start () {
        // ShiftUp(1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class IconRegionController : MonoBehaviour {

    public float TopLimit;
    public float BotLimit;
    public float MinDistance;
    public float MovingTime;
    public AnimationCurve MovingCurve;

    private float m_CurrDistance;

    private IEnumerator ShiftUpRoutine(float length)
    {
        BotLimit += length;
        float start = m_CurrDistance;
        float dest = (TopLimit + BotLimit) / 2.0f;
        // Debug.Log(dest);
        float startTime = Time.time;
        float t = 0.0f;
        Vector3 pos = transform.localPosition;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            m_CurrDistance = start * (1.0f - v) + dest * v;
            pos.y = m_CurrDistance;
            transform.localPosition = pos;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator ShiftDownRoutine(float length)
    {
        BotLimit -= length;
        float start = m_CurrDistance;
        float dest = (TopLimit + BotLimit) / 2.0f;
        float startTime = Time.time;
        float t = 0.0f;
        Vector3 pos = transform.localPosition;
        while (t < MovingTime)
        {
            t = Time.time - startTime;
            float v = MovingCurve.Evaluate(t / MovingTime);
            m_CurrDistance = start * (1.0f - v) + dest * v;
            pos.y = m_CurrDistance;
            transform.localPosition = pos;
            yield return null;
        }
        yield return null;
    }

    public void ShiftUp(float length)
    {
        StartCoroutine(ShiftUpRoutine(length));
    }

    public void ShiftDown(float length)
    {
        StartCoroutine(ShiftDownRoutine(length));
    }

    void Awake()
    {
        
    }

	// Use this for initialization
	void Start () {
        m_CurrDistance = (TopLimit + BotLimit) / 2.0f;
	}
	
	// Update is called once per frame
    void Update()
    {
    }
}
﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class MenuItemTileMesh : MonoBehaviour {

    [Header("Size of Tile")]
    public float Width;
    public float Height;
    public float FooterPercent;

    // Tile's mesh data
    private Vector3[] m_Vertices;
    private Vector2[] m_UVs;
    private int[] m_Indices;
    private Color[] m_VertexColors;
    private Mesh m_Mesh;

    // Cached components
    private MeshFilter m_MeshFilter;

    public void GenerateTileMesh(Color firstColor, Color secondColor)
    {
        // prevent memory leak
        if (m_Mesh != null)
        {
            SetColor(firstColor, secondColor);
            return;
        }

        // float z = transform.localPosition.z;
        m_Vertices = new Vector3[] {
            new Vector3(-Width/2.0f, -Height/2.0f, 0.0f),
            new Vector3(Width/2.0f, -Height/2.0f, 0.0f),
            new Vector3(-Width/2.0f, -Height/2.0f + Height * FooterPercent, 0.0f),
            new Vector3(Width/2.0f, -Height/2.0f + Height * FooterPercent, 0.0f),
            new Vector3(-Width/2.0f, -Height/2.0f + Height * FooterPercent, 0.0f),
            new Vector3(Width/2.0f, -Height/2.0f + Height * FooterPercent, 0.0f),
            new Vector3(-Width/2.0f, Height/2.0f, 0.0f),
            new Vector3(Width/2.0f, Height/2.0f, 0.0f)
        };
        m_UVs = new Vector2[] {
            new Vector2(-Width/2.0f, -Height/2.0f),
            new Vector2(Width/2.0f, -Height/2.0f),
            new Vector2(-Width/2.0f, -Height/2.0f + Height * FooterPercent),
            new Vector2(Width/2.0f, -Height/2.0f + Height * FooterPercent),
            new Vector2(-Width/2.0f, -Height/2.0f + Height * FooterPercent),
            new Vector2(Width/2.0f, -Height/2.0f + Height * FooterPercent),
            new Vector2(-Width/2.0f, Height/2.0f),
            new Vector2(Width/2.0f, Height/2.0f)
        };
        m_Indices = new int[] { 2, 1, 0, 2, 3, 1, 6, 5, 4, 6, 7, 5 };
        m_VertexColors = new Color[8];
        for (int i = 0; i < 4; i++)
            m_VertexColors[i] = secondColor;
        for (int i = 4; i < 8; i++)
            m_VertexColors[i] = firstColor;

        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UVs;
        m_Mesh.triangles = m_Indices;
        m_Mesh.colors = m_VertexColors;

        m_MeshFilter.mesh = m_Mesh;
    }

    public void SetColor(Color firstColor, Color secondColor)
    {
        if (m_VertexColors != null)
        {
            for (int i = 0; i < 4; i++)
                m_VertexColors[i] = secondColor;
            for (int i = 4; i < 8; i++)
                m_VertexColors[i] = firstColor;
        }
        if (m_Mesh != null)
            m_Mesh.colors = m_VertexColors;
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

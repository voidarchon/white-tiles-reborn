﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Tile))]
public class FrozenBlackTile : MonoBehaviour {

    [Header("Tile's Color")]
    public Color FrozenColor;
    public Color BlackTileColor;

    public UIMesh IceGO;

    public bool IsFrozen;

    // Cached components
    private Tile m_Tile;

    public void BreakIce()
    {
        m_Tile.GenerateTileMesh(BlackTileColor);
        IceGO.gameObject.SetActive(false);
    }

    public void CreateIce()
    {
        m_Tile.GenerateTileMesh(FrozenColor);
        IceGO.gameObject.SetActive(true);
    }

    public void SetColor(Color frozenColor, Color blackTileColor)
    {
        if (frozenColor != FrozenColor) {
            FrozenColor = frozenColor;
        }
        if (BlackTileColor != blackTileColor) {
            BlackTileColor = blackTileColor;
        }
        if (IsFrozen)
            m_Tile.GenerateTileMesh(FrozenColor);
        else
            m_Tile.GenerateTileMesh(BlackTileColor);
    }

    void Awake()
    {
        m_Tile = GetComponent<Tile>();
    }

    void OnEnable()
    {
        if (IsFrozen)
            IceGO.gameObject.SetActive(true);
    }

	// Use this for initialization
	void Start () {
//         IsFrozen = true;
        m_Tile.GenerateTileMesh(FrozenColor);

        IceGO.Width = m_Tile.Width;
        IceGO.Height = m_Tile.Height;
        IceGO.GenerateMesh();

        if (!IsFrozen)
            BreakIce();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

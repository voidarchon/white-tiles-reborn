﻿using UnityEngine;
using System.Collections;

public class FrozenLogic : Singleton<FrozenLogic> {

    [Header("GO References")]
    public FrozenRowsStreamController FrozenRowsStreamControllerGO;
    public UILabel UIScoreLabelGO;
    public UILabel UIScoreShadowLabelGO;
    public RankingSystem RankingGO;
    public GameObject LightningGO;
    public GameObject[] StartLabelGO;
    public GameObject StartOverlayGO;
    public Transform UIContainerGO;
    public ObjectsPool IceParticlesPoolGO;

    [Header("Gameplay's parameters")]
    [Range(0, 50)]
    public int SpeedLerpStep;
    public float TimeStep;
    public bool GameIsPause;
    public bool GameIsOver;
    public float ScrollSpeed;

    [Header("Dont touch this, NOOB!")]
    public int StartRowId;
    [Range(0, 5)]
    public int StartIdOffset;
    public int CurrRowId;

    private bool m_FirstTimeStartGame;
    private int m_Score;
    private int m_RowSuccess;
    private bool m_SoundOn;
    private ArcadeMusicController m_ArcadeMusicController;

    private IEnumerator SpeedChangerRoutine()
    {
        float currentSpeed = FrozenRowsStreamControllerGO.ScrollSpeed;
        float maxSpeed = 2 * currentSpeed;
        float atomicTime = (maxSpeed - currentSpeed) / (SpeedLerpStep + 1);
        float t = 0.0f;
        while (currentSpeed < maxSpeed)
        {
            if (!GameIsPause)
            {
                t += Time.deltaTime;
                if (t > TimeStep)
                {
                    t = 0.0f;
                    currentSpeed += atomicTime;
                    if (currentSpeed > maxSpeed)
                        currentSpeed = maxSpeed;
                    FrozenRowsStreamControllerGO.ScrollSpeed = currentSpeed;
                    ScrollSpeed = currentSpeed;
                }
                yield return null;
            }
            yield return null;
        }
        Debug.Log("max speed");
        yield return null;
    }

    private void SetScore(int score)
    {
        string scoreStr = score.ToString();
        UIScoreLabelGO.SetText(scoreStr);
        UIScoreShadowLabelGO.SetText(scoreStr);
    }

    private void Initialize()
    {
        m_FirstTimeStartGame = true;
        GameIsOver = false;
        m_Score = 0;
        m_RowSuccess = 0;
        if (PlayerPrefs.GetInt("game_settings_sound") == 1)
            m_SoundOn = true;
        else m_SoundOn = false;
        CurrRowId = StartRowId + StartIdOffset;
        StartOverlayGO.GetComponent<StartOverlay>().Initialize(4);
        SetScore(m_Score);
    }

    private void OnDoneRowDelegate(GameObject sender, object data)
    {
        FrozenRowController doneRow = (FrozenRowController)data;
        CurrRowId++;
        FrozenRowsStreamControllerGO.EnableNextRow(doneRow);
        m_RowSuccess++;
    }

    private void OnRowSuccessTapDelegate(GameObject sender, object data)
    {
        object[] dataArr = (object[])data;
        FrozenRowController theRow = (FrozenRowController)dataArr[0];
        int idHit = (int)dataArr[1];
        if (StartRowId + StartIdOffset == theRow.RowId)
        {
            GameIsPause = false;
            if (m_FirstTimeStartGame)
            {
                if (m_SoundOn)
                {
                    m_ArcadeMusicController.Shuffle();
                    m_ArcadeMusicController.PlayMusic();
                }
                StartCoroutine(SpeedChangerRoutine());
                m_FirstTimeStartGame = false;

                for (int i = 0; i < StartLabelGO.Length; i++)
                    if (StartLabelGO[i].activeInHierarchy)
                    {
                        StartLabelGO[i].transform.parent = UIContainerGO;
                        StartLabelGO[i].SetActive(false);
                    }
            }
        }
        m_Score++;
        SetScore(m_Score);
    }

    private void OnRowFailedTapDelegate(GameObject sender, object data)
    {
        FrozenRowController failedRow = (FrozenRowController)data;
        GameIsPause = true;
        GameIsOver = true;
        // pass data
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score });
        if (m_SoundOn)
            m_ArcadeMusicController.StopMusic();
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void OnRowMissDelegate(GameObject sender, object data)
    {
        // TODO: should not pause game outside of Logic class
        FrozenRowController missedRow = (FrozenRowController)data;
        GameIsPause = true;
        GameIsOver = true;
        // pass data
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score });
        if (m_SoundOn)
            m_ArcadeMusicController.StopMusic();
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void OnStartRowGoneDelegate(GameObject sender, object data)
    {
        StartOverlayGO.transform.parent = UIContainerGO;
        StartOverlayGO.SetActive(false);
    }

    private void WaitThenEndGame()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        PlayerPrefs.SetString("ArcadeMode", "freeze");
        Application.LoadLevel("_EndScene_ArcadeMode_Universal_");
    }

    private void RegisterAllListeners()
    {
        NotificationCenter.GetInstance().RegisterListener("RowDonePlay", OnDoneRowDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowSuccessTap", OnRowSuccessTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowFailedTap", OnRowFailedTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("Frozen_RowMiss", OnRowMissDelegate);
        NotificationCenter.GetInstance().RegisterListener("Frozen_StartRowGone", OnStartRowGoneDelegate);
    }

    void Awake()
    {
        Randomkun.GetInstance().ForgetEverything();
        m_ArcadeMusicController = GetComponent<ArcadeMusicController>();
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        Initialize();
        FrozenRowsStreamControllerGO.DoInit();
        RegisterAllListeners();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FrozenRow : MonoBehaviour {

    [Header("Rows configuration")]
    [Range(4, 10)]
    public int RowPerScreen = 4;
    [Range(4, 6)]
    public int TilePerRow = 4;

    [Header("Row's properties")]
    public int[] FrozenBlackTilePosition;
    public bool IsFrozen;

    [Header("Tiles Prefabs")]
    public GameObject PrefabWhiteTile;
    public GameObject PrefabFrozenBlackTile;
    public GameObject PrefabSuccessTile;
    public GameObject PrefabFailedTile;

    private List<Tile> m_WhiteTiles;
    private List<Tile> m_FrozenBlackTile;
    private List<GameObject> m_SuccessTile;
    private GameObject m_FailedTile;

    // Tile's size
    private float m_TileWidth;
    public float TileWidth
    {
        get
        {
            return m_TileWidth;
        }
    }
    private float m_TileHeight;
    public float TileHeight
    {
        get
        {
            return m_TileHeight;
        }
    }
    private float m_PixelSize;
    public float PixelSize
    {
        get
        {
            return m_PixelSize;
        }
    }
    private float m_ScreenMostLeft;
    public float ScreenMostLeft
    {
        get
        {
            return m_ScreenMostLeft;
        }
    }

    /// <summary>
    /// return the FrozenBlackTile hitted at pos coordinate with a hitboxScale.
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="hitboxScale"></param>
    /// <returns></returns>
    public int GetFrozenBlackTileInPos(Vector3 pos, float hitboxScale)
    {
        for (int i = 0; i < m_FrozenBlackTile.Count; i++)
        {
            if (!m_SuccessTile[i].activeInHierarchy)
            {
                float topY = m_FrozenBlackTile[i].transform.position.y + m_TileHeight / 2.0f * hitboxScale;
                float bottomY = topY - m_TileHeight * hitboxScale;
                float leftX = m_FrozenBlackTile[i].transform.position.x - m_TileWidth / 2.0f * hitboxScale;
                float rightX = leftX + m_TileWidth * hitboxScale;
                if (pos.y <= topY && pos.y >= bottomY && pos.x >= leftX && pos.x <= rightX)
                {
                    return i;
                }
            }
        }
        return -1;
    }

    public GameObject GetFrozenBlackTileById(int index)
    {
        return m_FrozenBlackTile[index].gameObject;
    }

    private void ComputeTileSize()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;
        m_PixelSize = pixelSize;
        m_ScreenMostLeft = -floatWidth / 2.0f;

        m_TileHeight = (floatHeight - (RowPerScreen + 1) * pixelSize) / RowPerScreen;
        m_TileWidth = (floatWidth - (TilePerRow + 1) * pixelSize) / TilePerRow;
    }

    private void GenerateTiles()
    {
        ComputeTileSize();
        m_WhiteTiles = new List<Tile>(TilePerRow);
        m_FrozenBlackTile = new List<Tile>(FrozenBlackTilePosition.Length);
        m_SuccessTile = new List<GameObject>(FrozenBlackTilePosition.Length);
        for (int i = 0; i < TilePerRow; i++)
        {
            GameObject tileObj = Instantiate(PrefabWhiteTile) as GameObject;
            tileObj.GetComponent<Tile>().Width = m_TileWidth;
            tileObj.GetComponent<Tile>().Height = m_TileHeight;
            tileObj.transform.parent = transform;
            Vector3 pos = new Vector3();
            pos.y = 0;
            pos.z = 0;
            pos.x = i * (m_PixelSize + m_TileWidth)
                + (m_PixelSize + m_TileWidth / 2.0f)
                + m_ScreenMostLeft;
            tileObj.transform.localPosition = pos;
            m_WhiteTiles.Add(tileObj.GetComponent<Tile>());
        }
        {
            for (int i = 0; i < FrozenBlackTilePosition.Length; i++)
            {
                GameObject tileObj = Instantiate(PrefabFrozenBlackTile) as GameObject;
                tileObj.transform.parent = transform;
                tileObj.GetComponent<Tile>().Width = m_TileWidth;
                tileObj.GetComponent<Tile>().Height = m_TileHeight;
                tileObj.GetComponent<FrozenBlackTile>().IsFrozen = IsFrozen;
                tileObj.SetActive(false);
                m_FrozenBlackTile.Add(tileObj.GetComponent<Tile>());
            }
        }

        {
            for (int i = 0; i < FrozenBlackTilePosition.Length; i++)
            {
                GameObject obj = Instantiate(PrefabSuccessTile) as GameObject;
                obj.GetComponent<AnimatingTile>().Width = m_TileWidth;
                obj.GetComponent<AnimatingTile>().Height = m_TileHeight;
                obj.transform.parent = transform;
                obj.SetActive(false);
                m_SuccessTile.Add(obj);
            }

            m_FailedTile = Instantiate(PrefabFailedTile) as GameObject;
            m_FailedTile.GetComponent<AnimatingTile>().Width = m_TileWidth;
            m_FailedTile.GetComponent<AnimatingTile>().Height = m_TileHeight;
            m_FailedTile.transform.parent = transform;
            m_FailedTile.SetActive(false);
        }
    }

    private void OnFailedTap(int positionId)
    {
        Vector3 pos = new Vector3();
        pos.y = 0;
        pos.z = -0.2f;
        pos.x = positionId * (m_PixelSize + m_TileWidth)
            + (m_PixelSize + m_TileWidth / 2.0f)
                + m_ScreenMostLeft;
        m_FailedTile.transform.localPosition = pos;
        m_FailedTile.SetActive(true);
    }

    // Message receiver - from RowController

    public void SetFrozenBlackTileColor(Color frozenColor, Color blackTileColor)
    {
        for (int i = 0; i < m_FrozenBlackTile.Count; i++)
            m_FrozenBlackTile[i].GetComponent<FrozenBlackTile>().SetColor(frozenColor, blackTileColor);
    }

    public void SetWhiteTileColor(Color color)
    {
        for (int i = 0; i < TilePerRow; i++)
            m_WhiteTiles[i].GetComponent<WhiteTile>().SetColor(color);
    }

    // can use to generate new row before adjusting public variable: BlackTilePosition
    private void OnResetRowAppearance()
    {
        m_FailedTile.SetActive(false);
        for (int i = 0; i < m_SuccessTile.Count; i++)
            m_SuccessTile[i].SetActive(false);

        for (int i = 0; i < TilePerRow; i++)
            m_WhiteTiles[i].gameObject.SetActive(true);
        // Set black tile position
        for (int i = 0; i < FrozenBlackTilePosition.Length; i++)
        {
            Vector3 pos = new Vector3();
            pos.y = 0;
            pos.z = -0.1f;
            pos.x = FrozenBlackTilePosition[i] * (m_PixelSize + m_TileWidth)
                + (m_PixelSize + m_TileWidth / 2.0f)
                + m_ScreenMostLeft;
            m_FrozenBlackTile[i].transform.localPosition = pos;
            m_FrozenBlackTile[i].gameObject.SetActive(true);

            m_FrozenBlackTile[i].GetComponent<FrozenBlackTile>().IsFrozen = IsFrozen;
            if (!IsFrozen)
                m_FrozenBlackTile[i].GetComponent<FrozenBlackTile>().BreakIce();
            else
                m_FrozenBlackTile[i].GetComponent<FrozenBlackTile>().CreateIce();
            m_WhiteTiles[FrozenBlackTilePosition[i]].gameObject.SetActive(false);
        }
    }

    private void OnBreakIce(int blackTileId)
    {
        m_FrozenBlackTile[blackTileId].GetComponent<FrozenBlackTile>().BreakIce();
        m_FrozenBlackTile[blackTileId].GetComponent<FrozenBlackTile>().IsFrozen = false;
        GameObject iceParticle = FrozenLogic.Instance.IceParticlesPoolGO.GetObject();
        iceParticle.transform.position = new Vector3(m_FrozenBlackTile[blackTileId].transform.position.x, transform.position.y, -9.0f);
        iceParticle.SetActive(true);
    }

    private void OnSuccessTap(int blackTileId)
    {
        Vector3 pos = new Vector3();
        pos.y = 0;
        pos.z = -0.2f;
        pos.x = FrozenBlackTilePosition[blackTileId] * (m_PixelSize + m_TileWidth)
            + (m_PixelSize + m_TileWidth / 2.0f)
                + m_ScreenMostLeft;

        m_SuccessTile[blackTileId].transform.localPosition = pos;
        m_SuccessTile[blackTileId].SetActive(true);
    }

    void Awake()
    {
        GenerateTiles();
        OnResetRowAppearance();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

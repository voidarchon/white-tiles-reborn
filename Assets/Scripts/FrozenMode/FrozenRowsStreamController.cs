﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FrozenRowsStreamController : MonoBehaviour {

    [Header("Objects pool reference")]
    public ObjectsPool FirstRowsPoolGO;
    public ObjectsPool SecondRowsPoolGO;

    [Header("Gameplay parameters")]
    public bool ColorfulMode;
    public float ScrollSpeed;
    public float ScrollUpSpeed;
    [Range(0, 100)]
    public int FrozenProp;
    public int RowCount;

    private int m_ObjectsPoolInited;

    private LinkedList<FrozenRowController> m_Rows;
    private float m_TileHeight;
    private float m_PixelSize;
    private bool m_DoneInit;
    private Vector3 m_ScrollVector;
    private Vector3 m_RowSpawnPoint;
    private int m_NextId;

    private void InitializeFirstLook()
    {
        m_NextId = FrozenLogic.Instance.StartRowId;
        m_Rows = new LinkedList<FrozenRowController>();
        GameObject obj = FirstRowsPoolGO.PeekNextObject();
        float tileHeight = obj.GetComponent<FrozenRow>().TileHeight;
        float pixelSize = obj.GetComponent<FrozenRow>().PixelSize;
        float screenTopY = Camera.main.orthographicSize;
        m_TileHeight = tileHeight;
        m_PixelSize = pixelSize;
        m_RowSpawnPoint = new Vector3(0.0f, screenTopY - (pixelSize + tileHeight / 2.0f) + (pixelSize + tileHeight), 0.0f);

        for (int i = 0; i < RowCount; i++)
        {
            int r = Random.Range(0, 100);
            GameObject row;
            if (i <= FrozenLogic.Instance.StartIdOffset)
            {
                row = FirstRowsPoolGO.GetObject();
            }
            else
            {
                if (r < FrozenProp)
                    row = SecondRowsPoolGO.GetObject();
                else row = FirstRowsPoolGO.GetObject();
            }

            Vector3 pos = new Vector3();
            pos.x = 0.0f;
            pos.z = 0.0f;
            pos.y = -screenTopY + (pixelSize + tileHeight / 2.0f) + i * (pixelSize + tileHeight);
            row.transform.position = pos;
            row.SetActive(true);
            row.GetComponent<FrozenRowController>().RowId = m_NextId;
            if (i == FrozenLogic.Instance.StartIdOffset)
            {
                row.GetComponent<FrozenRowController>().Enabled = true;
                // start label
                int blackTileNum = row.GetComponent<FrozenRowController>().FrozenBlackTileNumber;
                for (int j = 0; j < blackTileNum; j++)
                {
                    FrozenLogic.Instance.StartLabelGO[j].transform.parent =
                        row.GetComponent<FrozenRow>().GetFrozenBlackTileById(j).transform;
                    FrozenLogic.Instance.StartLabelGO[j].transform.localPosition =
                        new Vector3(0.0f, 0.0f, -1.0f);
                    FrozenLogic.Instance.StartLabelGO[j].gameObject.SetActive(true);
                }
            }
            if (i < FrozenLogic.Instance.StartIdOffset)
            {
                row.GetComponent<FrozenRow>().SetFrozenBlackTileColor(new Color(0.196f, 0.086f, 0.224f, 1.0f), new Color(0.196f, 0.086f, 0.224f, 1.0f));
                row.GetComponent<FrozenRow>().SetWhiteTileColor(new Color(0.196f, 0.086f, 0.224f, 1.0f));
                FrozenLogic.Instance.StartOverlayGO.transform.parent = row.transform;
                FrozenLogic.Instance.StartOverlayGO.transform.localPosition = new Vector3(0.0f, 0.0f, -1.0f);
                FrozenLogic.Instance.StartOverlayGO.gameObject.SetActive(true);
            }
            else
            {
                if (ColorfulMode)
                    row.GetComponent<FrozenRow>().SetFrozenBlackTileColor(new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f),
                        new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f));
            }
            m_Rows.AddFirst(row.GetComponent<FrozenRowController>());
            m_NextId++;
        }
        m_DoneInit = true;
    }

    public void EnableNextRow(FrozenRowController currRow)
    {
        currRow.Enabled = false;
        LinkedListNode<FrozenRowController> currNode = m_Rows.Find(currRow);
        currNode.Previous.Value.Enabled = true;
    }

    private bool IsLastRowOutOfScreen()
    {
        return (m_Rows.Last.Value.transform.position.y <= -Camera.main.orthographicSize - m_TileHeight / 2.0f);
    }

    private void ScrollRowDown()
    {
        for (LinkedListNode<FrozenRowController> node = m_Rows.First;
             node != null; node = node.Next)
        {
            node.Value.transform.Translate(m_ScrollVector);
        }
    }

    private IEnumerator ScrollUpRoutine()
    {
        NotificationCenter.GetInstance().InvokeNotification(gameObject, "Frozen_RowMiss", m_Rows.Last.Value);
        float dest = -Camera.main.orthographicSize + m_TileHeight / 2.0f;
        float currVal = m_Rows.Last.Value.transform.position.y;
        while (currVal < dest)
        {
            Vector3 scrollUpVector = new Vector3(0.0f, ScrollUpSpeed * Time.deltaTime, 0.0f);
            if (currVal + ScrollUpSpeed * Time.deltaTime <= dest)
            {
                for (LinkedListNode<FrozenRowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.Translate(scrollUpVector);
                }
                currVal = m_Rows.Last.Value.transform.position.y;
            }
            else
            {
                Vector3 lastScrollUpVector = new Vector3(0.0f,
                                                         dest - m_Rows.Last.Value.transform.position.y,
                                                         0.0f);
                for (LinkedListNode<FrozenRowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.Translate(lastScrollUpVector);
                }
                currVal = dest;
            }
            yield return null;
        }
        yield return null;
    }

    private void RenewRows()
    {
        if (IsLastRowOutOfScreen())
        {
            FrozenRowController oldRow = m_Rows.Last.Value;
            if (oldRow.RowId < FrozenLogic.Instance.StartIdOffset)
            {
                NotificationCenter.GetInstance().InvokeNotification(gameObject, "Frozen_StartRowGone", null);
            }
            if (FrozenLogic.Instance.CurrRowId == oldRow.RowId)
            {
                FrozenLogic.Instance.GameIsPause = true;
                StartCoroutine(ScrollUpRoutine());
                return;
            }

            oldRow.Enabled = false;
            if (!ColorfulMode)
                oldRow.ResetAllColor();
            else oldRow.ResetWhiteTileColor();
            oldRow.ResetRowRandomize();
            oldRow.GetComponent<PooledObject>().ThrowBackToPool();
            m_Rows.RemoveLast();

            int r = Random.Range(0, 100);
            GameObject newRow;
            if (r < FrozenProp)
                newRow = SecondRowsPoolGO.GetObject();
            else newRow = FirstRowsPoolGO.GetObject();

            newRow.transform.position = m_Rows.First.Value.transform.position + new Vector3(0.0f, m_PixelSize + m_TileHeight, 0.0f);
            if (ColorfulMode)
                newRow.GetComponent<FrozenRow>().SetFrozenBlackTileColor(new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f),
                    new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f));
            newRow.SetActive(true);
            newRow.GetComponent<FrozenRowController>().RowId = m_NextId;
            m_Rows.AddFirst(newRow.GetComponent<FrozenRowController>());
            m_NextId++;
        }
    }

    private void UpdateRows()
    {
        if (m_DoneInit && !FrozenLogic.Instance.GameIsPause)
        {
            m_ScrollVector.y = -ScrollSpeed * Time.deltaTime;
            ScrollRowDown();
            RenewRows();
        }
    }

    private void OnObjectsPoolDoneInit()
    {
        m_ObjectsPoolInited++;
        if (m_ObjectsPoolInited == 2)
            InitializeFirstLook();
    }

    /// <summary>
    /// Initialize the gameboard, called from GameLogic
    /// </summary>
    public void DoInit()
    {
        m_ObjectsPoolInited = 0;
        m_DoneInit = false;
        m_ScrollVector = new Vector3(0.0f, -ScrollSpeed * Time.deltaTime, 0.0f);
        ScrollSpeed = FrozenLogic.Instance.ScrollSpeed;

        FirstRowsPoolGO.OnDoneInitEventListener += OnObjectsPoolDoneInit;
        SecondRowsPoolGO.OnDoneInitEventListener += OnObjectsPoolDoneInit;
        FirstRowsPoolGO.InitializePool();
        SecondRowsPoolGO.InitializePool();
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("Frozen_RowMiss");
        NotificationCenter.GetInstance().RegisterNotification("Frozen_StartRowGone");
    }

    void Awake()
    {
        RegisterAllNotifications();
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        UpdateRows();
        if (FrozenLogic.Instance.GameIsOver)
            return;
#if !UNITY_EDITOR
		for (int i = 0; i < Input.touchCount; i++) {
			if (i > 1) return;
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.GetTouch(i).position);
				for (LinkedListNode<FrozenRowController> node = m_Rows.First;
			     node != null; node = node.Next) {
					if (node.Value.Enabled)
						node.Value.SendMessage ("OnTap", touchPos);
				}
			}
		}
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (LinkedListNode<FrozenRowController> node = m_Rows.First;
                 node != null; node = node.Next)
            {
                if (node.Value.Enabled)
                    node.Value.SendMessage("OnTap", touchPos);
            }
        }
#endif
	}
}

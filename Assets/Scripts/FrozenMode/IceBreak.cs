﻿using UnityEngine;
using System.Collections;

public class IceBreak : MonoBehaviour {

    private Rigidbody2D m_RigidBody2D;

    void Awake()
    {
        m_RigidBody2D = GetComponent<Rigidbody2D>();
    }

	// Use this for initialization
	void OnEnable () {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        Vector2 force = new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)).normalized;
        force = force * Random.Range(0.0f, 5.0f);

        m_RigidBody2D.AddForce(force, ForceMode2D.Impulse);
        m_RigidBody2D.AddTorque(Random.Range(-2.0f, 2.0f), ForceMode2D.Impulse);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

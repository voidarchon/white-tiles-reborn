﻿using UnityEngine;
using System.Collections;

public class IceParticle : MonoBehaviour
{
    private PooledObject m_PooledObject;

    void Awake()
    {
        m_PooledObject = GetComponent<PooledObject>();
    }

    private void AutoBackToPool()
    {
        m_PooledObject.ThrowBackToPool();
    }

    void OnEnable()
    {
        Invoke("AutoBackToPool", 2.0f);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class FrozenRowController : MonoBehaviour {

    [Header("Player's tapping properties")]
    public float HitBoxScale = 1.5f;

    [Header("Row's control properties")]
    public int RowId;
    public int FrozenBlackTileNumber;
    public bool IsFrozen = true;
    private int m_DoneFrozenBlackTileNumber;
    private int[] m_FrozenBlackTileHP;

    // TODO: add enable/disable
    private bool m_Enabled;
    public bool Enabled
    {
        get
        {
            return m_Enabled;
        }
        set
        {
            m_Enabled = value;
        }
    }

    private bool m_IsFailed;
    public bool IsFailed
    {
        get
        {
            return m_IsFailed;
        }
    }

    private bool m_IsDone;
    public bool IsDone
    {
        get
        {
            return m_IsDone;
        }
    }

    private FrozenRow m_FrozenRow;

    public void ResetAllColor()
    {
        m_FrozenRow.SetFrozenBlackTileColor(new Color (0.918f, 0.439f, 0.902f), new Color(0.0f, 0.0f, 0.0f, 1.0f));
        m_FrozenRow.SetWhiteTileColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    public void ResetWhiteTileColor()
    {
        m_FrozenRow.SetWhiteTileColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    public void ResetRowRandomize()
    {
        m_IsFailed = false;
        m_IsDone = false;
        m_DoneFrozenBlackTileNumber = 0;
        // beware the garbage. but there are none >.<
        Randomkun.GetInstance().Reset();
        for (int i = 0; i < FrozenBlackTileNumber; i++)
        {
            if (IsFrozen)
                m_FrozenBlackTileHP[i] = 2;
            else
                m_FrozenBlackTileHP[i] = 1;
            m_FrozenRow.FrozenBlackTilePosition[i] = Randomkun.GetInstance().GetRandomInt();
        }
        BroadcastMessage("OnResetRowAppearance");
    }

    public void ResetRow()
    {
        m_IsFailed = false;
        m_IsDone = false;
        m_DoneFrozenBlackTileNumber = 0;
        if (IsFrozen)
            m_FrozenBlackTileHP = new int[] { 2, 2 };
        else
            m_FrozenBlackTileHP = new int[] { 1, 1 };
        BroadcastMessage("OnResetRowAppearance");
    }

    private void OnTap(Vector3 mousePosition)
    {
        if (!m_Enabled)
            return;

        if (!m_IsFailed)
        {
            if (!m_IsDone)
            {
                bool isSuccessTap = false;
                int blackIdHit = m_FrozenRow.GetComponent<FrozenRow>().GetFrozenBlackTileInPos(mousePosition, HitBoxScale);
                int positionId = -1;
                if (blackIdHit >= 0)
                {
                    if (m_FrozenBlackTileHP[blackIdHit] == 0)       // if HP == 0
                        return;                                     // do nothing
                    else
                    {
                        m_FrozenBlackTileHP[blackIdHit]--;          // else decease its HP
                        isSuccessTap = true;                        // success tap.
                    }
                }
                else
                {	
                    // does not hit any blacktiles, check for whitetiles
                    float topY = transform.position.y + m_FrozenRow.TileHeight / 2.0f;
                    float bottomY = topY - m_FrozenRow.TileHeight;

                    if (mousePosition.y >= bottomY && mousePosition.y <= topY)
                    {
                        positionId = (int)Mathf.Floor((mousePosition.x - m_FrozenRow.ScreenMostLeft) 
                            / (m_FrozenRow.TileWidth + m_FrozenRow.PixelSize));
                        for (int i = 0; i < m_FrozenRow.FrozenBlackTilePosition.Length; i++)
                        {
                            if (positionId == m_FrozenRow.FrozenBlackTilePosition[i])
                            {
                                return;
                            }
                        }
                    }
                    else return;
                }


                if (isSuccessTap)
                {
//                     Debug.Log(m_FrozenBlackTileHP[blackIdHit]);
                    if (m_FrozenBlackTileHP[blackIdHit] == 1)   // if HP == 1 => ice broke
                    {
                        BroadcastMessage("OnBreakIce", blackIdHit);
                    }
                    else if (m_FrozenBlackTileHP[blackIdHit] == 0)
                        BroadcastMessage("OnSuccessTap", blackIdHit);
                    NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowSuccessTap",
                        new object[] { this, m_FrozenRow.FrozenBlackTilePosition[blackIdHit] });
                    // if FrozenBlackTile's HP is ZERO.
                    if (m_FrozenBlackTileHP[blackIdHit] == 0)                 
                        m_DoneFrozenBlackTileNumber++;

                    // all FrozenBlackTile is dead
                    if (m_DoneFrozenBlackTileNumber == FrozenBlackTileNumber)
                    {
                        m_IsDone = true;
                        NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowDonePlay", this);
                    }
                }
                else
                {
                    if (positionId < 0) positionId = 0;
                    if (positionId >= m_FrozenRow.TilePerRow) positionId = m_FrozenRow.TilePerRow - 1;
                    BroadcastMessage("OnFailedTap", positionId);
                    NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowFailedTap", this);
                    m_IsFailed = true;
                }
            }
        }
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("RowSuccessTap");
        NotificationCenter.GetInstance().RegisterNotification("RowDonePlay");
        NotificationCenter.GetInstance().RegisterNotification("RowFailedTap");
    }

    void Awake()
    {
        RegisterAllNotifications();
        m_Enabled = false;
        m_FrozenRow = GetComponent<FrozenRow>();
        m_FrozenBlackTileHP = new int[FrozenBlackTileNumber];
        m_FrozenRow.FrozenBlackTilePosition = new int[FrozenBlackTileNumber];
    }

	// Use this for initialization
	void Start () {
        Randomkun.GetInstance().Initialize(m_FrozenRow.TilePerRow);
        ResetRowRandomize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

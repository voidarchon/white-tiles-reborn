﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (LaunchPadTileTappable))]
public class LaunchPadTileController : MonoBehaviour {
    [Header("Coordinates")]
    public int X;
    public int Y;

    [Header("Light Color")]
    public Color InteractionLightOn;
    public Color InteractionLightOff;
    public Color BackgroundLight;

    [Header("Lighting system")]
    public LaunchPadTile BackgroundLightGO;
    public LaunchPadTile AnimationLightGO;
    public LaunchPadTile InteractionLightGO;

    private LaunchPadTileTappable m_LaunchPadTappable;

    private IEnumerator LerpAnimationLightRoutine(Color fromColor, Color toColor, float time)
    {
        float t = 0.0f;
        while (t < time)
        {
            t += Time.deltaTime;
            if (t > time)
                t = time;
            yield return null;
        }
    }

    public void SetWidth(float width)
    {
        BackgroundLightGO.Width = width;
        AnimationLightGO.Width = width;
        InteractionLightGO.Width = width;
    }

    public void SetHeight(float height)
    {
        BackgroundLightGO.Height = height;
        AnimationLightGO.Height = height;
        InteractionLightGO.Height = height;
    }

    public void SetTouchSize(Vector2 size)
    {
        m_LaunchPadTappable.SetTouchSize(size);
    }

    public void SetAnimationLight(Color color)
    {
        AnimationLightGO.ChangeColor(color);
    }

    public void LerpAnimationLight(Color fromColor, Color toColor, float duration)
    {
        AnimationLightGO.LerpColor(fromColor, toColor, duration);
    }

    public void SetInteractionLight(Color color)
    {
        InteractionLightGO.ChangeColor(color);
    }

    public void SetBackgroundLight(Color color)
    {
        BackgroundLightGO.ChangeColor(color);
    }

    private void OnLaunchPadTileDown()
    {
        InteractionLightGO.ChangeColor(InteractionLightOn);
    }

    private void OnLaunchPadTileUp()
    {
        InteractionLightGO.LerpColor(InteractionLightOn, InteractionLightOff, 0.5f);
    }

    void Awake()
    {
        m_LaunchPadTappable = GetComponent<LaunchPadTileTappable>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
// 	    if (Input.GetKeyDown(KeyCode.C))
//             LerpAnimationLight(new Color(0.0f, 1.0f, 1.0f, 1.0f),
//                 new Color(0.0f, 1.0f, 1.0f, 0.0f),
//                 0.5f);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaunchPadController : MonoBehaviour {
    [Header("Properties")]
    public int Size;
    public enum Alignment { Align_Top, Align_Bottom };
    public Alignment PadAlignment;

    [Header("Prefabs")]
    public GameObject PrefabLaunchPadTile;

    [Header("For Show Only")]
    public float m_LaunchTileSize;
    public float m_PixelSize;
    public Vector2 m_PadTopLeft;

    private List<List<LaunchPadTileController>> m_LaunchTiles;

    private void CalculateSizes()
    {
        float screenWidth = Camera.main.orthographicSize * 2.0f * Camera.main.aspect;
        m_PixelSize = screenWidth / Camera.main.pixelWidth;
        m_LaunchTileSize = (screenWidth - (Size + 1) * m_PixelSize) / Size;
        if (Size % 2 == 0)
        {
            int halfSize = (int)(Size / 2);
            m_PadTopLeft = new Vector2(-((halfSize - 0.5f) * m_LaunchTileSize + halfSize * m_PixelSize - 0.5f * m_PixelSize),
                ((halfSize - 0.5f) * m_LaunchTileSize + halfSize * m_PixelSize - 0.5f * m_PixelSize));
        }
        else
        {
            int halfSize = (int)(Size / 2);
            m_PadTopLeft = new Vector2(-(halfSize * (m_LaunchTileSize + m_PixelSize)),
                (halfSize * (m_LaunchTileSize + m_PixelSize)));
        }
    }

    private void GeneratePadTiles()
    {
        m_LaunchTiles = new List<List<LaunchPadTileController>>(Size);
        for (int i = 0; i < Size; i++)
            m_LaunchTiles.Add(new List<LaunchPadTileController>(Size));

        for (int i = 0; i < Size; i++)
        {
            for (int j = 0; j < Size; j++)
            {
                GameObject obj = Instantiate(PrefabLaunchPadTile) as GameObject;
                obj.GetComponent<LaunchPadTileController>().SetWidth(m_LaunchTileSize);
                obj.GetComponent<LaunchPadTileController>().SetHeight(m_LaunchTileSize);
                obj.GetComponent<LaunchPadTileController>().SetTouchSize(new Vector2(m_LaunchTileSize, m_LaunchTileSize));
                obj.transform.position = m_PadTopLeft
                    + new Vector2(j * (m_LaunchTileSize + m_PixelSize),
                        - i * (m_LaunchTileSize + m_PixelSize));
                obj.transform.parent = transform;
                m_LaunchTiles[i].Add(obj.GetComponent<LaunchPadTileController>());
            }
        }
    }

    private void RepositionPad()
    {
        if (PadAlignment == Alignment.Align_Bottom)
        {
            float yPos = m_PadTopLeft.y - m_PixelSize;
            transform.localPosition = new Vector3(0.0f, -yPos, 0.0f);
        }
    }

    private void GeneratePad()
    {
        CalculateSizes();
        GeneratePadTiles();
        RepositionPad();
    }

    private IEnumerator AnimationLayerAnimator()
    {
        Color transparent = Color.yellow;
        transparent.a = 0.0f;
        while (true)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                    m_LaunchTiles[i][j].LerpAnimationLight(Color.yellow,
                        transparent, 0.3f);
                yield return new WaitForSeconds(0.15f);
            }
            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                    m_LaunchTiles[j][i].LerpAnimationLight(Color.yellow,
                        transparent, 0.3f);
                yield return new WaitForSeconds(0.15f);
            }
            yield return new WaitForSeconds(1.0f);
        }
    }

    void Awake()
    {
        GeneratePad();
    }

    void testFunc()
    {
        StartCoroutine(AnimationLayerAnimator());
    }

	// Use this for initialization
	void Start () {
        Invoke("testFunc", 5.0f);
	}
	
	// Update is called once per frame
	void Update () {
	}
}

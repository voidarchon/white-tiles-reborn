﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaunchPadTouchHandler : MonoBehaviour
{
    [Header("Touches number")]
    public int TouchNumber;

    private List<Transform> m_TouchedTiles;

    // Use this for initialization
    void Start()
    {
        m_TouchedTiles = new List<Transform>(TouchNumber);
    }

    // Update is called once per frame
    void Update()
    {
#if !UNITY_EDITOR
        if (m_TouchedTiles.Count > 0)
            m_TouchedTiles.Clear();
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (i > TouchNumber - 1)
                return;
            if (Input.GetTouch(i).phase == TouchPhase.Began 
                || Input.GetTouch(i).phase == TouchPhase.Moved
                || Input.GetTouch(i).phase == TouchPhase.Stationary)
            {
                Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);

                RaycastHit2D hittedObj = Physics2D.Raycast(touchPos, Vector2.zero, 100.0f);
                if (hittedObj.collider != null)
                {
                    if (string.Compare(hittedObj.collider.gameObject.tag, "UI") != 0)
                    {
                        hittedObj.collider.gameObject.SendMessage("OnTapDown");
                        m_TouchedTiles.Add(hittedObj.transform);
                    }
                }
            }
        }

        foreach (Transform child in transform)
        {
            bool hitted = false;
            foreach (Transform e in m_TouchedTiles)
                if (child == e)
                {
                    hitted = true;
                    break;
                }
            if (!hitted)
                child.SendMessage("OnTapUp");
        }

#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            bool uiHitted = false;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hittedObj = Physics2D.Raycast(mousePos, Vector2.zero, 100.0f);
            if (hittedObj.collider != null)
            {
                if (string.Compare(hittedObj.collider.gameObject.tag, "UI") != 0)
                {
                    hittedObj.collider.gameObject.SendMessage("OnTapDown", SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    UIManager.Instance.OnTapDown(hittedObj.collider.gameObject);
                    uiHitted = true;
                }
            }
            else
            {
                // Debug.Log("coming through");
            }

            if (!uiHitted)
            {
                // BroadcastMessage("OnFire", mousePos);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hittedObj = Physics2D.Raycast(mousePos, Vector2.zero, 100.0f);
            //             if (hittedObj.collider != null 
            //                 && string.Compare(hittedObj.collider.gameObject.tag, "UI") == 0)
            //             {
            //                 UIManager.Instance.OnTapUp();
            //             }
            UIManager.Instance.OnTapUp();
        }
#endif
    }
}

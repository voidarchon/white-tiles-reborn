﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class LaunchPadTile : MonoBehaviour {

    [Header("Tile Properties")]
    public float Width;
    public float Height;
    public Color MeshColor;

    // mesh data
    private MeshFilter m_MeshFilter;
    private Mesh m_Mesh;
    private Vector3[] m_Vertices;
    private Vector2[] m_UVs;
    private int[] m_Indices;
    private Color[] m_VertexColors;

    private void CleanOldMeshData()
    {
        if (m_Mesh != null)
            Object.Destroy(m_Mesh);
    }

    public void GenerateMesh()
    {
        float z = transform.localPosition.z;

        m_Vertices = new Vector3[] {
            new Vector3(-Width / 2.0f, -Height / 2.0f, z), 
            new Vector3(-Width / 2.0f, Height / 2.0f, z), 
            new Vector3(Width / 2.0f, Height / 2.0f, z), 
            new Vector3(Width / 2.0f, -Height / 2.0f, z)
        };

        m_UVs = new Vector2[] {
            new Vector2(0.0f, 0.0f), 
            new Vector2(0.0f, 1.0f), 
            new Vector2(1.0f, 1.0f), 
            new Vector2(1.0f, 0.0f)
        };
        m_Indices = new int[] { 0, 1, 2, 2, 3, 0 };
        m_VertexColors = new Color[4];
        for (int i = 0; i < 4; i++)
            m_VertexColors[i] = MeshColor;

        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UVs;
        m_Mesh.triangles = m_Indices;
        m_Mesh.colors = m_VertexColors;

        m_MeshFilter.mesh = m_Mesh;
    }

    private IEnumerator LerpColorRoutine(Color fromColor, Color toColor, float duration)
    {
        float t = 0;
        while (t < duration)
        {
            t += Time.deltaTime;
            if (t > duration)
                t = duration;

            MeshColor = Color.Lerp(fromColor, toColor, t / duration);
            
            for (int i = 0; i < 4; i++)
                m_VertexColors[i] = MeshColor;
            m_MeshFilter.mesh.colors = m_VertexColors;
            
            yield return null;
        }
        yield return null;
    }

    public void LerpColor(Color fromColor, Color toColor, float duration)
    {
        StopCoroutine("LerpColorRoutine");
        StartCoroutine(LerpColorRoutine(fromColor, toColor, duration));
    }

    public void ChangeColor(Color color)
    {
        StopCoroutine("LerpColorRoutine");
        if (MeshColor == color)
            return;
        MeshColor = color;

        for (int i = 0; i < 4; i++)
            m_VertexColors[i] = MeshColor;
        m_MeshFilter.mesh.colors = m_VertexColors;
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
    }

    // Use this for initialization
    void Start()
    {
        GenerateMesh();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
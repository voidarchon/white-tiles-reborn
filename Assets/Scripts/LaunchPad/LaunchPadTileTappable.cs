﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof (BoxCollider2D))]
public class LaunchPadTileTappable : MonoBehaviour {

    private BoxCollider2D m_BoxCollider2D;

    private bool m_IsDown;

    private void OnTapDown()
    {
        if (!m_IsDown)
        {
            BroadcastMessage("OnLaunchPadTileDown", SendMessageOptions.DontRequireReceiver);
            m_IsDown = true;
        }
    }

    private void OnTapUp()
    {
        if (m_IsDown)
        {
            BroadcastMessage("OnLaunchPadTileUp", SendMessageOptions.DontRequireReceiver);
            m_IsDown = false;
        }
    }

    public void SetTouchSize(Vector2 size)
    {
        m_BoxCollider2D.size = size;
    }

    void Awake()
    {
        m_BoxCollider2D = GetComponent<BoxCollider2D>();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RhythmSpawner : MonoBehaviour {

	public RhythmTilesPool RhythmTilesPoolGO;
	public GameObject TapIndicatorGO;
	public GameObject RhythmIndicatorGO;

	[Header("Tap result labels")]
	public GameObject PerfectLabelGO;
	public GameObject MissLabelGO;
	public GameObject BadLabelGO;

	[Header("Spawner's properties")]
	public int SpawnerPosition;
	public int TilePerScreenWidth;

	private LinkedList<RhythmTileController> m_RhythmTilesList;

	private void OnTileGoneDelegate(RhythmTileController theRow)
	{
		m_RhythmTilesList.Remove (theRow);
	}

	private void OnKeyboardDown()
	{
		TapIndicatorGO.SetActive (true);
		if (m_RhythmTilesList.Count > 0)
			m_RhythmTilesList.Last.Value.SendMessage ("OnTapDown");
	}

	private void OnKeyboardUp()
	{
		TapIndicatorGO.SetActive (false);
		if (m_RhythmTilesList.Count > 0)
			m_RhythmTilesList.Last.Value.SendMessage ("OnTapUp");
	}

	private void OnTileDownDelegate()
	{
		RhythmIndicatorGO.SetActive (true);
	}

	private void OnTileUpDelegate()
	{
		RhythmIndicatorGO.SetActive (false);
	}

	private void OnTilePerfectDelegate()
	{
		Debug.Log ("PERFECT");
		BadLabelGO.SetActive (false);
		MissLabelGO.SetActive (false);
		PerfectLabelGO.SetActive (false);
		PerfectLabelGO.SetActive (true);
	}

	private void OnTileBadDelegate()
	{
		Debug.Log ("BAD");
		BadLabelGO.SetActive (false);
		MissLabelGO.SetActive (false);
		PerfectLabelGO.SetActive (false);
		BadLabelGO.SetActive (true);
	}

	private void OnTileMissDelegate()
	{
		Debug.Log ("MISS");
		BadLabelGO.SetActive (false);
		MissLabelGO.SetActive (false);
		PerfectLabelGO.SetActive (false);
		MissLabelGO.SetActive (true);
	}

	public void SpawnRhythm(int length, float offset)
	{
		GameObject tile = RhythmTilesPoolGO.GetRhythmTile (length);
		Vector3 pos = transform.position;
		pos.y += tile.GetComponent<RhythmTile>().GetFloatLength() / 2.0f + offset;
		tile.transform.position = pos;
		tile.transform.parent = transform;
		tile.SetActive (true);
		RhythmTileController tileController = tile.GetComponent<RhythmTileController> ();
		tileController.OnTileGone = OnTileGoneDelegate;
		tileController.OnTileDown = OnTileDownDelegate;
		tileController.OnTileUp = OnTileUpDelegate;
		tileController.OnTilePerfect = OnTilePerfectDelegate;
		tileController.OnTileBad = OnTileBadDelegate;
		tileController.OnTileMiss = OnTileMissDelegate;

		m_RhythmTilesList.AddFirst (tile.GetComponent<RhythmTileController> ());
	}

	private void RegisterAllNotifications()
	{
		NotificationCenter.GetInstance ().RegisterNotification ("Rhythm_TilePerfect");
		NotificationCenter.GetInstance ().RegisterNotification ("Rhythm_TileBad");
		NotificationCenter.GetInstance ().RegisterNotification ("Rhythm_TileMiss");
	}

	void Awake() {
		float pixelSize = Camera.main.orthographicSize * 2.0f / Camera.main.pixelHeight;
		float tileWidth = (Camera.main.orthographicSize * 2.0f * Camera.main.aspect - (TilePerScreenWidth + 1) * pixelSize) / TilePerScreenWidth;
		float screenMostLeft = - Camera.main.orthographicSize * Camera.main.aspect;

		transform.position = new Vector3 (screenMostLeft + pixelSize + tileWidth / 2.0f + SpawnerPosition * (tileWidth + pixelSize),
		                                  Camera.main.orthographicSize,
		                                  0.0f);
	}

	// Use this for initialization
	void Start () {
		m_RhythmTilesList = new LinkedList<RhythmTileController> ();
//		StartCoroutine (WaitThenSpawn ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class RhythmIndicator : MonoBehaviour {
	private float m_MagicNumber;

	private MeshRenderer m_MeshRenderer;

	void Awake() {
		m_MeshRenderer = GetComponent<MeshRenderer> ();
	}

	// Use this for initialization
	void Start () {
		m_MagicNumber = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		m_MagicNumber += Time.deltaTime;
		m_MeshRenderer.material.SetFloat ("_TheMagicNumber", m_MagicNumber);
	}
}

﻿using UnityEngine;
using System.Collections;

public class RhythmTilesPool : MonoBehaviour {

	public ObjectsPool[] RhythmTilesPoolGO;

	public GameObject GetRhythmTile(int length)
	{
		return RhythmTilesPoolGO [length].GetObject ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

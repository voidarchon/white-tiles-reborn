﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshRenderer))]
public class RhythmTile : MonoBehaviour {
	[Header("RhythmTile's properties")]
	public int TilePerScreenWidth;
	public int Length;

	[Header ("General properties, must be removed")]
	public float AtomicLength;

	private float m_TileWidth;
	private float m_PixelSize;
	private Mesh m_Mesh;
	private Vector3[] m_Vertices;
	private Vector2[] m_UVs;
	private int[] m_Indices;
	private Color[] m_VertexColors;

	// cached components
	private MeshFilter m_MeshFilter;

	public void GenerateTileMesh(Color fillColor)
	{
		m_PixelSize = Camera.main.orthographicSize * 2.0f / Camera.main.pixelHeight;
		m_TileWidth = (Camera.main.orthographicSize * 2.0f * Camera.main.aspect - (TilePerScreenWidth + 1) * m_PixelSize) / TilePerScreenWidth;

		float tileLength = Length * AtomicLength;
		if (m_Mesh != null)
			Object.Destroy (m_Mesh);
		
		float z = transform.localPosition.z;
		m_Vertices = new Vector3[] {
			new Vector3(-m_TileWidth/2.0f, -tileLength/2.0f, z),
			new Vector3(-m_TileWidth/2.0f, tileLength/2.0f, z),
			new Vector3(m_TileWidth/2.0f, tileLength/2.0f, z),
			new Vector3(m_TileWidth/2.0f, -tileLength/2.0f, z)
		};
		m_UVs = new Vector2[] {
			new Vector2(-m_TileWidth/2.0f, -tileLength/2.0f),
			new Vector2(-m_TileWidth/2.0f, tileLength/2.0f),
			new Vector2(m_TileWidth/2.0f, tileLength/2.0f),
			new Vector2(m_TileWidth/2.0f, -tileLength/2.0f)
		};
		m_Indices = new int[] { 0, 1, 2, 2, 3, 0 };
		m_VertexColors = new Color[4];
		for (int i = 0; i < 4; i++)
			m_VertexColors[i] = fillColor;
		
		m_Mesh = new Mesh();
		m_Mesh.vertices = m_Vertices;
		m_Mesh.uv = m_UVs;
		m_Mesh.triangles = m_Indices;
		m_Mesh.colors = m_VertexColors;
		
		m_MeshFilter.mesh = m_Mesh;
	}

	public float GetFloatLength()
	{
		return Length * AtomicLength;
	}

	void OnDestroy()
	{
		Object.Destroy(m_Mesh);
	}

	void Awake() {
		m_MeshFilter = GetComponent<MeshFilter> ();
		GenerateTileMesh (new Color (0.3f, 0.4f, 0.6f, 1.0f));
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
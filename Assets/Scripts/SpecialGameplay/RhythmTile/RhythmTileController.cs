﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (RhythmTile))]
[RequireComponent (typeof (PooledObject))]
public class RhythmTileController : MonoBehaviour {

	[Header("Gameplay configurations")]
	public float FallingSpeed;
	public float Anchor;

	private RhythmTile m_RhythmTile;
	private bool m_IsTapping;
	private bool m_DoneTapped;

	public delegate void TileGoneListener(RhythmTileController theRow);
	public TileGoneListener OnTileGone;
	public delegate void TileStateListener();
	public TileStateListener OnTileDown;
	public TileStateListener OnTileUp;
	public TileStateListener OnTilePerfect;
	public TileStateListener OnTileBad;
	public TileStateListener OnTileMiss;

	private void CheckVisible()
	{
		if (transform.position.y <= Anchor - m_RhythmTile.GetFloatLength () / 2.0f - m_RhythmTile.AtomicLength * 1.0f) {
			/*
			if (m_IsTapping && m_RhythmTile.Length > 1)
			{
				// Debug.Log("out-bad");
				if (OnTileBad != null)
					OnTileBad();
			}
			if (m_IsTapping == false && !m_DoneTapped)
				if (OnTileMiss != null)
					OnTileMiss();
			m_IsTapping = false;
			m_DoneTapped = false;
			if (OnTileUp != null)
				OnTileUp();
			*/
			if (!m_DoneTapped) {
				if (OnTileMiss != null)
					OnTileMiss();
			}

			m_IsTapping = false;
			m_DoneTapped = false;
			if (OnTileUp != null)
				OnTileUp();
			if (OnTileGone != null)
				OnTileGone(this);
			GetComponent<PooledObject>().ThrowBackToPool();

		}
	}

	private void UpdatePosition()
	{
		float translateDist = FallingSpeed * Time.deltaTime;
		transform.Translate (new Vector3 (0.0f, -translateDist, 0.0f));
	}

	private void UpdateTile()
	{
		UpdatePosition ();
		CheckVisible ();
	}
	
	private void OnTapDown()
	{
		float length = m_RhythmTile.GetFloatLength ();
		float atomicLength = m_RhythmTile.AtomicLength;
		float tapTopY = transform.position.y - length / 2.0f + 1 * atomicLength;
		float tapBottomY = transform.position.y - length / 2.0f - 1 * atomicLength;

		if (Anchor >= tapBottomY && Anchor <= tapTopY) {
			if (m_RhythmTile.Length == 1) {
				m_DoneTapped = true;
				if (OnTilePerfect != null)
					OnTilePerfect();
			} else {
				m_IsTapping = true;
			}
		} else {
			/*
			m_DoneTapped = true;
			if (OnTileMiss != null)
				OnTileMiss();
			*/
		}

		/*
		if (Anchor >= tapBottomY && Anchor <= tapTopY) {
			// Debug.Log ("perfect");
			m_IsTapping = true;
			if (OnTileDown != null)
				OnTileDown();
			if (m_RhythmTile.Length == 1)
			{
				if (OnTilePerfect != null)
					OnTilePerfect();
				m_DoneTapped = true;
			}
		} else if (Anchor < tapBottomY) {
			// Debug.Log ("too soon");
		} else if (Anchor > tapTopY) {
			// Debug.Log ("miss");
			if (OnTileMiss != null)
				OnTileMiss();
		}
		*/
	}

	private void OnTapUp()
	{
		float length = m_RhythmTile.GetFloatLength ();
		float atomicLength = m_RhythmTile.AtomicLength;
		float tapUpBottomY = transform.position.y + length / 2.0f - atomicLength;
		float tapUpTopY = tapUpBottomY + atomicLength;
		if (m_RhythmTile.Length == 1) {
			return;
		} else {
			if (Anchor >= tapUpBottomY && Anchor <= tapUpTopY) {
				m_IsTapping = false;
				m_DoneTapped = true;
				if (OnTilePerfect != null)
					OnTilePerfect();
			} else {
				m_DoneTapped = true;
				if (OnTileMiss != null)
					OnTileMiss();
			}
		}

		/*
		if (m_IsTapping) {
			if (Anchor < tapUpBottomY)
			{
				// Debug.Log("failed");
				if (OnTileMiss != null)
					OnTileMiss();
			}
			else if (Anchor >= tapUpBottomY && Anchor <= tapUpTopY)
			{
				// Debug.Log("out-perfect");
				if (OnTilePerfect != null)
					OnTilePerfect();
			}
			m_IsTapping = false;
			m_DoneTapped = true;
			if (OnTileUp != null)
				OnTileUp();
		}
		*/
	}

	private void RegisterAllNotifications()
	{
	}

	void Awake() {
		m_RhythmTile = GetComponent<RhythmTile> ();
		m_DoneTapped = false;
		RegisterAllNotifications ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		UpdateTile ();
	}
}

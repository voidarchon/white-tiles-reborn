﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshRenderer))]
public class TapIndicator : MonoBehaviour {
	public int TilePerScreenWidth;
	public float Length;
	public Color FillColor;
	
	private float m_TileWidth;
	private float m_PixelSize;
	private Mesh m_Mesh;
	private Vector3[] m_Vertices;
	private Vector2[] m_UVs;
	private int[] m_Indices;
	private Color[] m_VertexColors;
	
	// cached components
	private MeshFilter m_MeshFilter;
	
	public void GenerateTileMesh(Color fillColor)
	{
		m_PixelSize = Camera.main.orthographicSize * 2.0f / Camera.main.pixelHeight;
		m_TileWidth = (Camera.main.orthographicSize * 2.0f * Camera.main.aspect - (TilePerScreenWidth + 1) * m_PixelSize) / TilePerScreenWidth;

		if (m_Mesh != null)
			Object.Destroy (m_Mesh);
		
		float z = transform.localPosition.z;
		m_Vertices = new Vector3[] {
			new Vector3(-m_TileWidth/2.0f, -Length/2.0f, z),
			new Vector3(-m_TileWidth/2.0f, Length/2.0f, z),
			new Vector3(m_TileWidth/2.0f, Length/2.0f, z),
			new Vector3(m_TileWidth/2.0f, -Length/2.0f, z)
		};
		m_UVs = new Vector2[] {
			new Vector2(-m_TileWidth/2.0f, -Length/2.0f),
			new Vector2(-m_TileWidth/2.0f, Length/2.0f),
			new Vector2(m_TileWidth/2.0f, Length/2.0f),
			new Vector2(m_TileWidth/2.0f, -Length/2.0f)
		};
		m_Indices = new int[] { 0, 1, 2, 2, 3, 0 };
		m_VertexColors = new Color[] {
			FillColor,
			new Color(0.0f, 0.0f, 0.0f, 0.0f),
			new Color(0.0f, 0.0f, 0.0f, 0.0f),
			FillColor
		};
		
		m_Mesh = new Mesh();
		m_Mesh.vertices = m_Vertices;
		m_Mesh.uv = m_UVs;
		m_Mesh.triangles = m_Indices;
		m_Mesh.colors = m_VertexColors;
		
		m_MeshFilter.mesh = m_Mesh;
	}
	
	void OnDestroy()
	{
		Object.Destroy(m_Mesh);
	}
	
	void Awake() {
		m_MeshFilter = GetComponent<MeshFilter> ();
	}
	
	// Use this for initialization
	void Start () {
		GenerateTileMesh (FillColor);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

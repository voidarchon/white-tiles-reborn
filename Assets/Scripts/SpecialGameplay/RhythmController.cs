﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class Rhythm {
	public float PlayTime;
	public int Length;
	public int Height;

	public Rhythm(float playTime, int length, int height)
	{
		PlayTime = playTime;
		Length = length;
		Height = height;
	}
}

public class RhythmController : MonoBehaviour {

	[Header("Spawners references")]
	public RhythmSpawner[] RhythmSpawnerGOs;
	public float FallingSpeed;

	private float m_LastPlayedTime;
	private float m_PlayedTime;
	private List<Rhythm> m_Rhythms;
	private int m_CurrentRhythm;

	private IEnumerator RhythmPlayRoutine() {
		while (m_CurrentRhythm < m_Rhythms.Count) {
			m_LastPlayedTime = m_PlayedTime;
			m_PlayedTime += Time.deltaTime;

			while (m_CurrentRhythm < m_Rhythms.Count && m_Rhythms[m_CurrentRhythm].PlayTime < m_PlayedTime)
			{
				float offset = FallingSpeed * (m_Rhythms[m_CurrentRhythm].PlayTime - m_LastPlayedTime); 
				RhythmSpawnerGOs[m_Rhythms[m_CurrentRhythm].Height].SpawnRhythm(
					m_Rhythms[m_CurrentRhythm].Length, offset);
				m_CurrentRhythm++;
			}
			yield return null;
		}
	}

	public void StartGame()
	{
		QualitySettings.vSyncCount = 0;
		Application.targetFrameRate = 10;
		m_LastPlayedTime = 0.0f;
		m_PlayedTime = 0.0f;
	}

	// test code
	/*
	private void GenerateSampleRhythms()
	{
		m_Rhythms = new Rhythm[50];
		float lastTime = 1.0f;
		for (int i = 0; i < 50; i++) {
			int len;
			int r = Random.Range(0, 100);
			if (r < 80)
				len = 0;
			else len = 2;
			float timeStep = Random.Range(1.0f, 2.0f);
			Rhythm rhythm = new Rhythm(lastTime + timeStep, len, Random.Range(0, 3));
			lastTime = lastTime + timeStep;
			m_Rhythms[i] = rhythm;
		}
	}
	*/

	private void LoadSong(string fileName)
	{
		XmlDocument doc = new XmlDocument ();
		doc.Load ("Assets/Resources/Rhythms/" + fileName + ".xml");
		int rhythmCount = int.Parse (doc.ChildNodes [1].Attributes ["rhythmcount"].Value);
		m_Rhythms = new List<Rhythm> (rhythmCount);
		foreach (XmlNode node in doc.ChildNodes[1].ChildNodes) {
			m_Rhythms.Add(new Rhythm(float.Parse(node.Attributes["playtime"].Value),
			                         int.Parse(node.Attributes["length"].Value),
			                         int.Parse(node.Attributes["height"].Value)));
		}
	}

	// Use this for initialization
	void Start () {
		LoadSong ("HuaSuiYue");
		// GenerateSampleRhythms ();
		m_CurrentRhythm = 0;
		StartCoroutine (RhythmPlayRoutine ());
	}
	
	// Update is called once per frame
	void Update () {
		// if (Input.GetKeyDown (KeyCode.Space))
	}
}

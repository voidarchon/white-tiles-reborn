﻿using UnityEngine;
using System.Collections;

public class KeyboardController : MonoBehaviour {

	public int LanePerScreen;
	private float LaneWidth;

	public RhythmSpawner[] RhythmSpawners;
	private int[] m_KeyboardTouchIds;

	// Use this for initialization
	void Start () {
		LaneWidth = Camera.main.orthographicSize * 2.0f * Camera.main.aspect / LanePerScreen;
		m_KeyboardTouchIds = new int[] {-1, -1, -1, -1};
	}
	
	// Update is called once per frame
	void Update () {
// #if !UNITY_EDITOR
		for (int i = 0; i < Input.touchCount; i++) {
			if (i > 4) return;
			Touch currTouch = Input.GetTouch(i);
			if (currTouch.phase == TouchPhase.Began) {
				Vector2 touchPos = Camera.main.ScreenToWorldPoint(currTouch.position);
				if (touchPos.y <= -3.5f) {
					int id = (int)((touchPos.x + Camera.main.orthographicSize * Camera.main.aspect) / LaneWidth);
					RhythmSpawners[id].SendMessage("OnKeyboardDown");
					m_KeyboardTouchIds[id] = currTouch.fingerId;
				}
			}
			if (currTouch.phase == TouchPhase.Ended) {
				for (int j = 0; j < RhythmSpawners.Length; j++)
					if (m_KeyboardTouchIds[j] == currTouch.fingerId)
				{
					RhythmSpawners[j].SendMessage("OnKeyboardUp");
					m_KeyboardTouchIds[j] = -1;
				}
			}
		}
// #endif

#if UNITY_EDITOR
		if (Input.GetKeyDown (KeyCode.Alpha1))
			RhythmSpawners [0].SendMessage ("OnKeyboardDown");
		if (Input.GetKeyUp (KeyCode.Alpha1))
			RhythmSpawners [0].SendMessage ("OnKeyboardUp");

		if (Input.GetKeyDown (KeyCode.Alpha2))
			RhythmSpawners [1].SendMessage ("OnKeyboardDown");
		if (Input.GetKeyUp (KeyCode.Alpha2))
			RhythmSpawners [1].SendMessage ("OnKeyboardUp");

		if (Input.GetKeyDown (KeyCode.Alpha3))
			RhythmSpawners [2].SendMessage ("OnKeyboardDown");
		if (Input.GetKeyUp (KeyCode.Alpha3))
			RhythmSpawners [2].SendMessage ("OnKeyboardUp");

		if (Input.GetKeyDown (KeyCode.Alpha4))
			RhythmSpawners [3].SendMessage ("OnKeyboardDown");
		if (Input.GetKeyUp (KeyCode.Alpha4))
			RhythmSpawners [3].SendMessage ("OnKeyboardUp");
#endif
	}
}

﻿using UnityEngine;
using System.Collections;

// ONLY USE FOR MANAGERS

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T m_Instance;

    public static T Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = (T)FindObjectOfType(typeof(T));
                if (m_Instance == null)
                {
                    Debug.LogError("An instance of " + typeof(T) +
                  " is needed in the scene, but there is none.");
                }
            }
            return m_Instance;
        }
    }
}
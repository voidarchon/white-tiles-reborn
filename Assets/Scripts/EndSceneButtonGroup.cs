﻿using UnityEngine;
using System.Collections;

public class EndSceneButtonGroup : MonoBehaviour {

    public GameObject[] Buttons;

    [Header("Screen configurations")]
    public int TilePerRow;
    public int RowPerScreen;

    private float m_PixelSize;
    private float m_ScreenMostLeft;
    private float m_TileHeight;
    private float m_TileWidth;

    private void CalculateSizes()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;
        m_PixelSize = pixelSize;
        m_ScreenMostLeft = -floatWidth / 2.0f;

        m_TileHeight = (floatHeight - (RowPerScreen + 1) * pixelSize) / RowPerScreen;
        m_TileWidth = (floatWidth - (TilePerRow + 1) * pixelSize) / TilePerRow;
    }

    private void Initialize()
    {
        CalculateSizes();
        Vector3 parentPos = transform.localPosition;
        parentPos.y = -Camera.main.orthographicSize + m_PixelSize + m_TileHeight / 2.0f;
        transform.localPosition = parentPos;
        for (int i = 0; i < Buttons.Length; i++)
        {
            Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
            pos.x = m_ScreenMostLeft + m_PixelSize + m_TileWidth / 2.0f
                + i * (m_PixelSize + m_TileWidth);
            Buttons[i].transform.localPosition = pos;
        }
    }

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArcadeEndSceneLogic : MonoBehaviour
{
    [Header("Stuffs")]
    public GameObject ScoreValueGO;
    public GameObject BestScoreValueGO;
    public GameObject AnimationGroupGO;
    public SpeedGraph PerformanceGraphGO;
    public GameObject InfoLabelGO;

    private int m_ScoreValue;
    private RankingSystem.Rank m_Rank;

    [Header("Ranks")]

    public SpriteRenderer BlurLayerGO;
    public SpriteRenderer MainLayerGO;

    public Sprite NoviceBlur;
    public Sprite NoviceMain;

    public Sprite RatherGoodBlur;
    public Sprite RatherGoodMain;

    public Sprite PianistBlur;
    public Sprite PianistMain;

    public Sprite GeniusBlur;
    public Sprite GeniusMain;

    public Sprite LegendaryBlur;
    public Sprite LegendaryMain;

    public Sprite GodlikeBlur;
    public Sprite GodlikeMain;

    public Sprite FreezeBlur;
    public Sprite FreezeMain;

    public Sprite ZenBlur;
    public Sprite ZenMain;

    public Sprite MirrorBlur;
    public Sprite MirrorMain;
    

    private string m_ArcadeFullName;
    private string m_FlipFullName;
    private string m_ZenFullName;

    private void OnFacebookShareClicked()
    {
        Debug.Log("facebook share");
        // facebook share go here, no need to login? YES!!!!
        // can work even without facebook app installed.
        string shareStr;

        if (PlayerPrefs.GetInt("HasLightning") == 1)
        {
            shareStr = "Completed Blink Mode with " + m_ScoreValue + " points.";
        }
        else if (PlayerPrefs.GetInt("ReverseActivated") == 1)
        {
            shareStr = "Completed Flip Mode with " + m_ScoreValue + " points.";
        }
        else
        {
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") == 0)
            {
                shareStr = "Completed Freeze Mode with " + m_ScoreValue + " points.";
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "mirror") == 0)
            {
                shareStr = "Completed Flip Mode with " + m_ScoreValue + " points.";
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "zen") == 0)
            {
                shareStr = "Completed Zen Mode with " + m_ScoreValue + " points.";
            }
            else
            {
                shareStr = "Completed Arcade " + PlayerPrefs.GetString("ArcadeMode") + " Mode with " + m_ScoreValue + " points.";
            }
            /*
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") != 0)
            {
                shareStr = "Completed Arcade " + PlayerPrefs.GetString("ArcadeMode") + " Mode with " + m_ScoreValue + " points.";
            }
            else
            {
                shareStr = "Completed Freeze Mode with " + m_ScoreValue + " points.";
            }
             */
        }

        FacebookServ.Instance.ShareLinkContent(
            "https://dl.dropboxusercontent.com/u/99391934/empty.html",
            "WhiteTiles Reborn",
            shareStr,
            null);
    }

    private void OnReplayClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") == 0)
        {
            Application.LoadLevel("_PlayScene_FrozenMode_");
        }
        else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "mirror") == 0)
        {
            Application.LoadLevel("_PlayScene_MirrorMode_");
        }
        else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "zen") == 0)
        {
            Application.LoadLevel("_PlayScene_ZenMode_");
        }
        else
        {
            Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
        }
        /*
        if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") != 0)
            Application.LoadLevel("_PlayScene_ArcadeMode_Universal_");
        else
            Application.LoadLevel("_PlayScene_FrozenMode_");
         */
    }

    private void OnExitClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        Application.LoadLevel("_MainMenu_");
    }

    private void OnLBOpenClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        if (PlayerPrefs.GetInt("HasLightning") == 1)
        {
            PlayerPrefs.SetString("ToLB_GameMode", "blink");
            PlayerPrefs.SetString("ToLB_ScoreType", "point");
            PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName("blink_recent"));
            PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName("blink"));
        }
        else if (PlayerPrefs.GetInt("ReverseActivated") == 1)
        {
            PlayerPrefs.SetString("ToLB_GameMode", m_FlipFullName);
            PlayerPrefs.SetString("ToLB_ScoreType", "point");
            PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName(m_FlipFullName + "_recent"));
            PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName(m_FlipFullName));
        }
        else
        {
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") == 0)
            {
                PlayerPrefs.SetString("ToLB_GameMode", "freeze");
                PlayerPrefs.SetString("ToLB_ScoreType", "point");
                PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName("freeze_recent"));
                PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName("freeze"));
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "mirror") == 0)
            {
                PlayerPrefs.SetString("ToLB_GameMode", "mirror");
                PlayerPrefs.SetString("ToLB_ScoreType", "point");
                PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName("mirror_recent"));
                PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName("mirror"));
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "zen") == 0)
            {
                PlayerPrefs.SetString("ToLB_GameMode", m_ZenFullName);
                PlayerPrefs.SetString("ToLB_ScoreType", "point");
                PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName(m_ZenFullName + "_recent"));
                PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName(m_ZenFullName));
            }
            else
            {
                PlayerPrefs.SetString("ToLB_GameMode", m_ArcadeFullName);
                PlayerPrefs.SetString("ToLB_ScoreType", "point");
                PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName(m_ArcadeFullName + "_recent"));
                PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName(m_ArcadeFullName));
            }
            /*
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") != 0)
            {
                // string arcadeFullModeName = "arcade" + PlayerPrefs.GetString("ArcadeMode");
                PlayerPrefs.SetString("ToLB_GameMode", m_ArcadeFullName);
                PlayerPrefs.SetString("ToLB_ScoreType", "point");
                PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName(m_ArcadeFullName + "_recent"));
                PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName(m_ArcadeFullName));
            }
            else
            {
                PlayerPrefs.SetString("ToLB_GameMode", "freeze");
                PlayerPrefs.SetString("ToLB_ScoreType", "point");
                PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName("freeze_recent"));
                PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName("freeze"));
            }
            */
        }
        Application.LoadLevel("_Leaderboard_");
    }

    // TODO: change banner
    private void InitializeData()
    {
        SceneToSceneData data = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        object[] dataArr = (object[])data.PayloadData;
        if (data != null)
        {
            m_ScoreValue = (int)dataArr[0];
        }
        ScoreValueGO.GetComponent<TextMesh>().text = m_ScoreValue.ToString();

        if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") != 0
            && string.Compare(PlayerPrefs.GetString("ArcadeMode"), "mirror") != 0
            && string.Compare(PlayerPrefs.GetString("ArcadeMode"), "zen") != 0)
        {
            m_Rank = (RankingSystem.Rank)dataArr[1];
            switch (m_Rank)
            {
                case RankingSystem.Rank.Rank_Novice:
                    {
                        BlurLayerGO.sprite = NoviceBlur;
                        MainLayerGO.sprite = NoviceMain;
                        break;
                    }
                case RankingSystem.Rank.Rank_RatherGood:
                    {
                        BlurLayerGO.sprite = RatherGoodBlur;
                        MainLayerGO.sprite = RatherGoodMain;
                        break;
                    }
                case RankingSystem.Rank.Rank_Pianist:
                    {
                        BlurLayerGO.sprite = PianistBlur;
                        MainLayerGO.sprite = PianistMain;
                        break;
                    }
                case RankingSystem.Rank.Rank_Genius:
                    {
                        BlurLayerGO.sprite = GeniusBlur;
                        MainLayerGO.sprite = GeniusMain;
                        break;
                    }
                case RankingSystem.Rank.Rank_Legendary:
                    {
                        BlurLayerGO.sprite = LegendaryBlur;
                        MainLayerGO.sprite = LegendaryMain;
                        break;
                    }
                case RankingSystem.Rank.Rank_Godlike:
                    {
                        BlurLayerGO.sprite = GodlikeBlur;
                        MainLayerGO.sprite = GodlikeMain;
                        break;
                    }
                default:
                    {
                        BlurLayerGO.sprite = NoviceBlur;
                        MainLayerGO.sprite = NoviceMain;
                        break;
                    }
            }
        }
        else
        {
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") == 0)
            {
                BlurLayerGO.sprite = FreezeBlur;
                MainLayerGO.sprite = FreezeMain;
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "zen") == 0)
            {
                BlurLayerGO.sprite = ZenBlur;
                MainLayerGO.sprite = ZenMain;
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "mirror") == 0)
            {
                BlurLayerGO.sprite = MirrorBlur;
                MainLayerGO.sprite = MirrorMain;
            }
        }

        Object.DestroyImmediate(data.gameObject);
    }

    private void UpdateLocalData()
    {
        string allTimeKeyStr = "";
        string recentKeyStr = "";
        if (PlayerPrefs.GetInt("HasLightning") == 1)
        {
            allTimeKeyStr = "blink"; recentKeyStr = "blink_recent";
        }
        else if (PlayerPrefs.GetInt("ReverseActivated") == 1)
        {
            m_FlipFullName = "flip";
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "4k") == 0)
            {
                if (PlayerPrefs.GetInt("IsMaster") == 0)
                    m_FlipFullName += "4";
                else m_FlipFullName += "4master";
            }
            else
            {
                if (PlayerPrefs.GetInt("IsMaster") == 0)
                    m_FlipFullName += "6";
                else m_FlipFullName += "6master";
            }
            allTimeKeyStr = m_FlipFullName; recentKeyStr = m_FlipFullName + "_recent";
        }
        else
        {
            if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "4k") == 0)
            {
                if (PlayerPrefs.GetInt("IsMaster") == 1)
                {
                    allTimeKeyStr = "arcade5k"; recentKeyStr = "arcade5k_recent";
                    m_ArcadeFullName = "arcade5k";
                }
                else
                {
                    allTimeKeyStr = "arcade4k"; recentKeyStr = "arcade4k_recent";
                    m_ArcadeFullName = "arcade4k";
                }
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "6k") == 0)
            {
                if (PlayerPrefs.GetInt("IsMaster") == 1)
                {
                    allTimeKeyStr = "arcade8k"; recentKeyStr = "arcade8k_recent";
                    m_ArcadeFullName = "arcade8k";
                }
                else
                {
                    allTimeKeyStr = "arcade6k"; recentKeyStr = "arcade6k_recent";
                    m_ArcadeFullName = "arcade6k";
                }
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "freeze") == 0)
            {
                allTimeKeyStr = "freeze"; recentKeyStr = "freeze_recent";
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "mirror") == 0)
            {
                allTimeKeyStr = "mirror"; recentKeyStr = "mirror_recent";
            }
            else if (string.Compare(PlayerPrefs.GetString("ArcadeMode"), "zen") == 0)
            {
                m_ZenFullName = "zen" + PlayerPrefs.GetInt("ZenPlayTime").ToString();
                allTimeKeyStr = m_ZenFullName; recentKeyStr = m_ZenFullName + "_recent";
            }
        }

        HighscoreServ.Instance.PushAlltimeScore(allTimeKeyStr, m_ScoreValue);
        HighscoreServ.Instance.PushRecentScore(recentKeyStr, m_ScoreValue);
        HighscoreServ.Instance.PushHistory(allTimeKeyStr, m_ScoreValue);

        List<float> arcadeHistory = HighscoreServ.Instance.GetHistory(allTimeKeyStr);
        PerformanceGraphGO.Initialize(arcadeHistory);
        List<float> otherButSameHistory = new List<float>(arcadeHistory);
        // HighscoreServ.Instance.GetHistory(allTimeKeyStr);
        otherButSameHistory.Sort();
        for (int i = 0; i < otherButSameHistory.Count; i++)
        {
            if ((int)otherButSameHistory[i] == m_ScoreValue)
            {
                InfoLabelGO.GetComponent<TextMesh>().text =
                    string.Format("This score ranks {0} \n in {1} recent scores.",
                    otherButSameHistory.Count - i, otherButSameHistory.Count);
                break;
            }
        }
        BestScoreValueGO.GetComponent<TextMesh>().text = HighscoreServ.Instance.GetAlltimeScore(allTimeKeyStr).ToString();
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        // show ads
        AdsServ.Instance.ShowPopupWithCountChecking();

        InitializeData();
        UpdateLocalData();
        // AnimationGroupGO.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
        // if (Input.GetKeyDown(KeyCode.Space))
        //     OnExitClicked();
	}
}

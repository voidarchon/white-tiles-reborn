﻿using UnityEngine;
using System.Collections;

/// <summary>
/// items need to be tracked:
///     - time between 2 success rows
///     - number of tile pressed
///     - number of row succeed
/// </summary>

public class ArcadeLogic : Singleton<ArcadeLogic> {

	[Header("GO References")]
	public ArcadeRowsStreamController ArcadeRowsStreamControllerGO;
    public UILabel UIScoreLabelGO;
    public UILabel UIScoreShadowLabelGO;
    public RankingSystem RankingGO;
    public GameObject LightningGO;
    public GameObject[] StartLabelGO;
    public GameObject StartOverlayGO;
    public Transform UIContainerGO;

    [Header("Ranking color")]
    public Color[] RankingColors;

	[Header("Gameplay's parameters")]
	[Range(0, 50)]
	public int SpeedLerpStep;
	public float TimeStep;
	public bool GameIsPause;
    public bool GameIsOver;

    public enum ArcadeGameMode { Arcade_4k, Arcade_6k };
    public ArcadeGameMode Difficulty;
    public float ScrollSpeed;
    public bool HasLightning = false;
    public float LightningInterval;

    [Header("6k Mode Exclusive")]
    [Range(0, 100)]
    public int TwoBlackProp;
    public float Normal6kScrollSpeed;
    public float Master6kScrollSpeed;

    [Header("4k Mode Exclusive")]
    public float Normal4kScrollSpeed;
    public float Master4kScrollSpeed;

    [Header("Reverse Mode Exclusive")]
    public bool ReverseActivated;
    public AnimationCurve ReverseInterpolation;
    public bool IsReversing;
    public bool IsDoingReverse;
    public float ReverseTime;
    public float ReverseInterval;
	
	[Header("Dont touch this, NOOB!")]
	public int StartRowId;
	[Range(0, 5)]
	public int StartIdOffset;
	public int CurrRowId;

    private bool m_SoundOn;
	private bool m_FirstTimeStartGame;
    private int m_Score;
    private int m_RowSuccess;
    private RankingSystem.Rank m_CurrentRank;

    private ArcadeMusicController m_ArcadeMusicController;

    private IEnumerator ReverseBoardRoutine(float rootAngle)
    {
        GameIsPause = true;
        IsDoingReverse = true;
        float startTime = Time.time;
        float t = 0;
        while (t < ReverseTime)
        {
            Vector3 angle = new Vector3(0.0f, 0.0f, 0.0f);
            t = Time.time - startTime;
            float r = ReverseInterpolation.Evaluate(t / ReverseTime);
            angle.z = (1.0f - r) * rootAngle + r * (180.0f + rootAngle);
            ArcadeRowsStreamControllerGO.transform.localRotation = Quaternion.Euler(angle);
            yield return null;
        }
        IsDoingReverse = false;
        GameIsPause = false;
        yield return null;
    }

    private void ReverseBoard()
    {
        if (!IsReversing)
        {
            StartCoroutine(ReverseBoardRoutine(0.0f));
            IsReversing = true;
        }
        else
        {
            StartCoroutine(ReverseBoardRoutine(180.0f));
            IsReversing = false;
        }
    }

	private IEnumerator SpeedChangerRoutine()
	{
		float currentSpeed = ArcadeRowsStreamControllerGO.ScrollSpeed;
		float maxSpeed = 2 * currentSpeed;
		float atomicTime = (maxSpeed - currentSpeed) / (SpeedLerpStep + 1);
		float t = 0.0f;
		while (currentSpeed < maxSpeed) {
			if (!GameIsPause)
			{
				t += Time.deltaTime;
				if (t > TimeStep)
				{
					t = 0.0f;
					currentSpeed += atomicTime;
					if (currentSpeed > maxSpeed)
						currentSpeed = maxSpeed;
					ArcadeRowsStreamControllerGO.ScrollSpeed = currentSpeed;
                    ScrollSpeed = currentSpeed;
				}
				yield return null;
			}
			yield return null;
		}
		Debug.Log ("max speed");
		yield return null;
	}

    private void TriggerLightning()
    {
        LightningGO.GetComponent<Animator>().SetTrigger("DoFlash");
    }

	private void OnDoneRowDelegate(GameObject sender, object data)
	{
		RowController doneRow = (RowController)data;
		CurrRowId++;
		ArcadeRowsStreamControllerGO.EnableNextRow (doneRow);
        m_RowSuccess++;
	}

	private void OnRowSuccessTapDelegate(GameObject sender, object data)
	{
        object[] dataArr = (object[])data;
        RowController theRow = (RowController)dataArr[0];
        int idHit = (int)dataArr[1];
		if (StartRowId + StartIdOffset == theRow.RowId) {
			GameIsPause = false;
			if (m_FirstTimeStartGame)
			{
                if (m_SoundOn)
                {
                    m_ArcadeMusicController.Shuffle();
                    m_ArcadeMusicController.PlayMusic();
                }
				StartCoroutine(SpeedChangerRoutine());
				m_FirstTimeStartGame = false;
                if (HasLightning)
                    InvokeRepeating("TriggerLightning", LightningInterval, LightningInterval);
                if (ReverseActivated)
                    InvokeRepeating("ReverseBoard", ReverseInterval, ReverseInterval);

                for (int i = 0; i < StartLabelGO.Length; i++)
                    if (StartLabelGO[i].activeInHierarchy)
                    {
                        StartLabelGO[i].transform.parent = UIContainerGO;
                        StartLabelGO[i].SetActive(false);
                    }
			}
		}
        m_Score++;
        SetScore(m_Score);
        CheckRanking();
	}

	private void OnRowFailedTapDelegate(GameObject sender, object data)
	{
		RowController failedRow = (RowController)data;
		GameIsPause = true;
        GameIsOver = true;
        // pass data
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score, m_CurrentRank });
        if (HasLightning)
        {
            LightningGO.SetActive(false);
            CancelInvoke("TriggerLightning");
        }

        if (ReverseActivated)
        {
            CancelInvoke("ReverseBoard");
            StopAllCoroutines();
        }
        if (m_SoundOn)
            m_ArcadeMusicController.StopMusic();
        Invoke("WaitThenEndGame", 2.0f);
	}

	private void OnArcadeRowMissDelegate(GameObject sender, object data)
	{
		// TODO: should not pause game outside of Logic class
		RowController missedRow = (RowController)data;
        GameIsPause = true;
        GameIsOver = true;
        // pass data
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score, m_CurrentRank });

        if (HasLightning)
        {
            LightningGO.SetActive(false);
            CancelInvoke("TriggerLightning");
        }

        if (ReverseActivated)
        {
            CancelInvoke("ReverseBoard");
        }
        if (m_SoundOn)
            m_ArcadeMusicController.StopMusic();
        Invoke("WaitThenEndGame", 2.0f);
	}

    private void OnArcadeStartRowGone(GameObject sender, object data)
    {
        StartOverlayGO.transform.parent = UIContainerGO;
        StartOverlayGO.SetActive(false);
    }

    private void WaitThenEndGame()
    {
        Application.LoadLevel("_EndScene_ArcadeMode_Universal_");
    }

	private void RegisterAllNotifications()
	{

	}

	private void RegisterAllListeners()
	{
		NotificationCenter.GetInstance ().RegisterListener ("RowDonePlay", OnDoneRowDelegate);
		NotificationCenter.GetInstance ().RegisterListener ("RowSuccessTap", OnRowSuccessTapDelegate);
		NotificationCenter.GetInstance ().RegisterListener ("RowFailedTap", OnRowFailedTapDelegate);
		NotificationCenter.GetInstance ().RegisterListener ("Arcade_RowMiss", OnArcadeRowMissDelegate);
        NotificationCenter.GetInstance().RegisterListener("Arcade_StartRowGone", OnArcadeStartRowGone);
	}

    private IEnumerator LerpTileColor(Color from, Color to)
    {
        float timeLerp = 0.5f;
        float t = 0.0f;
        float startTime = Time.time;
        while (t < timeLerp)
        {
            t = Time.time - startTime;
            ArcadeRowsStreamControllerGO.SetBlackTilesColor(Color.Lerp(from, to, t / timeLerp));
            yield return null;
        }
        yield return null;
    }

    private void CheckRanking()
    {
        if (m_Score == 20)
        {
            RankingGO.ShowBanner(RankingSystem.Rank.Rank_Novice);
            m_CurrentRank = RankingSystem.Rank.Rank_Novice;
            StartCoroutine(LerpTileColor(Color.black, RankingColors[0]));
        }
        else if (m_Score == 50)
        {
            RankingGO.ShowBanner(RankingSystem.Rank.Rank_RatherGood);
            m_CurrentRank = RankingSystem.Rank.Rank_RatherGood;
            StartCoroutine(LerpTileColor(RankingColors[0], RankingColors[1]));
        }
        else if (m_Score == 100)
        {
            RankingGO.ShowBanner(RankingSystem.Rank.Rank_Pianist);
            m_CurrentRank = RankingSystem.Rank.Rank_Pianist;
            StartCoroutine(LerpTileColor(RankingColors[1], RankingColors[2]));
        }
        else if (m_Score == 200)
        {
            RankingGO.ShowBanner(RankingSystem.Rank.Rank_Genius);
            m_CurrentRank = RankingSystem.Rank.Rank_Genius;
            StartCoroutine(LerpTileColor(RankingColors[2], RankingColors[3]));
        }
        else if (m_Score == 400)
        {
            RankingGO.ShowBanner(RankingSystem.Rank.Rank_Legendary);
            m_CurrentRank = RankingSystem.Rank.Rank_Legendary;
            StartCoroutine(LerpTileColor(RankingColors[3], RankingColors[4]));
        }
        else if (m_Score == 800)
        {
            RankingGO.ShowBanner(RankingSystem.Rank.Rank_Godlike);
            m_CurrentRank = RankingSystem.Rank.Rank_Godlike;
            StartCoroutine(LerpTileColor(RankingColors[4], RankingColors[5]));
        }
    }

    private void SetScore(int score)
    {
        string scoreStr = score.ToString();
        UIScoreLabelGO.SetText(scoreStr);
        UIScoreShadowLabelGO.SetText(scoreStr);
    }

    private void Initialize()
    {
        m_FirstTimeStartGame = true;
        GameIsOver = false;
        IsDoingReverse = false;
        IsReversing = false;
        m_Score = 0;
        m_RowSuccess = 0;
        m_CurrentRank = RankingSystem.Rank.Rank_Novice;
        CurrRowId = StartRowId + StartIdOffset;
        SetScore(m_Score);
        string modeStr = PlayerPrefs.GetString("ArcadeMode");
        int isMaster = PlayerPrefs.GetInt("IsMaster");
        if (PlayerPrefs.GetInt("game_settings_sound") == 1)
            m_SoundOn = true;
        else m_SoundOn = false;
        if (string.Compare(modeStr, "6k") == 0)
        {
            Difficulty = ArcadeGameMode.Arcade_6k;
            StartOverlayGO.GetComponent<StartOverlay>().Initialize(6);
            StartLabelGO[0].transform.localScale = new Vector3(0.33f, 0.33f, 1.0f);
            StartLabelGO[1].transform.localScale = new Vector3(0.33f, 0.33f, 1.0f);
            if (isMaster == 1)
                ScrollSpeed = Master6kScrollSpeed;
            else
                ScrollSpeed = Normal6kScrollSpeed;
        }
        else if (string.Compare(modeStr, "4k") == 0)
        {
            Difficulty = ArcadeGameMode.Arcade_4k;
            StartOverlayGO.GetComponent<StartOverlay>().Initialize(4);
            StartLabelGO[0].transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
            StartLabelGO[1].transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
            if (isMaster == 1)
                ScrollSpeed = Master4kScrollSpeed;
            else
                ScrollSpeed = Normal4kScrollSpeed;
        }
        int hasLightning = PlayerPrefs.GetInt("HasLightning");
        if (hasLightning == 1)
            HasLightning = true;
        else HasLightning = false;

        int reverseActivated = PlayerPrefs.GetInt("ReverseActivated");
        if (reverseActivated == 1)
            ReverseActivated = true;
        else ReverseActivated = false;
    }

	void Awake() {
		RegisterAllNotifications ();
        Randomkun.GetInstance().ForgetEverything();
        m_ArcadeMusicController = GetComponent<ArcadeMusicController>();
	}

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        Initialize();
		ArcadeRowsStreamControllerGO.DoInit ();
		RegisterAllListeners ();
	}
	
	// Update is called once per frame
	void Update () {
	}
}
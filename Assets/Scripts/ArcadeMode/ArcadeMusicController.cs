﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (AudioSource))]
public class ArcadeMusicController : MonoBehaviour {

    public List<AudioClip> BackgroundMusic;
    private int m_SongPlayed;
    private AudioSource m_AudioSource;

    public void Shuffle()
    {
        for (int i = 0; i < BackgroundMusic.Count; i++)
        {
            int j = Random.Range(0, BackgroundMusic.Count);
            Debug.Log(j);
            AudioClip tmp = BackgroundMusic[j];
            BackgroundMusic[j] = BackgroundMusic[i];
            BackgroundMusic[i] = tmp;
        }
    }

    public void PlayMusic()
    {
        m_AudioSource.PlayOneShot(BackgroundMusic[m_SongPlayed]);
        Invoke("PlayMusic", BackgroundMusic[m_SongPlayed].length);
        m_SongPlayed++;
        if (m_SongPlayed == BackgroundMusic.Count)
        {
            Shuffle();
            m_SongPlayed = 0;
        }
    }

    public void StopMusic()
    {
        CancelInvoke("PlayMusic");
        m_AudioSource.Stop();
    }

    void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
        m_SongPlayed = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

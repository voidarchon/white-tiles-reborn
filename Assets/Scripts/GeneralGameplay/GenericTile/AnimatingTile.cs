﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class AnimatingTile : MonoBehaviour {

    [Header("Size of Tile")]
    public float Width;
    public float Height;

    // Tile's mesh data
    private Vector3[] m_Vertices;
    private Vector2[] m_UVs;
    private int[] m_Indices;
    private Mesh m_Mesh;

    // Cached components
    private MeshFilter m_MeshFilter;
    private MeshRenderer m_MeshRenderer;

    public void GenerateTileMesh(Color fillColor)
    {
        float z = transform.localPosition.z;
        m_Vertices = new Vector3[] {
            new Vector3(-Width/2.0f, -Height/2.0f, z),
            new Vector3(-Width/2.0f, Height/2.0f, z),
            new Vector3(Width/2.0f, Height/2.0f, z),
            new Vector3(Width/2.0f, -Height/2.0f, z)
        };
//         m_UVs = new Vector2[] {
//             new Vector2(-Width/2.0f, -Height/2.0f),
//             new Vector2(-Width/2.0f, Height/2.0f),
//             new Vector2(Width/2.0f, Height/2.0f),
//             new Vector2(Width/2.0f, -Height/2.0f)
//         };
        m_UVs = new Vector2[] {
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(1.0f, 0.0f)
        };
        m_Indices = new int[] { 0, 1, 2, 2, 3, 0 };
        m_MeshRenderer.material.SetColor("_FillColor", fillColor);

        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UVs;
        m_Mesh.triangles = m_Indices;

        m_MeshFilter.mesh = m_Mesh;
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

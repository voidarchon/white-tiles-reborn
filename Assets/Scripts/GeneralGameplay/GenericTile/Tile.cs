﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class Tile : MonoBehaviour
{
    [Header("Size of Tile")]
    public float Width;
    public float Height;

    // Tile's mesh data
    private Vector3[] m_Vertices;
    private Vector2[] m_UVs;
    private int[] m_Indices;
    private Color[] m_VertexColors;
    private Mesh m_Mesh;

    // Cached components
    private MeshFilter m_MeshFilter;

    public void GenerateTileMesh(Color fillColor)
    {
		// prevent memory leak
        if (m_Mesh != null)
        {
            SetColor(fillColor);
            return;
            // Object.Destroy(m_Mesh);
        }

        float z = transform.localPosition.z;
        m_Vertices = new Vector3[] {
            new Vector3(-Width/2.0f, -Height/2.0f, z),
            new Vector3(-Width/2.0f, Height/2.0f, z),
            new Vector3(Width/2.0f, Height/2.0f, z),
            new Vector3(Width/2.0f, -Height/2.0f, z)
        };
        m_UVs = new Vector2[] {
            new Vector2(-Width/2.0f, -Height/2.0f),
            new Vector2(-Width/2.0f, Height/2.0f),
            new Vector2(Width/2.0f, Height/2.0f),
            new Vector2(Width/2.0f, -Height/2.0f)
        };
        m_Indices = new int[] { 0, 1, 2, 2, 3, 0 };
        m_VertexColors = new Color[4];
        for (int i = 0; i < 4; i++)
            m_VertexColors[i] = fillColor;

        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices;
        m_Mesh.uv = m_UVs;
        m_Mesh.triangles = m_Indices;
        m_Mesh.colors = m_VertexColors;

        m_MeshFilter.mesh = m_Mesh;
    }

    public void SetColor(Color tileColor)
    {
        if (m_VertexColors != null)
        {
            for (int i = 0; i < 4; i++)
                m_VertexColors[i] = tileColor;
        }
        if (m_Mesh != null)
            m_Mesh.colors = m_VertexColors;
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}

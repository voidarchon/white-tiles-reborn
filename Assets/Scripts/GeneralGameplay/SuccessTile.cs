﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AnimatingTile))]
public class SuccessTile : MonoBehaviour {

    [Header("Tile's Color")]
    public Color FillColor;

    // Cached components
    private AnimatingTile m_AnimatingTile;

    void Awake()
    {
        m_AnimatingTile = GetComponent<AnimatingTile>();
    }

	// Use this for initialization
	void Start () {
        m_AnimatingTile.GenerateTileMesh(FillColor);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

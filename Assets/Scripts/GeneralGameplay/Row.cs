﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Row : MonoBehaviour {

    [Header("Rows configuration")]
    [Range(4, 10)]
    public int RowPerScreen = 4;
    [Range(4, 6)]
    public int TilePerRow = 4;

    [Header("Row's properties")]
    public int[] BlackTilePosition;

    [Header("Tiles Prefabs")]
    public GameObject PrefabWhiteTile;
    public GameObject PrefabBlackTile;
    public GameObject PrefabSuccessTile;
    public GameObject PrefabFailedTile;

    private List<Tile> m_WhiteTiles;
    private List<Tile> m_BlackTile;
    private List<GameObject> m_SuccessTile;
    private GameObject m_FailedTile;

    // Tile's size
    private float m_TileWidth;
	public float TileWidth {
		get {
			return m_TileWidth;
		}
	}
    private float m_TileHeight;
	public float TileHeight {
		get {
			return m_TileHeight;
		}
	}
    private float m_PixelSize;
	public float PixelSize {
		get {
			return m_PixelSize;
		}
	}
    private float m_ScreenMostLeft;
	public float ScreenMostLeft {
		get {
			return m_ScreenMostLeft;
		}
	}

	public int GetBlackTileInPos(Vector3 pos, float hitboxScale)
	{
		for (int i = 0; i < m_BlackTile.Count; i++) {
			if (!m_SuccessTile[i].activeInHierarchy) 
			{
				float topY = m_BlackTile[i].transform.position.y + m_TileHeight / 2.0f * hitboxScale;
				float bottomY = topY - m_TileHeight * hitboxScale;
				float leftX = m_BlackTile[i].transform.position.x - m_TileWidth / 2.0f * hitboxScale;
				float rightX = leftX + m_TileWidth * hitboxScale;
				if (pos.y <= topY && pos.y >= bottomY && pos.x >= leftX && pos.x <= rightX) {
					return i;
				}
			}
		}
		return -1;
	}

    public GameObject GetBlackTileById(int index)
    {
        return m_BlackTile[index].gameObject;
    }

    private void ComputeTileSize()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;
        m_PixelSize = pixelSize;
        m_ScreenMostLeft = - floatWidth / 2.0f;

        m_TileHeight = (floatHeight - (RowPerScreen + 1) * pixelSize) / RowPerScreen;
        m_TileWidth = (floatWidth - (TilePerRow + 1) * pixelSize) / TilePerRow;
    }

    private void GenerateTiles()
    {
        ComputeTileSize();
        m_WhiteTiles = new List<Tile>(TilePerRow);
		m_BlackTile = new List<Tile> (BlackTilePosition.Length);
		m_SuccessTile = new List<GameObject> (BlackTilePosition.Length);
        for (int i = 0; i < TilePerRow; i++)
        {
            GameObject tileObj = Instantiate(PrefabWhiteTile) as GameObject;
            tileObj.GetComponent<Tile>().Width = m_TileWidth;
            tileObj.GetComponent<Tile>().Height = m_TileHeight;
			tileObj.transform.parent = transform;
            Vector3 pos = new Vector3();
            pos.y = 0;
            pos.z = 0;
            pos.x = i * (m_PixelSize + m_TileWidth) 
                + (m_PixelSize + m_TileWidth / 2.0f) 
                + m_ScreenMostLeft;
            tileObj.transform.localPosition = pos;
            m_WhiteTiles.Add(tileObj.GetComponent<Tile>());
        }
        {
			for (int i = 0; i < BlackTilePosition.Length; i++)
			{
            	GameObject tileObj = Instantiate(PrefabBlackTile) as GameObject;
				tileObj.transform.parent = transform;
            	tileObj.GetComponent<Tile>().Width = m_TileWidth;
            	tileObj.GetComponent<Tile>().Height = m_TileHeight;
            	tileObj.SetActive(false);
            	m_BlackTile.Add(tileObj.GetComponent<Tile>());
			}
        }

        {
			for (int i = 0; i < BlackTilePosition.Length; i++) {
            	GameObject obj = Instantiate(PrefabSuccessTile) as GameObject;
            	obj.GetComponent<AnimatingTile>().Width = m_TileWidth;
            	obj.GetComponent<AnimatingTile>().Height = m_TileHeight;
            	obj.transform.parent = transform;
            	obj.SetActive(false);
				m_SuccessTile.Add(obj);
			}

            m_FailedTile = Instantiate(PrefabFailedTile) as GameObject;
            m_FailedTile.GetComponent<AnimatingTile>().Width = m_TileWidth;
            m_FailedTile.GetComponent<AnimatingTile>().Height = m_TileHeight;
            m_FailedTile.transform.parent = transform;
            m_FailedTile.SetActive(false);
        }
    }

	private void OnFailedTap(int positionId)
	{
		Vector3 pos = new Vector3();
		pos.y = 0;
		pos.z = -0.2f;
		pos.x = positionId * (m_PixelSize + m_TileWidth)
			+ (m_PixelSize + m_TileWidth / 2.0f)
				+ m_ScreenMostLeft;
		m_FailedTile.transform.localPosition = pos;
		m_FailedTile.SetActive (true);
	}

	// Message receiver - from RowController

	public void SetBlackTileColor(Color color)
	{
		for (int i = 0; i < m_BlackTile.Count; i++)
			m_BlackTile [i].GetComponent<BlackTile> ().SetColor (color);
	}

	public void SetWhiteTileColor(Color color)
	{
		for (int i = 0; i < TilePerRow; i++)
			m_WhiteTiles [i].GetComponent<WhiteTile> ().SetColor (color);
	}

	// can use to generate new row before adjusting public variable: BlackTilePosition
	private void OnResetRowAppearance()
	{
		m_FailedTile.SetActive (false);
		for (int i = 0; i <m_SuccessTile.Count; i++)
			m_SuccessTile[i].SetActive (false);

		for (int i = 0; i < TilePerRow; i++)
			m_WhiteTiles [i].gameObject.SetActive (true);
		// Set black tile position
		for (int i = 0; i < BlackTilePosition.Length; i++) {
			Vector3 pos = new Vector3 ();
			pos.y = 0;
			pos.z = -0.1f;
			pos.x = BlackTilePosition[i] * (m_PixelSize + m_TileWidth)
				+ (m_PixelSize + m_TileWidth / 2.0f)
				+ m_ScreenMostLeft;
			m_BlackTile[i].transform.localPosition = pos;
			m_BlackTile[i].gameObject.SetActive (true);
			m_WhiteTiles [BlackTilePosition[i]].gameObject.SetActive (false);
		}
	}

	private void OnSuccessTap(int blackTileId)
	{
		Vector3 pos = new Vector3();
		pos.y = 0;
		pos.z = -0.2f;
		pos.x = BlackTilePosition[blackTileId] * (m_PixelSize + m_TileWidth)
			+ (m_PixelSize + m_TileWidth / 2.0f)
				+ m_ScreenMostLeft;

		m_SuccessTile[blackTileId].transform.localPosition = pos;
		m_SuccessTile[blackTileId].SetActive (true);
	}

    void Awake()
    {
		GenerateTiles ();
		OnResetRowAppearance ();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
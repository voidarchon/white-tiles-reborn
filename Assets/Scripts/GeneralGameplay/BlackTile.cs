﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Tile))]
public class BlackTile : MonoBehaviour {

    [Header("Tile's Color")]
    public Color FillColor;

    // Cached components
    private Tile m_Tile;

	public void SetColor(Color color)
	{
		if (color != FillColor) {
			FillColor = color;
			m_Tile.GenerateTileMesh (color);
		}
	}

    void Awake()
    {
        m_Tile = GetComponent<Tile>();
    }

	// Use this for initialization
	void Start () {
        m_Tile.GenerateTileMesh(FillColor);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

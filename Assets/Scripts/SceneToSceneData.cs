﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneToSceneData : MonoBehaviour {

    public object PayloadData;

    public void SetPayLoadData(object data)
    {
        PayloadData = data;
        DontDestroyOnLoad(gameObject);
    }

    void Awake()
    {
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class RankingSystem : MonoBehaviour {

    [Header("Novice")]
    public Sprite NoviceMain;
    public Sprite NoviceBlur;

    [Header("Rather Good")]
    public Sprite RatherGoodMain;
    public Sprite RatherGoodBlur;

    [Header("Pianist")]
    public Sprite PianistMain;
    public Sprite PianistBlur;

    [Header("Genius")]
    public Sprite GeniusMain;
    public Sprite GeniusBlur;

    [Header("Legendary")]
    public Sprite LegendaryMain;
    public Sprite LegendaryBlur;

    [Header("Godlike")]
    public Sprite GodlikeMain;
    public Sprite GodlikeBlur;

    public enum Rank
    {
        Rank_Novice, Rank_RatherGood, Rank_Pianist,
        Rank_Genius, Rank_Legendary, Rank_Godlike
    };

    public GameObject BannerGO;
    public SpriteRenderer OverlayGO;
    public SpriteRenderer MainGO;

    public void ShowBanner(Rank rank)
    {
        if (BannerGO.activeInHierarchy)
            return;
        switch (rank)
        {
            case Rank.Rank_Novice:
                {
                    OverlayGO.sprite = NoviceBlur;
                    MainGO.sprite = NoviceMain;
                    break;
                }
            case Rank.Rank_RatherGood:
                {
                    OverlayGO.sprite = RatherGoodBlur;
                    MainGO.sprite = RatherGoodMain;
                    break;
                }
            case Rank.Rank_Pianist:
                {
                    OverlayGO.sprite = PianistBlur;
                    MainGO.sprite = PianistMain;
                    break;
                }
            case Rank.Rank_Genius:
                {
                    OverlayGO.sprite = GeniusBlur;
                    MainGO.sprite = GeniusMain;
                    break;
                }
            case Rank.Rank_Legendary:
                {
                    OverlayGO.sprite = LegendaryBlur;
                    MainGO.sprite = LegendaryMain;
                    break;
                }
            case Rank.Rank_Godlike:
                {
                    OverlayGO.sprite = GodlikeBlur;
                    MainGO.sprite = GodlikeMain;
                    break;
                }
        }
        BannerGO.gameObject.SetActive(true);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.N))
            ShowBanner(Rank.Rank_Novice);
        if (Input.GetKeyDown(KeyCode.R))
            ShowBanner(Rank.Rank_RatherGood);
	}
}

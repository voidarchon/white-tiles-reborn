﻿using UnityEngine;
using System.Collections;

public class RankBannner : MonoBehaviour {

    public TextMesh RankTextGO;
    private UIMesh m_UIMesh;
    private Animator m_Animator;

    public void Initialize(Color bgColor, Color textColor, string text)
    {
        GetComponent<UIMesh>().ChangeColor(bgColor);
        RankTextGO.text = text;
        RankTextGO.color = textColor;
    }

    private void SelfDisable()
    {
        m_Animator.enabled = false;
        transform.localPosition = new Vector3(0.0f, 3.75f, 0.0f);
        gameObject.SetActive(false);
    }

    private void AutoLeadOut()
    {
        m_Animator.SetBool("LeadOut", true);
    }

    void OnEnable()
    {
        m_Animator.enabled = true;
        Invoke("AutoLeadOut", 2.5f);
    }

    void Awake()
    {
        m_UIMesh = GetComponent<UIMesh>();
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

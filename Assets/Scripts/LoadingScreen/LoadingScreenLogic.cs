﻿using UnityEngine;
using System.Collections;

public class LoadingScreenLogic : MonoBehaviour {
    [Header("Highscore")]
    public string HighscoreDataPath;
    [TextArea(5, 10)]
    public string DefaultContent;
    public string Version;

    [Header("Game Settings")]
    public string SettingsFileName;
    [TextArea(5, 10)]
    public string SettingsDefaultContent;
    public string SettingsVersion;

    public GameObject LoadingIndicatorGO;

    [Header("Debug")]
    public bool DBForeverLoad;

    private void SimulateLoading()
    {
        LoadingIndicatorGO.SetActive(false);
        Application.LoadLevel("_MainMenu_");
    }

    private void LoadGameSettings()
    {
        GameSettingsManager.Instance.Initialize(SettingsFileName, SettingsDefaultContent, SettingsVersion);
        if (GameSettingsManager.Instance.GetBoolSetting("sound"))
        {
            PlayerPrefs.SetInt("game_settings_sound", 1);
        }
        else
        {
            PlayerPrefs.SetInt("game_settings_sound", 0);
        }
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        LBSettingsManager.Instance.Initialize();
        HighscoreDataPath = Application.persistentDataPath + "/highscore.xml";
        
        LoadingIndicatorGO.SetActive(true);
        HighscoreServ.Instance.Initialize(HighscoreDataPath, DefaultContent, Version);
        LoadGameSettings();
        if (!DBForeverLoad)
            Invoke("SimulateLoading", 3.0f);
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}

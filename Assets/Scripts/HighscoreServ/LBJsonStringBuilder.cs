﻿using UnityEngine;
using System.Collections;

public class LBFacebookInfo {
    public string FirstName;
    public string LastName;
    public string Id;
    public string Gender;
    public string Link;
    public string FullName;

    public LBFacebookInfo(string firstName, string lastName, string id, string gender,
        string link, string fullName)
    {
        FirstName = firstName;
        LastName = lastName;
        Id = id;
        Gender = gender;
        Link = link;
        FullName = fullName;
    }
}

public class LBJsonStringBuilder
{
    private static LBJsonStringBuilder m_Instance;
    public static LBJsonStringBuilder Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new LBJsonStringBuilder();
                return m_Instance;
            }
            return m_Instance;
        }
    }

    private LBJsonStringBuilder() { }

    public string BuildGetScoreStr(string os, string did, string boardName,
        string fid, string appId, int range)
    {
        string retStr = "{\"end\":\"" + range + "\",\"os\":\""
                      + os
                      + "\",\"did\":\""
                      + did
                      + "\",\"order\":\"descending\",\"boardname\":\""
                      + boardName
                      + "\",\"fid\":\""
                      + fid
                      + "\",\"appid\":\""
                      + appId
                      + "\",\"start\":\"0\"}";
        return retStr;
    }

    public string BuildSignInStr(string os, string appId, string did, 
        LBFacebookInfo fbInfo )
    {
        string retStr = "{\"appid\":\"" + appId + "\",\"did\":\""
                     + did
                     + "\",\"fb_basic_info\":{\"first_name\":\""
                     + fbInfo.FirstName
                     + "\",\"gender\":\""
                     + fbInfo.Gender
                     + "\",\"id\":\""
                     + fbInfo.Id
                     + "\",\"last_name\":\""
                     + fbInfo.LastName
                     + "\",\"link\":\""
                     + fbInfo.Link
                     + "\",\"name\":\""
                     + fbInfo.FullName
                     + "\"},\"os\":\""
                     + os
                     + "\"}";
        return retStr;
    }

    public string BuildPostScoreStr(string os, string appId, string did, string boardName, 
        int expiredMillis, string fid, int score)
    {
        string retStr = "{\"appid\":\"" + appId + "\",\"boardname\":\""
            + boardName
            + "\",\"did\":\""
            + did
            + "\",\"expired\":\"" + expiredMillis.ToString() +  "\",\"fid\":\""
            + fid
            + "\",\"order\":\"descending\",\"os\":\""
            + os
            + "\",\"score\":\""
            + score
            + "\"}";
        return retStr;
    }
}

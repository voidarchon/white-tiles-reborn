﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.IO;
using MiniJSON;

public class LBSettingsManager
{
    private static LBSettingsManager m_Instance;
    public static LBSettingsManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new LBSettingsManager();
                return m_Instance;
            }
            return m_Instance;
        }
    }

    private LBSettingsManager()
    {
        m_LBInfo = new Dictionary<string, string>();
        SetDefaultValues();
    }

    // game specification
    private string m_ClassicAllTimeBoardName;
    public string ClassicAllTimeBoardName
    {
        get
        {
            return m_ClassicAllTimeBoardName;
        }
    }
    private string m_ClassicRecentBoardName;
    public string ClassicRecentBoardName
    {
        get
        {
            return m_ClassicRecentBoardName;
        }
    }

    private string m_Arcade4kAllTimeBoardName;
    public string Arcade4kAllTimeBoardName
    {
        get
        {
            return m_Arcade4kAllTimeBoardName;
        }
    }
    private string m_Arcade4kRecentBoardName;
    public string Arcade4kRecentBoardName
    {
        get
        {
            return m_Arcade4kRecentBoardName;
        }
    }

    private string m_Arcade6kAllTimeBoardName;
    public string Arcade6kAllTimeBoardName
    {
        get
        {
            return m_Arcade6kAllTimeBoardName;
        }
    }
    private string m_Arcade6kRecentBoardName;
    public string Arcade6kRecentBoardName
    {
        get
        {
            return m_Arcade6kRecentBoardName;
        }
    }

    private Dictionary<string, string> m_LBInfo;

    private string m_MoreGames;

    private void SetDefaultValues()
    {
        // classic mode
        m_LBInfo.Add("classic20", "Classic20 Alltime Generic");
        m_LBInfo.Add("classic20_recent", "Classic20 Recent Generic");

        m_LBInfo.Add("classic35", "Classic35 Alltime Generic");
        m_LBInfo.Add("classic35_recent", "Classic35 Recent Generic");

        m_LBInfo.Add("classic50", "Classic50 Alltime Generic");
        m_LBInfo.Add("classic50_recent", "Classic50 Recent Generic");

        m_LBInfo.Add("classic75", "Classic75 Alltime Generic");
        m_LBInfo.Add("classic75_recent", "Classic75 Recent Generic");

        // arcade mode
        m_LBInfo.Add("arcade4k", "Arcade4k Alltime Generic");
        m_LBInfo.Add("arcade4k_recent", "Arcade4k Recent Generic");

        m_LBInfo.Add("arcade5k", "Arcade5k Alltime Generic");
        m_LBInfo.Add("arcade5k_recent", "Arcade5k Recent Generic");

        m_LBInfo.Add("arcade6k", "Arcade6k Alltime Generic");
        m_LBInfo.Add("arcade6k_recent", "Arcade6k Recent Generic");

        m_LBInfo.Add("arcade8k", "Arcade8k Alltime Generic");
        m_LBInfo.Add("arcade8k_recent", "Arcade8k Recent Generic");

        // blink mode
        m_LBInfo.Add("blink", "Blink Alltime Generic");
        m_LBInfo.Add("blink_recent", "Blink Recent Generic");

        // freeze mode
        m_LBInfo.Add("freeze", "Freeze Alltime Generic");
        m_LBInfo.Add("freeze_recent", "Freeze Recent Generic");

        // version 2.0
        // flip mode
        m_LBInfo.Add("flip4", "Flip4 Alltime Generic");
        m_LBInfo.Add("flip4_recent", "Flip4 Recent Generic");
        m_LBInfo.Add("flip4master", "Flip4Master Alltime Generic");
        m_LBInfo.Add("flip4master_recent", "Flip4Master Recent Generic");
        m_LBInfo.Add("flip6", "Flip6 Alltime Generic");
        m_LBInfo.Add("flip6_recent", "Flip6 Recent Generic");
        m_LBInfo.Add("flip6master", "Flip6Master Alltime Generic");
        m_LBInfo.Add("flip6master_recent", "Flip6Master Recent Generic");
        
        // dual mode
        m_LBInfo.Add("dual", "Dual Alltime Generic");
        m_LBInfo.Add("dual_recent", "Dual Recent Generic");

        // mirror mode
        m_LBInfo.Add("mirror", "Mirror Alltime Generic");
        m_LBInfo.Add("mirror_recent", "Mirror Recent Generic");

        // zen mode
        m_LBInfo.Add("zen7", "Zen7 Alltime Generic");
        m_LBInfo.Add("zen7_recent", "Zen7 Recent Generic");
        m_LBInfo.Add("zen9", "Zen9 Alltime Generic");
        m_LBInfo.Add("zen9_recent", "Zen9 Recent Generic");
        m_LBInfo.Add("zen12", "Zen12 Alltime Generic");
        m_LBInfo.Add("zen12_recent", "Zen12 Recent Generic");
        m_LBInfo.Add("zen15", "Zen15 Alltime Generic");
        m_LBInfo.Add("zen15_recent", "Zen15 Recent Generic");
    }

    public string GetLBName(string boardId)
    {
        return m_LBInfo[boardId];
    }

    /// <summary>
    /// not be used anymore cuz it ate too much data bandwidth.
    /// </summary>
    /// <param name="jsonStr"></param>
    private void ProcessJSONString(string jsonStr)
    {
        Debug.Log(jsonStr);
        Dictionary<string, object> rubbish = MiniJSON.Json.Deserialize(jsonStr) as Dictionary<string, object>;
        Dictionary<string, object> allConfigs = rubbish["config"] as Dictionary<string, object>;
        Dictionary<string, object> leaderboardConfig = allConfigs["leaderboard_config"] as Dictionary<string, object>;
        
        Dictionary<string, object> modeConfig;
        // classicmode
        modeConfig = leaderboardConfig["classic_mode"] as Dictionary<string, object>;
        m_ClassicAllTimeBoardName = (string)modeConfig["alltime"];
        m_ClassicRecentBoardName = (string)modeConfig["recent"];

        #region ClassicMode
        modeConfig = leaderboardConfig["classic20_mode"] as Dictionary<string, object>;
        m_LBInfo["classic20"] = (string)modeConfig["alltime"];
        m_LBInfo["classic20_recent"] = (string)modeConfig["recent"];

        modeConfig = leaderboardConfig["classic35_mode"] as Dictionary<string, object>;
        m_LBInfo["classic35"] = (string)modeConfig["alltime"];
        m_LBInfo["classic35_recent"] = (string)modeConfig["recent"];

        modeConfig = leaderboardConfig["classic50_mode"] as Dictionary<string, object>;
        m_LBInfo["classic50"] = (string)modeConfig["alltime"];
        m_LBInfo["classic50_recent"] = (string)modeConfig["recent"];

        modeConfig = leaderboardConfig["classic75_mode"] as Dictionary<string, object>;
        m_LBInfo["classic75"] = (string)modeConfig["alltime"];
        m_LBInfo["classic75_recent"] = (string)modeConfig["recent"];
        #endregion

        // arcade4kmode
        modeConfig = leaderboardConfig["arcade4k_mode"] as Dictionary<string, object>;
        m_Arcade4kAllTimeBoardName = (string)modeConfig["alltime"];
        m_Arcade4kRecentBoardName = (string)modeConfig["recent"];

        // arcade6kmode
        modeConfig = leaderboardConfig["arcade6k_mode"] as Dictionary<string, object>;
        m_Arcade6kAllTimeBoardName = (string)modeConfig["alltime"];
        m_Arcade6kRecentBoardName = (string)modeConfig["recent"];

        #region ArcadeMode
        modeConfig = leaderboardConfig["arcade4k_mode"] as Dictionary<string, object>;
        m_LBInfo["arcade4k"] = (string)modeConfig["alltime"];
        m_LBInfo["arcade4k_recent"] = (string)modeConfig["recent"];

        modeConfig = leaderboardConfig["arcade5k_mode"] as Dictionary<string, object>;
        m_LBInfo["arcade5k"] = (string)modeConfig["alltime"];
        m_LBInfo["arcade5k_recent"] = (string)modeConfig["recent"];

        modeConfig = leaderboardConfig["arcade6k_mode"] as Dictionary<string, object>;
        m_LBInfo["arcade6k"] = (string)modeConfig["alltime"];
        m_LBInfo["arcade6k_recent"] = (string)modeConfig["recent"];

        modeConfig = leaderboardConfig["arcade8k_mode"] as Dictionary<string, object>;
        m_LBInfo["arcade8k"] = (string)modeConfig["alltime"];
        m_LBInfo["arcade8k_recent"] = (string)modeConfig["recent"];
        #endregion

        // blink mode
        modeConfig = leaderboardConfig["blink_mode"] as Dictionary<string, object>;
        m_LBInfo["blink"] = (string)modeConfig["alltime"];
        m_LBInfo["blink_recent"] = (string)modeConfig["recent"];

        // freeze mode
        modeConfig = leaderboardConfig["freeze_mode"] as Dictionary<string, object>;
        m_LBInfo["freeze"] = (string)modeConfig["alltime"];
        m_LBInfo["freeze_recent"] = (string)modeConfig["recent"];

        m_MoreGames = (string)rubbish["store_url"];
    }

    public string GetMoreGamesURL()
    {
        return m_MoreGames;
    }


//     private void GetJSONConfigs()
//     {
//         var webAddr = "http://5play.mobi:8888/adsservice-0.0.1-SNAPSHOT/getads/config";
//         var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
//         httpWebRequest.ContentType = "application/json; charset=utf-8";
//         httpWebRequest.Method = "POST";
//         // set timeout = 3 sec
// 
//         using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
//         {
// #if UNITY_ANDROID
//             string json = "{\"os\":\""
//                           + "android"
//                           + "\",\"did\":\""
//                           + "-1"
//                           + "\",\"appid\":\"white_tiles_2\"}";
// #endif
// #if UNITY_IOS
//              string json = "{\"os\":\""
//                            + "ios"
//                            + "\",\"did\":\""
//                            + "-1"
//                            + "\",\"appid\":\"white_tiles_2\"}";
// #endif
// #if UNITY_WP8
//              string json = "{\"os\":\""
//                            + "ios"
//                            + "\",\"did\":\""
//                            + "-1"
//                            + "\",\"appid\":\"white_tiles_2\"}";
// #endif
//             streamWriter.Write(json);
//             streamWriter.Flush();
//         }
// 
//         try
//         {
//             var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
//             using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
//             {
//                 string result = streamReader.ReadToEnd();
//                 ProcessJSONString(result);
//             }
//         }
//         catch (WebException ex)
//         {
//             Debug.Log("network error, use default values...");
//         }
//     }
    

    public void Initialize()
    {
//         Thread newThread = new Thread(new ThreadStart(GetJSONConfigs));
//         newThread.Start();
    }
}

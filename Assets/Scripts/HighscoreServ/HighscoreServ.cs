﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
#if !UNITY_WP8
using System.Xml;
#endif
#if UNITY_WP8
using System.Linq;
using System.Xml.Linq;
// using System.Xml.XPath;
#endif
using System.Globalization;

public class HighscoreServ {

	private static HighscoreServ instance;
	
	private HighscoreServ() {}
	
	public static HighscoreServ Instance
	{
		get 
		{
			if (instance == null)
			{
				instance = new HighscoreServ();
			}
			return instance;
		}
	}

    private string m_FilePath;
#if !UNITY_WP8
    private XmlDocument m_Document;
#endif
#if UNITY_WP8
    private XDocument m_Document;
#endif

    private string m_DefaultContent;
    private string m_Version;

    public void Initialize(string fileName, string defaultContent, string version)
    {
        m_DefaultContent = defaultContent;
        m_Version = version;
        m_FilePath = Path.Combine(Application.persistentDataPath, fileName);
#if !UNITY_WP8
        m_Document = new XmlDocument();
#endif
#if UNITY_WP8
        m_Document = new XDocument();
#endif
        LoadFile();
        Validate();
        // UpdateFile();
    }

    private void ReadContent()
    {
        Debug.Log("[HighscoreServ] Attemping to read file content...");
        FileStream fileStream = null;
#if NETFX_CORE
        fileStream = File.Open(m_FilePath);
#else
        fileStream = File.Open(m_FilePath, FileMode.Open);
#endif
#if !UNITY_WP8
        m_Document.Load(fileStream);
#endif
#if UNITY_WP8
        m_Document = XDocument.Load(fileStream);
#endif
        fileStream.Close();
        fileStream.Dispose();
        Debug.Log("[HighscoreServ] Done reading.");
    }

    private void CreateNewlyFile()
    {
        FileStream fileStream = null;
        Debug.Log("[HighscoreServ] Attemping to create newly highscore file...");
#if NETFX_CORE
        fileStream = File.Open(m_FilePath);
#else
        fileStream = File.Open(m_FilePath, FileMode.Create);
#endif
        StreamWriter sw = new StreamWriter(fileStream, Encoding.UTF8);
        sw.Write(m_DefaultContent);
        sw.Close();
        fileStream.Close();
        fileStream.Dispose();
        Debug.Log("[HighscoreServ] Done creating new highscore file.");
    }

    private void LoadFile()
    {
        Debug.Log("[HighscoreServ] Attemping to load file...");
        try
        {
            ReadContent();
        }
        catch
        {
            Debug.LogWarning("[HighscoreServ] File not found.");
            CreateNewlyFile();
            ReadContent();
        }
    }

    private void Validate()
    {
        Debug.Log("[HighscoreServ] Validating...");
        if (!ValidateVersion())
        {
            Debug.Log("[HighscoreServ] Version is out of date.");
            CreateNewlyFile();
            ReadContent();
        }
        else
        {
            Debug.Log("[HighscoreServ] Up to date.");
        }
        ValidateRecentScores();
    }

    private bool ValidateVersion()
    {
#if !UNITY_WP8
        string fileVersion = m_Document.SelectSingleNode("/root/version").InnerText;
#endif
#if UNITY_WP8
        // string fileVersion = m_Document.Element("root").Element("version").Value;
        // string fileVersion = m_Document.XPathSelectElement("./root/version").Value;
        string fileVersion = m_Document.Descendants("root").Descendants("version").First().Value;
#endif
        Debug.Log("[HighscoreServ] Version: " + fileVersion);
        return (string.Compare(fileVersion, m_Version) == 0);
    }

    private bool ValidateRecentScores()
    {
        Debug.Log("[HighscoreServ] Validating recent score records...");
        bool modified = false;
#if !UNITY_WP8
        XmlNodeList recentScores = m_Document.SelectNodes("/root/recent_highscores/recent_highscore");
        foreach (XmlNode node in recentScores)
        {
            Debug.Log("[HighscoreServ] Checking record named: " + node.Attributes["name"].Value + "...");
            XmlNode scoreNode = node.SelectSingleNode(".//score");
            XmlNode expireDateNode = node.SelectSingleNode(".//expire_date");
            XmlNode postedNode = node.SelectSingleNode(".//posted");
            int score = int.Parse(scoreNode.InnerText);
            DateTime expireDate;
            expireDate = DateTime.ParseExact(expireDateNode.InnerText, 
                "dd/MM/yyyy HH:mm", 
                null);
            // bool posted = bool.Parse(postedNode.InnerText);
            // if (posted)
            // {
            // TimeSpan dayPassed = DateTime.Now - expireDate;
            // if (dayPassed.TotalDays >= 1.0f)
                {
                    Debug.Log("[HighscoreServ] Score is expired. Renewing...");
                    scoreNode.InnerText = "0";
                    postedNode.InnerText = "false";
                    node.Attributes["isnew"].Value = "true";
                    modified = true;
                    Debug.Log("[HighscoreServ] Done.");
                }
            // }
        }
#endif
#if UNITY_WP8
        // var recentScores = m_Document.XPathSelectElements("./root/recent_highscores/recent_highscore");
        var recentScores = m_Document.Descendants("root").Descendants("recent_highscores").Descendants("recent_highscore");
        foreach (XElement node in recentScores)
        {
            Debug.Log("[HighscoreServ] Checking record named: " + node.Attribute("name").Value + "...");
            // XElement scoreNode = node.XPathSelectElement(".//score");
            XElement scoreNode = node.Descendants("score").First();
            // XElement expireDateNode = node.XPathSelectElement(".//expire_date");
            XElement expireDateNode = node.Descendants("expire_date").First();
            // XElement postedNode = node.XPathSelectElement(".//posted");
            XElement postedNode = node.Descendants("posted").First();

            int score = int.Parse(scoreNode.Value);
            DateTime expireDate;
            expireDate = DateTime.ParseExact(expireDateNode.Value,
                "dd/MM/yyyy HH:mm",
                null);
            // bool posted = bool.Parse(postedNode.InnerText);
            // if (posted)
            // {
            // TimeSpan dayPassed = DateTime.Now - expireDate;
            // if (dayPassed.TotalDays >= 1.0f)
            {
                Debug.Log("[HighscoreServ] Score is expired. Renewing...");
                scoreNode.Value = "0";
                postedNode.Value = "false";
                node.Attribute("isnew").Value = "true";
                modified = true;
                Debug.Log("[HighscoreServ] Done.");
            }
            // }
        }
#endif
        if (modified)
            UpdateFile();
        return true;
    }

    private void UpdateFile()
    {
        Debug.Log("[HighscoreServ] Updating file content...");
        FileStream fileStream = null;
#if NETFX_CORE
        fileStream = File.Open(m_FilePath);
#else
        fileStream = File.Open(m_FilePath, FileMode.Create);
#endif
        StreamWriter sw = new StreamWriter(fileStream, Encoding.UTF8);
#if !UNITY_WP8
        sw.Write(m_Document.OuterXml);
#endif
#if UNITY_WP8
        sw.Write(m_Document.ToString());
#endif
        sw.Close();
        fileStream.Close();
        fileStream.Dispose();
        Debug.Log("[HighscoreServ] Update done.");
    }

    public bool PushRecentScore(string name, int score)
    {
        Debug.Log("[HighscoreServ] Pushing new recent score to " + name + " record...");
        bool newHighscore = false;
#if !UNITY_WP8
        XmlNodeList recentScores = m_Document.SelectNodes("/root/recent_highscores/recent_highscore");
        foreach (XmlNode node in recentScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                XmlNode scoreNode = node.SelectSingleNode(".//score");
                XmlNode postedNode = node.SelectSingleNode(".//posted");
                bool isNew = bool.Parse(node.Attributes["isnew"].Value);
                int oldScore = int.Parse(scoreNode.InnerText);
                if (string.Compare(node.Attributes["order"].Value, "descending") == 0)
                {
                    if (oldScore < score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.InnerText = score.ToString();
                        postedNode.InnerText = "false";
                        newHighscore = true;
                        node.Attributes["isnew"].Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
                else
                {
                    if (oldScore > score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.InnerText = score.ToString();
                        postedNode.InnerText = "false";
                        newHighscore = true;
                        node.Attributes["isnew"].Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
            }
        }
#endif
#if UNITY_WP8
        // var recentScores = m_Document.XPathSelectElements("./root/recent_highscores/recent_highscore");
        var recentScores = m_Document.Descendants("root").Descendants("recent_highscores").Descendants("recent_highscore");
        foreach (XElement node in recentScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                XElement scoreNode = node.Descendants("score").First();
                XElement postedNode = node.Descendants("posted").First();
                bool isNew = bool.Parse(node.Attribute("isnew").Value);
                int oldScore = int.Parse(scoreNode.Value);
                if (string.Compare(node.Attribute("order").Value, "descending") == 0)
                {
                    if (oldScore < score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.Value = score.ToString();
                        postedNode.Value = "false";
                        newHighscore = true;
                        node.Attribute("isnew").Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
                else
                {
                    if (oldScore > score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.Value = score.ToString();
                        postedNode.Value = "false";
                        newHighscore = true;
                        node.Attribute("isnew").Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
            }
        }
#endif
        if (newHighscore)
            UpdateFile();
        return newHighscore;
    }

    public bool PushAlltimeScore(string name, int score)
    {
        Debug.Log("[HighscoreServ] Pushing new alltime score to " + name + " record...");
        bool newHighscore = false;

#if !UNITY_WP8
        XmlNodeList alltimeScores = m_Document.SelectNodes("/root/alltime_highscores/alltime_highscore");
        foreach (XmlNode node in alltimeScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                XmlNode scoreNode = node.SelectSingleNode(".//score");
                bool isNew = bool.Parse(node.Attributes["isnew"].Value);
                int oldScore = int.Parse(scoreNode.InnerText);
                if (string.Compare(node.Attributes["order"].Value, "descending") == 0)
                {
                    if (oldScore < score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.InnerText = score.ToString();
                        newHighscore = true;
                        node.Attributes["isnew"].Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
                else
                {
                    if (oldScore > score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.InnerText = score.ToString();
                        newHighscore = true;
                        node.Attributes["isnew"].Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
            }
        }
#endif
#if UNITY_WP8
        var alltimeScores = m_Document.Descendants("root").Descendants("alltime_highscores").Descendants("alltime_highscore");
        foreach (XElement node in alltimeScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                XElement scoreNode = node.Descendants("score").First();
                bool isNew = bool.Parse(node.Attribute("isnew").Value);
                int oldScore = int.Parse(scoreNode.Value);
                if (string.Compare(node.Attribute("order").Value, "descending") == 0)
                {
                    if (oldScore < score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.Value = score.ToString();
                        newHighscore = true;
                        node.Attribute("isnew").Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
                else
                {
                    if (oldScore > score || isNew)
                    {
                        Debug.Log("[HighscoreServ] We have a new highscore. Gratz!");
                        scoreNode.Value = score.ToString();
                        newHighscore = true;
                        node.Attribute("isnew").Value = "false";
                        break;
                    }
                    else
                    {
                        Debug.Log("[HighscoreServ] Sorry. Make it better next time.");
                        newHighscore = false;
                        break;
                    }
                }
            }
        }
#endif

        if (newHighscore)
            UpdateFile();
        return newHighscore;
    }

    public int GetRecentScore(string name)
    {
        Debug.Log("[HighscoreServ] Getting recent score of " + name + " record...");
#if !UNITY_WP8
        XmlNodeList recentScores = m_Document.SelectNodes("/root/recent_highscores/recent_highscore");
        foreach (XmlNode node in recentScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                XmlNode scoreNode = node.SelectSingleNode(".//score");
                int score = int.Parse(scoreNode.InnerText);
                return score;
            }
        }
#endif
#if UNITY_WP8
        var recentScores = m_Document.Descendants("root").Descendants("recent_highscores").Descendants("recent_highscore");
        foreach (XElement node in recentScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                XElement scoreNode = node.Descendants("score").First();
                int score = int.Parse(scoreNode.Value);
                return score;
            }
        }
#endif
        Debug.Log("[HighscoreServ] Done.");
        return 0;
    }

    public int GetAlltimeScore(string name)
    {
        Debug.Log("[HighscoreServ] Getting alltime score of " + name + " record...");
#if !UNITY_WP8
        XmlNodeList alltimeScores = m_Document.SelectNodes("/root/alltime_highscores/alltime_highscore");
        foreach (XmlNode node in alltimeScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                XmlNode scoreNode = node.SelectSingleNode(".//score");
                int score = int.Parse(scoreNode.InnerText);
                return score;
            }
        }
#endif
#if UNITY_WP8
        var alltimeScores = m_Document.Descendants("root").Descendants("alltime_highscores").Descendants("alltime_highscore");
        foreach (XElement node in alltimeScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                XElement scoreNode = node.Descendants("score").First();
                int score = int.Parse(scoreNode.Value);
                return score;
            }
        }
#endif
        Debug.Log("[HighscoreServ] Done.");
        return 0;
    }

    public bool IsRecentScorePosted(string name)
    {
#if !UNITY_WP8
        XmlNodeList alltimeScores = m_Document.SelectNodes("/root/recent_highscores/recent_highscore");
        foreach (XmlNode node in alltimeScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                XmlNode postedNode = node.SelectSingleNode(".//posted");
                return bool.Parse(postedNode.InnerText);
            }
        }
#endif
#if UNITY_WP8
        var alltimeScores = m_Document.Descendants("root").Descendants("recent_highscores").Descendants("recent_highscore");
        foreach (XElement node in alltimeScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                XElement postedNode = node.Descendants("posted").First();
                return bool.Parse(postedNode.Value);
            }
        }
#endif
        return true;
    }

    public bool IsNoAlltimeScoreYet(string name)
    {
#if !UNITY_WP8
        XmlNodeList alltimeScores = m_Document.SelectNodes("/root/alltime_highscores/alltime_highscore");
        foreach (XmlNode node in alltimeScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                return (bool.Parse(node.Attributes["isnew"].Value));
            }
        }
#endif
#if UNITY_WP8
        var alltimeScores = m_Document.Descendants("root").Descendants("alltime_highscores").Descendants("alltime_highscore");
        foreach (XElement node in alltimeScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                return (bool.Parse(node.Attribute("isnew").Value));
            }
        }
#endif
        return true;
    }

    public void SetRecentScorePosted(string name)
    {
        Debug.Log("[HighscoreServ] Marking recent score record named " + name + " as posted.");
#if !UNITY_WP8
        XmlNodeList alltimeScores = m_Document.SelectNodes("/root/recent_highscores/recent_highscore");
        foreach (XmlNode node in alltimeScores)
        {
            if (string.Compare(node.Attributes["name"].Value, name) == 0)
            {
                XmlNode postedNode = node.SelectSingleNode(".//posted");
                XmlNode expireDateNode = node.SelectSingleNode(".//expire_date");
                postedNode.InnerText = "true";
                expireDateNode.InnerText = string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
                UpdateFile();
                return;
            }
        }
#endif
#if UNITY_WP8
        var alltimeScores = m_Document.Descendants("root").Descendants("recent_highscores").Descendants("recent_highscore");
        foreach (XElement node in alltimeScores)
        {
            if (string.Compare(node.Attribute("name").Value, name) == 0)
            {
                XElement postedNode = node.Descendants("posted").First();
                XElement expireDateNode = node.Descendants("expire_date").First();
                postedNode.Value = "true";
                expireDateNode.Value = string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
                UpdateFile();
                return;
            }
        }
#endif
        UpdateFile();
        Debug.Log("[HighscoreServ] Done.");
    }

    // score history
    public void PushHistory(string scoreCategory, float score)
    {
        Debug.Log("[HighscoreServ] Push new score into the history named " + scoreCategory);
#if !UNITY_WP8
        XmlNodeList scoreHistories = m_Document.SelectNodes("/root/score_histories/score_history");
        foreach (XmlNode node in scoreHistories)
        {
            if (string.Compare(node.Attributes["name"].Value, scoreCategory) == 0)
            {
                if (node.ChildNodes.Count == 20)
                {
                    node.RemoveChild(node.FirstChild);
                }

                XmlNode newScoreNode = m_Document.CreateElement("score");
                
                XmlAttribute attrib = m_Document.CreateAttribute("id");
                attrib.Value = (int.Parse(node.LastChild.Attributes["id"].Value) + 1).ToString();
                newScoreNode.Attributes.Append(attrib);
                newScoreNode.InnerText = score.ToString();
                node.AppendChild(newScoreNode);

                UpdateFile();
                Debug.Log("[HighscoreServ] Done adding.");
                return;
                
            }
        }
#endif
#if UNITY_WP8
        var scoreHistories = m_Document.Descendants("root").Descendants("score_histories").Descendants("score_history");
        foreach (XElement node in scoreHistories)
        {
            if (string.Compare(node.Attribute("name").Value, scoreCategory) == 0)
            {
                int count = (from x in node.Descendants() select x).Count();
                if (count == 20)
                {
                    // node.RemoveChild(node.FirstChild);
                    node.Descendants().First().Remove();
                }

                XElement newScoreNode = new XElement("score",
                    new XAttribute("id", (int.Parse(node.Descendants().Last().Attribute("id").Value) + 1).ToString()));
                newScoreNode.Value = score.ToString();
                node.Add(newScoreNode);

                UpdateFile();
                Debug.Log("[HighscoreServ] Done adding.");
                return;

            }
        }
#endif
        UpdateFile();
        Debug.Log("[HighscoreServ] Done.");
    }

    public List<float> GetHistory(string scoreCategory)
    {
        List<float> retList = new List<float>(20);
        Debug.Log("[HighscoreServ] Get score history of " + scoreCategory);
#if !UNITY_WP8
        XmlNodeList scoreHistories = m_Document.SelectNodes("/root/score_histories/score_history");
        foreach (XmlNode node in scoreHistories)
        {
            if (string.Compare(node.Attributes["name"].Value, scoreCategory) == 0)
            {
                foreach (XmlNode historyNode in node.ChildNodes)
                {
                    retList.Add(float.Parse(historyNode.InnerText));
                }

            }
        }
#endif
#if UNITY_WP8
        var scoreHistories = m_Document.Descendants("root").Descendants("score_histories").Descendants("score_history");
        foreach (XElement node in scoreHistories)
        {
            if (string.Compare(node.Attribute("name").Value, scoreCategory) == 0)
            {
                foreach (XElement historyNode in node.Descendants())
                {
                    retList.Add(float.Parse(historyNode.Value));
                }

            }
        }
#endif
        Debug.Log("[HighscoreServ] Done.");
        return retList;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;

public class ServerCommunicator : Singleton<ServerCommunicator>
{
    public delegate void RequestDoneDelegate(string returnJson, bool error, string errorName);

    private IEnumerator RequestRoutine(string url, string requestJson, RequestDoneDelegate onComplete)
    {
        var encoding = new System.Text.UTF8Encoding();

        Dictionary<string, string> dict;
        dict = new Dictionary<string, string>();
        dict.Add("Content-Type", "application/json");
        dict.Add("Content-Length", encoding.GetBytes(requestJson).Length.ToString());
        WWW request = new WWW(url, encoding.GetBytes(requestJson), dict);

        yield return request;
        onComplete(request.text, !string.IsNullOrEmpty(request.error), request.error);
    }

    public void DoRequest(string url, string requestJson, RequestDoneDelegate onComplete)
    {
        StartCoroutine(RequestRoutine(url, requestJson, onComplete));
    }

    public void AbortAllRequests()
    {
        StopAllCoroutines();
    }
}
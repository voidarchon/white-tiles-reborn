﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class SpeedGraph : MonoBehaviour {
    [Header("__debug")]
    public bool __isDebug;
    public int __testDataCount;

    [Header("Used prefabs")]
    public GameObject PrefabValueIndicator;

    [Header("Graph configs")]
    public float Width;
    public float Height;
    public bool DisableValueIndicator;
    public UILabel[] ValueTextGOs;

    [Header("For animating graph")]
    public bool IsAnimating;
    public float AnimatingPercent;

    private MeshFilter m_MeshFilter;
    private MeshRenderer m_MeshRenderer;

    private List<float> m_TimeData;
    private List<GameObject> m_Indicator;

    private List<Vector3> m_Vertices;
    private List<Vector2> m_UVs;
    private List<int> m_Indices;
    private Mesh m_Mesh;

    private float m_DataSpacing;
    private float m_DataHeight;
    private float m_MaxTime;
    private float m_MinTime;

    private void SetValueTexts()
    {
        float step = m_DataHeight / 5.0f;
        for (int i = 0; i < ValueTextGOs.Length; i++)
        {
            ValueTextGOs[i].SetText(string.Format("{0:0.00}", m_MinTime + i * step));
        }
    }

    private void CalculatePrimitives()
    {
        m_MaxTime = float.NegativeInfinity;
        m_MinTime = float.PositiveInfinity; 
        for (int i = 0; i < m_TimeData.Count; i++ ) {
            if (m_TimeData[i] > m_MaxTime)
                m_MaxTime = m_TimeData[i];
            if (m_TimeData[i] < m_MinTime)
                m_MinTime = m_TimeData[i];
        }

        m_MinTime = 0.25f * m_MinTime;

        m_DataHeight = (m_MaxTime - m_MinTime) < 0.005f? 1.0f : (m_MaxTime - m_MinTime);
        m_DataSpacing = Width / (m_TimeData.Count - 1);
    }

    public void Initialize(List<float> timeData)
    {
        if (timeData.Count >= 20)
            DisableValueIndicator = true;
        m_TimeData = timeData;
        CalculatePrimitives();
        SetValueTexts();
        GenerateZeroMesh();
    }

    public void PushTimeData(float data)
    {
        m_TimeData.Add(data);
    }

    private void GenerateZeroMesh()
    {
        m_Indicator = new List<GameObject>();
        m_Vertices = new List<Vector3>();
        m_UVs = new List<Vector2>();
        m_Indices = new List<int>();

        for (int i = 0; i < m_TimeData.Count; i++)
        {
            m_Vertices.Add(new Vector3(m_DataSpacing * i, 0.0f, 0.0f));
            m_Vertices.Add(new Vector3(m_DataSpacing * i, 0.0f, 0.0f));
            m_UVs.Add(new Vector2(m_DataSpacing * i, 0.0f));
            m_UVs.Add(new Vector2(m_DataSpacing * i, 0.0f));
            int doubleI = (i - 1) * 2;
            if (i > 0)
            {
                m_Indices.Add(doubleI); m_Indices.Add(1 + doubleI); m_Indices.Add(2 + doubleI);
                m_Indices.Add(2 + doubleI); m_Indices.Add(1 + doubleI); m_Indices.Add(3 + doubleI);
            }
            if (!DisableValueIndicator)
            {
                GameObject obj = Instantiate(PrefabValueIndicator) as GameObject;
                obj.transform.parent = transform;
                obj.transform.localPosition = new Vector3(m_DataSpacing * i, 0.0f, -1.0f);
                obj.SetActive(false);
                m_Indicator.Add(obj);
            }
        }
        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices.ToArray();
        m_Mesh.uv = m_UVs.ToArray();
        m_Mesh.triangles = m_Indices.ToArray();
        GetComponent<MeshFilter>().mesh = m_Mesh;
    }

    private void GenerateMesh()
    {
        m_Indicator = new List<GameObject>();
        m_Vertices = new List<Vector3>();
        m_UVs = new List<Vector2>();
        m_Indices = new List<int>();

        for (int i = 0; i < m_TimeData.Count; i++)
        {
            m_Vertices.Add(new Vector3(0.2f * i, 0.0f, 0.0f));
            m_Vertices.Add(new Vector3(0.2f * i, m_TimeData[i], 0.0f));
            m_UVs.Add(new Vector2(0.2f * i, 0.0f));
            m_UVs.Add(new Vector2(0.2f * i, m_TimeData[i]));
            int doubleI = (i - 1) * 2;
            if (i > 0)
            {
                m_Indices.Add(doubleI); m_Indices.Add(1 + doubleI); m_Indices.Add(2 + doubleI);
                m_Indices.Add(2 + doubleI); m_Indices.Add(1 + doubleI); m_Indices.Add(3 + doubleI);
            }

            GameObject obj = Instantiate(PrefabValueIndicator) as GameObject;
            obj.transform.parent = transform;
            obj.transform.localPosition = new Vector3(0.2f * i, m_TimeData[i], -1.0f);
            m_Indicator.Add(obj);
        }
        m_Mesh = new Mesh();
        m_Mesh.vertices = m_Vertices.ToArray();
        m_Mesh.uv = m_UVs.ToArray();
        m_Mesh.triangles = m_Indices.ToArray();
        m_MeshFilter.mesh = m_Mesh;
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void OnDisable()
    {
        for (int i = 0; i < m_Indicator.Count; i++)
            m_Indicator[i].SetActive(false);
        Object.Destroy(m_Mesh);
    }

    private void __debugRandomData()
    {
        List<float> __debugTestData = new List<float>(__testDataCount);
        for (int i = 0; i < __testDataCount; i++)
            __debugTestData.Add(Random.Range(0.2f, 0.7f));
        Initialize(__debugTestData);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshRenderer = GetComponent<MeshRenderer>();
        if (__isDebug)
            __debugRandomData();
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (IsAnimating)
        {
            m_Vertices.Clear();
            m_UVs.Clear();
            m_Indices.Clear();
            for (int i = 0; i < m_TimeData.Count; i++)
            {
                m_Vertices.Add(new Vector3(m_DataSpacing * i, 0.0f, 0.0f));
                m_Vertices.Add(new Vector3(m_DataSpacing * i,
                    (m_TimeData[i] - m_MinTime) / m_DataHeight * AnimatingPercent * Height, 0.0f));
                m_UVs.Add(new Vector2(m_DataSpacing * i, 0.0f));
                m_UVs.Add(new Vector2(m_DataSpacing * i,
                    (m_TimeData[i] - m_MinTime) / m_DataHeight * AnimatingPercent));
                int doubleI = (i - 1) * 2;
                if (i > 0)
                {
                    m_Indices.Add(doubleI); m_Indices.Add(1 + doubleI); m_Indices.Add(2 + doubleI);
                    m_Indices.Add(2 + doubleI); m_Indices.Add(1 + doubleI); m_Indices.Add(3 + doubleI);
                }
                if (!DisableValueIndicator)
                {
                    m_Indicator[i].transform.localPosition = new Vector3(m_DataSpacing * i,
                        (m_TimeData[i] - m_MinTime) / m_DataHeight * AnimatingPercent * Height, -1.0f);
                    m_Indicator[i].SetActive(true);
                }
            }
            Object.Destroy(m_Mesh);
            m_Mesh = new Mesh();
            m_Mesh.vertices = m_Vertices.ToArray();
            m_Mesh.uv = m_UVs.ToArray();
            m_Mesh.triangles = m_Indices.ToArray();
            m_MeshFilter.mesh = m_Mesh;
        }
	}
}
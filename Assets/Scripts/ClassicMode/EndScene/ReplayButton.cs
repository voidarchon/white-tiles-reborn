﻿using UnityEngine;
using System.Collections;

public class ReplayButton : MonoBehaviour {

    private void OnTapDownDelegate()
    {
        GetComponent<TextMesh>().fontSize = 40;
    }

    private void OnTapUpDelegate()
    {
        GetComponent<TextMesh>().fontSize = 50;
    }

    private void OnTapUpAsButtonDelegate()
    {
        NotificationCenter.GetInstance().InvokeNotification(gameObject, "CESL_Replay", null);
        GetComponent<TextMesh>().fontSize = 50;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class ExitButton : MonoBehaviour {

    private void OnTapDownDelegate()
    {
        GetComponent<TextMesh>().fontSize = 40;
    }

    private void OnTapUpDelegate()
    {
        GetComponent<TextMesh>().fontSize = 50;
    }

    private void OnTapUpAsButtonDelegate()
    {
        NotificationCenter.GetInstance().InvokeNotification(gameObject, "CESL_Exit", null);
        GetComponent<TextMesh>().fontSize = 50;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Animator))]
public class DoneAnimationCallbackServ : MonoBehaviour {
    public string NotificationName;

    private Animator m_Animator;

    private void DoneAnimate()
    {
        // m_Animator.enabled = false;
        NotificationCenter.GetInstance().InvokeNotification(gameObject, NotificationName, null);
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class GraphIndicator : MonoBehaviour {

    private MeshFilter m_MeshFilter;
    private MeshRenderer m_MeshRenderer;

    private Mesh m_Mesh;

    private void GenerateMesh()
    {
        Vector3[] vertices = new Vector3[] {
            new Vector3(-1.0f, -1.0f, 0.0f),
            new Vector3(-1.0f, 1.0f, 0.0f),
            new Vector3(1.0f, 1.0f, 0.0f),
            new Vector3(1.0f, -1.0f, 0.0f)
        };
        Vector2[] uvs = new Vector2[] {
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(1.0f, 0.0f)
        };
        int[] indices = new int[] { 0, 1, 2, 2, 3, 0 };
        m_Mesh = new Mesh();
        m_Mesh.vertices = vertices;
        m_Mesh.uv = uvs;
        m_Mesh.triangles = indices;
        m_MeshFilter.mesh = m_Mesh;
    }

    void OnDestroy()
    {
        Object.Destroy(m_Mesh);
    }

    void Awake()
    {
        m_MeshFilter = GetComponent<MeshFilter>();
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

	// Use this for initialization
	void Start () {
        GenerateMesh();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

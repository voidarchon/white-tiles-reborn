﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClassicEndSceneLogic : Singleton<ClassicEndSceneLogic>
{
    [Header("Many stuffs =))")]
    public SpriteRenderer WinLoseGO;
    public SpeedGraph PerformanceGraphGO;
    public UILabel MeanTimeValueGO;
    public UILabel ScoreValueLabelGO;
    public UILabel BestScoreValueLabelGO;

    [Header("Animation chain properties")]
    public float TimeOut;

    [Header("Result sprites")]
    public Sprite WinSprite;
    public Sprite LoseSprite;

    // data from PlayScene
    public float m_ScoreValue;
    public bool m_IsWon;

    private void OnFacebookShareClicked()
    {
        Debug.Log("facebook share");
        // facebook share go here, no need to login?
        if (m_IsWon)
        {
            FacebookServ.Instance.ShareLinkContent(
                "https://dl.dropboxusercontent.com/u/99391934/empty.html",
                "WhiteTiles Reborn",
                "Won Classic Mode in " + string.Format("{0:0.0000}", m_ScoreValue) + " seconds.", 
                null);
        }
        else
        {
            FacebookServ.Instance.ShareLinkContent(
                "https://dl.dropboxusercontent.com/u/99391934/empty.html",
                "WhiteTiles Reborn",
                "Lost Classic Mode in " + string.Format("{0:0.0000}", m_ScoreValue) + " seconds.", 
                null);
        }
    }

    private void OnReplayClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        Application.LoadLevel("_PlayScene_ClassicMode_");
    }

    private void OnExitClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        Application.LoadLevel("_MainMenu_");
    }

    private void LBButton_Clicked()
    {
        // TODO: board name will be get from the game_mode_major and game_mode_minor prefs variable.
        int rowNum = PlayerPrefs.GetInt("classic_mode_maxrow");
        PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName("classic" + rowNum.ToString() + "_recent"));
        PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName("classic" + rowNum.ToString()));

        PlayerPrefs.SetString("ToLB_GameMode", "classic" + rowNum.ToString());

        PlayerPrefs.SetString("ToLB_ScoreType", "time");
        Application.LoadLevel("_Leaderboard_");
    }

    private void RegisterAllListeners()
    {
    }

    private void RegisterAllEvents()
    {
    }

    private void InitializeData()
    {
        SceneToSceneData data = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        float meanTime = 0.0f;
        object[] dataArr = (object[])data.PayloadData;
        if (data != null)
        {
            m_ScoreValue = (float)dataArr[1];
            ScoreValueLabelGO.GetComponent<TextMesh>().text = string.Format("{0:0.0000}\"", m_ScoreValue);
            List<float> rawTime = (List<float>)dataArr[0];
            m_IsWon = (bool)dataArr[2];

            if (m_IsWon)
                WinLoseGO.sprite = WinSprite;
            else WinLoseGO.sprite = LoseSprite;

            for (int i = rawTime.Count - 1; i >= 1; i--)
            {
                rawTime[i] = rawTime[i] - rawTime[i - 1];
                meanTime += rawTime[i];
            }
            List<float> graphData;
            try
            {
                graphData = rawTime.GetRange(1, rawTime.Count - 1);
                if (graphData.Count == 0)
                {
                    graphData = new List<float>(2);
                    graphData.Add(0.0f); graphData.Add(0.0f);
                }
                else if (graphData.Count == 1)
                {
                    graphData.Add(graphData[0]);
                }
            }
            catch
            {
                graphData = new List<float>(2);
                graphData.Add(0.0f); graphData.Add(0.0f);
            }

            PerformanceGraphGO.Initialize(graphData);
            meanTime = rawTime.Count <= 1 ? 0.0f : meanTime / (rawTime.Count - 1.0f);
        }
        MeanTimeValueGO.GetComponent<TextMesh>().text = string.Format("{0:0.0000}\"", meanTime);
        Object.DestroyImmediate(data.gameObject);
    }

    private void UpdateLocalData()
    {
        string classicType = PlayerPrefs.GetInt("classic_mode_maxrow").ToString();
        if (m_IsWon)
        {
            int normalizeScoreValue = (int)(float.Parse(string.Format("{0:0.0000}", m_ScoreValue)) * 10000.0f);

            HighscoreServ.Instance.PushAlltimeScore("classic" + classicType, normalizeScoreValue);
            HighscoreServ.Instance.PushRecentScore("classic" + classicType + "_recent", normalizeScoreValue);
            HighscoreServ.Instance.PushHistory("classic" + classicType, normalizeScoreValue);
        }
        BestScoreValueLabelGO.SetText(string.Format("{0:0.0000}\"",
            (float)HighscoreServ.Instance.GetAlltimeScore("classic" + classicType) / 10000.0f ));
    }

    private void UpdateServerData()
    {

    }

    void Awake()
    {
        RegisterAllEvents();
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        // show ads
        AdsServ.Instance.ShowPopupWithCountChecking();

        RegisterAllListeners();
        m_IsWon = true;

        InitializeData();
        UpdateLocalData();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnExitClicked();
	}
}

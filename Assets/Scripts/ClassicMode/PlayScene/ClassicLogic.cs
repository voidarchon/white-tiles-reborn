﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClassicLogic : Singleton<ClassicLogic> {
    [Header("GO references")]
    public ClassicRowsStreamController ClassicRowsStreamControllerGO;
    public UILabel TimeScoreLabelGO;
    public UILabel TimeScoreShadowLabelGO;
    public SceneTransition SceneTransitionGO;
    public GameObject StartLabelGO;
    public GameObject FinishOverlayGO;
    public GameObject StartOverlayGO;
    public UILabel FinishLabelGO;
    public Transform UIContainerGO;

    [Header("Sounds")]
    public AudioClip[] Sounds;

    [Header("Gameplay parameters")]
    public bool GameIsPause;
    public bool GameIsOver;
    public int MaxRowNum;

    [Header("Dont touch this, NOOB!")]
    public int StartRowId;
    [Range(0, 5)]
    public int StartIdOffset;
    public int CurrRowId;

    private bool m_FirstTimeStartGame;

    // sound effects
    private int m_LastIdHit;
    private int m_LastSoundPitch;
    private AudioSource m_AudioSource;

    // data collect for summary view
    private List<float> m_DoneRowTimeStamps;
    private float m_StartGameTimeStamp;
    private float m_TimeElapsed;
    private Coroutine m_TimeScoreUpdate;
    private bool m_IsWon;

    private bool m_SoundOn;

    private void UpdateTimeScore()
    {
        if (!GameIsPause && !GameIsOver)
        {
            m_TimeElapsed = Time.time - m_StartGameTimeStamp;
            string newTimeStr = string.Format("{0:0.0000}\"", m_TimeElapsed);
            TimeScoreLabelGO.SetText(newTimeStr);
            TimeScoreShadowLabelGO.SetText(newTimeStr);
        }
    }

    private void OnWin()
    {
        GameIsOver = true;
        m_IsWon = true;
        // TODO: replace this with Invoke()
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_DoneRowTimeStamps, m_TimeElapsed, m_IsWon });
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void OnDoneRowDelegate(GameObject sender, object data)
    {
        RowController doneRow = (RowController)data;
        if (doneRow.RowId == MaxRowNum - 1)
            OnWin();
        CurrRowId++;
        ClassicRowsStreamControllerGO.EnableNextRow(doneRow);
        m_DoneRowTimeStamps.Add(Time.time);
    }

    private void OnRowSuccessTapDelegate(GameObject sender, object data)
    {
        object[] dataArr = (object[])data;
        RowController theRow = (RowController)dataArr[0];
        int idHit = (int)dataArr[1];
        if (StartRowId + StartIdOffset == theRow.RowId)
        {
            GameIsPause = false;
            if (m_FirstTimeStartGame)
            {
                m_StartGameTimeStamp = Time.time;
                m_FirstTimeStartGame = false;
                StartLabelGO.transform.parent = UIContainerGO;
                StartLabelGO.SetActive(false);
            }
        }
        if (m_SoundOn)
            PlaySoundEffect(idHit);
    }

    private void OnRowFailedTapDelegate(GameObject sender, object data)
    {
        RowController failedRow = (RowController)data;
        GameIsPause = true;
        GameIsOver = true;
        m_IsWon = false;
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_DoneRowTimeStamps, m_TimeElapsed, m_IsWon });
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void OnStartRowGoneDelegate(GameObject sender, object data)
    {
        StartOverlayGO.transform.parent = UIContainerGO;
        StartOverlayGO.SetActive(false);
    }

    private void WaitThenEndGame()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        Application.LoadLevel("_EndScene_ClassicMode_");
    }

    private void PlaySoundEffect(int idHit)
    {
        if (m_LastIdHit < 0)
        {
            m_LastSoundPitch = Random.Range(0, 26);
        }
        else
        {
            if (idHit > m_LastIdHit)
            {
                m_LastSoundPitch = Random.Range(m_LastSoundPitch, 26);
            }
            else if (idHit < m_LastIdHit)
            {
                m_LastSoundPitch = Random.Range(0, m_LastSoundPitch);
            }
        }
        m_LastIdHit = idHit;
        m_AudioSource.PlayOneShot(Sounds[m_LastSoundPitch]);
    }

    private void RegisterAllListeners()
    {
        NotificationCenter.GetInstance().RegisterListener("RowDonePlay", OnDoneRowDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowSuccessTap", OnRowSuccessTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowFailedTap", OnRowFailedTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("Classic_StartRowGone", OnStartRowGoneDelegate);
    }

    void Awake()
    {
        Randomkun.GetInstance().ForgetEverything();
        m_AudioSource = GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        m_FirstTimeStartGame = true;
        CurrRowId = StartRowId + StartIdOffset;
        m_LastIdHit = -1;
        m_IsWon = false;
        m_DoneRowTimeStamps = new List<float>(200);
        if (PlayerPrefs.GetInt("game_settings_sound") == 1)
            m_SoundOn = true;
        else m_SoundOn = false;

        MaxRowNum = PlayerPrefs.GetInt("classic_mode_maxrow");
        StartOverlayGO.GetComponent<StartOverlay>().Initialize(4);

        RegisterAllListeners();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateTimeScore();
	}
}

﻿using UnityEngine;
using System.Collections;

public class FinishOverlay : MonoBehaviour {

    public GameObject TopMaskGO;
    public GameObject BotMaskGO;

    public void Initialize()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;

        float tileHeight = (floatHeight - (4 + 1) * pixelSize) / 4;

        TopMaskGO.transform.localPosition += new Vector3(0.0f, 1.0f + tileHeight / 2.0f, 0.0f);
        BotMaskGO.transform.localPosition += new Vector3(0.0f, -1.0f - tileHeight / 2.0f, 0.0f);
    }

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

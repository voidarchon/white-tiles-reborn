﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReversableRowsStreamController : MonoBehaviour {
    [Header("Objects pool reference")]
    public ObjectsPool RowsPoolGO;

    [Header("Gameplay parameters")]
    public float ScrollSpeed;
    public float ScrollUpSpeed;
    public float ReversingInterval;
    public int RowCount;

    private LinkedList<RowController> m_Rows;
    private float m_TileHeight;
    private float m_PixelSize;
    private bool m_DoneInit;
    private Vector3 m_ScrollVector;
    private Vector3 m_RowSpawnPoint;
    private int m_NextId;

    private void InitializeFirstLook()
    {
        m_NextId = ReverseLogic.Instance.StartRowId;
        m_Rows = new LinkedList<RowController>();
        GameObject obj = RowsPoolGO.PeekNextObject();
        float tileHeight = obj.GetComponent<Row>().TileHeight;
        float pixelSize = obj.GetComponent<Row>().PixelSize;
        float screenTopY = Camera.main.orthographicSize;
        m_TileHeight = tileHeight;
        m_PixelSize = pixelSize;
        m_RowSpawnPoint = new Vector3(0.0f, screenTopY - (pixelSize + tileHeight / 2.0f) + (pixelSize + tileHeight), 0.0f);

        for (int i = 0; i < RowCount; i++)
        {
            GameObject row;
            row = RowsPoolGO.GetObject();

            Vector3 pos = new Vector3();
            pos.x = 0.0f;
            pos.z = 0.0f;
            pos.y = -screenTopY + (pixelSize + tileHeight / 2.0f) + (i) * (pixelSize + tileHeight);
            row.transform.parent = transform;
            row.transform.localPosition = pos;
            row.SetActive(true);
            row.GetComponent<RowController>().RowId = m_NextId;
            if (i == ReverseLogic.Instance.StartIdOffset)
            {
                row.GetComponent<RowController>().Enabled = true;
                // start label
//                 int blackTileNum = row.GetComponent<RowController>().BlackTileNumber;
//                 for (int j = 0; j < blackTileNum; j++)
//                 {
//                     ArcadeLogic.Instance.StartLabelGO[j].transform.parent =
//                         row.GetComponent<Row>().GetBlackTileById(j).transform;
//                     ArcadeLogic.Instance.StartLabelGO[j].transform.localPosition =
//                         new Vector3(0.0f, 0.0f, -1.0f);
//                     ArcadeLogic.Instance.StartLabelGO[j].gameObject.SetActive(true);
//                 }
            }
            if (i < ReverseLogic.Instance.StartIdOffset)
            {
                row.GetComponent<Row>().SetBlackTileColor(new Color(1.0f, 1.0f, 0.0f, 1.0f));
                row.GetComponent<Row>().SetWhiteTileColor(new Color(1.0f, 1.0f, 0.0f, 1.0f));
            }
            else
            {
                row.GetComponent<Row>().SetBlackTileColor(Color.black);
            }
            m_Rows.AddFirst(row.GetComponent<RowController>());
            m_NextId++;
        }
        m_DoneInit = true;
    }

    public void EnableNextRow(RowController currRow)
    {
        currRow.Enabled = false;
        LinkedListNode<RowController> currNode = m_Rows.Find(currRow);
        currNode.Previous.Value.Enabled = true;
    }

    private bool IsLastRowOutOfScreen()
    {
        return (m_Rows.Last.Value.transform.localPosition.y <= -Camera.main.orthographicSize - m_TileHeight / 2.0f);
    }

    private void ScrollRowDown()
    {
        for (LinkedListNode<RowController> node = m_Rows.First;
             node != null; node = node.Next)
        {
            node.Value.transform.localPosition += m_ScrollVector;
        }
    }

    private IEnumerator ScrollUpRoutine()
    {
        NotificationCenter.GetInstance().InvokeNotification(gameObject, "Reverse_RowMiss", m_Rows.Last.Value);
        float dest = -Camera.main.orthographicSize + m_TileHeight / 2.0f;
        float currVal = m_Rows.Last.Value.transform.localPosition.y;
        while (currVal < dest)
        {
            Vector3 scrollUpVector = new Vector3(0.0f, ScrollUpSpeed * Time.deltaTime, 0.0f);
            if (currVal + ScrollUpSpeed * Time.deltaTime <= dest)
            {
                for (LinkedListNode<RowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.localPosition += scrollUpVector;
                }
                currVal = m_Rows.Last.Value.transform.localPosition.y;
            }
            else
            {
                Vector3 lastScrollUpVector = new Vector3(0.0f,
                                                         dest - m_Rows.Last.Value.transform.localPosition.y,
                                                         0.0f);
                for (LinkedListNode<RowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.localPosition += lastScrollUpVector;
                }
                currVal = dest;
            }
            yield return null;
        }
        yield return null;
    }

    private void RenewRows()
    {
        if (IsLastRowOutOfScreen())
        {
            RowController oldRow = m_Rows.Last.Value;
            if (ReverseLogic.Instance.CurrRowId == oldRow.RowId)
            {
                ReverseLogic.Instance.GameIsPause = true;
                StartCoroutine(ScrollUpRoutine());
                return;
            }

            oldRow.Enabled = false;
            oldRow.ResetWhiteTileColor();
            oldRow.ResetRowRandomBlack();
            oldRow.GetComponent<PooledObject>().ThrowBackToPool();
            m_Rows.RemoveLast();

            int r = Random.Range(0, 100);
            GameObject newRow;
            newRow = RowsPoolGO.GetObject();

            newRow.transform.parent = transform;
            newRow.transform.localPosition = m_Rows.First.Value.transform.localPosition + new Vector3(0.0f, m_PixelSize + m_TileHeight, 0.0f);
            newRow.GetComponent<Row>().SetBlackTileColor(Color.black);
            newRow.SetActive(true);
            newRow.GetComponent<RowController>().RowId = m_NextId;
            m_Rows.AddFirst(newRow.GetComponent<RowController>());
            m_NextId++;
        }
    }

    public void SetBlackTilesColor(Color color)
    {
        for (LinkedListNode<RowController> node = m_Rows.First;
            node != null; node = node.Next)
        {
            node.Value.GetComponent<Row>().SetBlackTileColor(color);
        }
    }

    private void UpdateRows()
    {
        if (m_DoneInit && !ReverseLogic.Instance.GameIsPause)
        {
            m_ScrollVector.y = -ScrollSpeed * Time.deltaTime;
            ScrollRowDown();
            RenewRows();
        }
    }

    /// <summary>
    /// Initialize the gameboard, called from ReverseLogic
    /// </summary>
    public void DoInit()
    {
        m_DoneInit = false;
        m_ScrollVector = new Vector3(0.0f, -ScrollSpeed * Time.deltaTime, 0.0f);
        ScrollSpeed = ReverseLogic.Instance.ScrollSpeed;

        RowsPoolGO.OnDoneInitEventListener += OnObjectsPoolDoneInit;
        RowsPoolGO.InitializePool();
    }

    private void OnObjectsPoolDoneInit()
    {
        InitializeFirstLook();
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("Reverse_RowMiss");
    }

    void Awake()
    {
        RegisterAllNotifications();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	        UpdateRows();
        if (ReverseLogic.Instance.GameIsOver)
            return;
#if !UNITY_EDITOR
		for (int i = 0; i < Input.touchCount; i++) {
			if (i > 1) return;
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.GetTouch(i).position);
				for (LinkedListNode<RowController> node = m_Rows.First;
			     node != null; node = node.Next) {
					if (node.Value.Enabled)
						node.Value.SendMessage ("OnTap", touchPos);
				}
			}
		}
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (LinkedListNode<RowController> node = m_Rows.First;
                 node != null; node = node.Next)
            {
                if (node.Value.Enabled)
                    node.Value.SendMessage("OnTap", touchPos);
            }
        }
#endif
	}
}

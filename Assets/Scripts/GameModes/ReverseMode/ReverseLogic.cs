﻿using UnityEngine;
using System.Collections;

public class ReverseLogic : Singleton<ReverseLogic> {
    [Header("GO References")]
    public ReversableRowsStreamController ReversableRowsStreamControllerGO;

    [Header("Gameplay's parameters")]
    public bool GameIsPause;
    public bool GameIsOver;
    public float ScrollSpeed;

    [Header("Dont touch this, NOOB!")]
    public int StartRowId;
    [Range(0, 5)]
    public int StartIdOffset;
    public int CurrRowId;

    private void Initialize()
    {
        GameIsOver = false;
        CurrRowId = StartRowId + StartIdOffset;
    }

	// Use this for initialization
	void Start () {
        Initialize();
        ReversableRowsStreamControllerGO.DoInit();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

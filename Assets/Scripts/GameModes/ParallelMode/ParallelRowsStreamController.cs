﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParallelRowsStreamController : MonoBehaviour {
    [Header("Objects pool reference")]
    public ObjectsPool ParallelRowsPoolGO;

    [Header("Gameplay parameter")]
    public float ScrollSpeed;
    public float ScrollUpSpeed;
    public enum StreamPosition { Stream_Top, Stream_Bottom };
    public StreamPosition Position;
    public int RowCount;
    public int Id;

    private LinkedList<ParallelRowController> m_Rows;
    private float m_TileHeight;
    private float m_PixelSize;
    private Vector3 m_ScrollVector;
    private Vector3 m_RowSpawnPoint;
    private int m_NextId;
    private bool m_DoneInit;
    private float m_TapRegionSizeY;

    private void InitializeFirstLook()
    {
        m_NextId = ParallelLogic.Instance.StartRowId;
        m_Rows = new LinkedList<ParallelRowController>();
        GameObject obj = ParallelRowsPoolGO.PeekNextObject();
        float tileHeight = obj.GetComponent<ParallelRow>().TileHeight;
        float tileWidth = obj.GetComponent<ParallelRow>().TileWidth;
        float pixelSize = obj.GetComponent<ParallelRow>().PixelSize;
        float screenTopY = Camera.main.orthographicSize * Camera.main.aspect;
        m_TileHeight = tileHeight;
        m_PixelSize = pixelSize;

        float sign = 1.0f;
        if (Position == StreamPosition.Stream_Bottom)
            sign = 1.0f;
        else sign = -1.0f;

        transform.position = new Vector3(0.0f,
            sign * (- Camera.main.orthographicSize + (tileWidth * 4 + pixelSize * 5) / 2.0f),
            0.0f);
        m_TapRegionSizeY = 2.5f;

        m_RowSpawnPoint = new Vector3(0.0f, screenTopY - (pixelSize + tileHeight / 2.0f) + (pixelSize + tileHeight), 0.0f);

        for (int i = 0; i < RowCount; i++)
        {
            int r = Random.Range(0, 100);
            GameObject row;
            row = ParallelRowsPoolGO.GetObject();

            Vector3 pos = new Vector3();
            pos.x = 0.0f;
            pos.z = 0.0f;
            pos.y = -screenTopY + (pixelSize + tileHeight / 2.0f) + (i) * (pixelSize + tileHeight);
            row.transform.parent = transform;
            row.transform.localPosition = pos;
            row.SetActive(true);
            row.GetComponent<ParallelRowController>().RowId = m_NextId;
            if (i == ParallelLogic.Instance.StartIdOffset)
            {
                row.GetComponent<ParallelRowController>().Enabled = true;
                // start label
                ParallelLogic.Instance.StartLabelGO[Id].transform.parent =
                    row.GetComponent<ParallelRow>().GetBlackTile().transform;
                ParallelLogic.Instance.StartLabelGO[Id].transform.localPosition =
                    new Vector3(0.0f, 0.0f, -1.0f);
                ParallelLogic.Instance.StartLabelGO[Id].gameObject.SetActive(true);
            }
            if (i < ParallelLogic.Instance.StartIdOffset)
            {
                row.GetComponent<ParallelRow>().SetBlackTileColor(new Color(0.216f, 0.4f, 0.635f, 1.0f));
                row.GetComponent<ParallelRow>().SetWhiteTileColor(new Color(0.216f, 0.4f, 0.635f, 1.0f));
            }
            else
            {
                row.GetComponent<ParallelRow>().SetBlackTileColor(Color.black);
            }
            m_Rows.AddFirst(row.GetComponent<ParallelRowController>());
            m_NextId++;
        }
        m_DoneInit = true;
    }

    public void EnableNextRow(ParallelRowController currRow)
    {
        currRow.Enabled = false;
        LinkedListNode<ParallelRowController> currNode = m_Rows.Find(currRow);
        currNode.Previous.Value.Enabled = true;
    }

    // TODO: eliminate this calculation, performance will gain
    private bool IsLastRowOutOfScreen()
    {
        return (m_Rows.Last.Value.transform.localPosition.y <= -Camera.main.orthographicSize * Camera.main.aspect - m_TileHeight / 2.0f);
    }

    private void ScrollRowDown()
    {
        for (LinkedListNode<ParallelRowController> node = m_Rows.First;
             node != null; node = node.Next)
        {
            node.Value.transform.localPosition += m_ScrollVector;
        }
    }

    private IEnumerator ScrollUpRoutine()
    {
        NotificationCenter.GetInstance().InvokeNotification(gameObject, "Parallel_RowMiss", m_Rows.Last.Value);
        float dest = -Camera.main.orthographicSize * Camera.main.aspect + m_TileHeight / 2.0f;
        float currVal = m_Rows.Last.Value.transform.localPosition.y;
        while (currVal < dest)
        {
            Vector3 scrollUpVector = new Vector3(0.0f, ScrollUpSpeed * Time.deltaTime, 0.0f);
            if (currVal + ScrollUpSpeed * Time.deltaTime <= dest)
            {
                for (LinkedListNode<ParallelRowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.localPosition += scrollUpVector;
                }
                currVal = m_Rows.Last.Value.transform.localPosition.y;
            }
            else
            {
                Vector3 lastScrollUpVector = new Vector3(0.0f,
                                                         dest - m_Rows.Last.Value.transform.localPosition.y,
                                                         0.0f);
                for (LinkedListNode<ParallelRowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.localPosition += lastScrollUpVector;
                }
                currVal = dest;
            }
            yield return null;
        }
        yield return null;
    }

    private void RenewRows()
    {
        if (IsLastRowOutOfScreen())
        {
            ParallelRowController oldRow = m_Rows.Last.Value;

            if (!ParallelLogic.Instance.__debug)
            {
                if (ParallelLogic.Instance.CurrRowId[Id] == oldRow.RowId)
                {
                    ParallelLogic.Instance.GameIsPause = true;
                    StartCoroutine(ScrollUpRoutine());
                    return;
                }
            }

            oldRow.Enabled = false;
            oldRow.ResetAllColor();
            oldRow.ResetRowRandomBlack();
            oldRow.GetComponent<PooledObject>().ThrowBackToPool();
            m_Rows.RemoveLast();

            int r = Random.Range(0, 100);
            GameObject newRow;
            newRow = ParallelRowsPoolGO.GetObject();
            newRow.transform.parent = transform;
            newRow.transform.localPosition = m_Rows.First.Value.transform.localPosition + new Vector3(0.0f, m_PixelSize + m_TileHeight, 0.0f);
            newRow.GetComponent<ParallelRow>().SetBlackTileColor(Color.black);
            newRow.SetActive(true);
            newRow.GetComponent<ParallelRowController>().RowId = m_NextId;
            m_Rows.AddFirst(newRow.GetComponent<ParallelRowController>());
            m_NextId++;
        }
    }

    public void SetBlackTilesColor(Color color)
    {
        for (LinkedListNode<ParallelRowController> node = m_Rows.First;
            node != null; node = node.Next)
        {
            node.Value.GetComponent<ParallelRow>().SetBlackTileColor(color);
        }
    }

    private void UpdateRows()
    {
        if (m_DoneInit && !ParallelLogic.Instance.GameIsPause)
        {
            m_ScrollVector.y = -ScrollSpeed * Time.deltaTime;
            ScrollRowDown();
            RenewRows();
        }
    }

    private void OnObjectsPoolDoneInit()
    {
        InitializeFirstLook();
    }

    /// <summary>
    /// Initialize the gameboard, called from ParallelLogic
    /// </summary>
    public void DoInit()
    {
        m_DoneInit = false;
        m_ScrollVector = new Vector3(0.0f, -ScrollSpeed * Time.deltaTime, 0.0f);
        ScrollSpeed = ParallelLogic.Instance.ScrollSpeed;

        ParallelRowsPoolGO.OnDoneInitEventListener += OnObjectsPoolDoneInit;
        ParallelRowsPoolGO.InitializePool();
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("Parallel_RowMiss");
    }

    void Awake()
    {
        RegisterAllNotifications();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    // TODO: 
    //      attention to this, we must support multitouch (2 points) 
    //      on 2 diffirent regions.
	void Update () {
        UpdateRows();
        if (ParallelLogic.Instance.GameIsOver)
            return;
        // TODO: check this, multi-touch for dual mode
#if !UNITY_EDITOR
		for (int i = 0; i < Input.touchCount; i++) {
			// if (i > 1) return;
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.GetTouch(i).position);
                if (Mathf.Abs(touchPos.y - transform.position.y) > m_TapRegionSizeY)
                    return;
				for (LinkedListNode<ParallelRowController> node = m_Rows.First;
			     node != null; node = node.Next) {
					if (node.Value.Enabled)
						node.Value.SendMessage ("OnTap", touchPos);
				}
			}
		}
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Mathf.Abs(touchPos.y - transform.position.y) > m_TapRegionSizeY)
                return;
            for (LinkedListNode<ParallelRowController> node = m_Rows.First;
                 node != null; node = node.Next)
            {
                if (node.Value.Enabled)
                    node.Value.SendMessage("OnTap", touchPos);
            }
        }
#endif
	}
}

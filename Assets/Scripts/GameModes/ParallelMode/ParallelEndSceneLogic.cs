﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParallelEndSceneLogic : MonoBehaviour {

    [Header("Stuffs")]
    public GameObject P1ScoreValueGO;
    public GameObject P2ScoreValueGO;
    public GameObject TotalScoreValueGO;
    public GameObject BestScoreValueGO;
    public GameObject AnimationGroupGO;
    public SpeedGraph PerformanceGraphGO;
    public GameObject InfoLabelGO;

    private int m_P1ScoreValue;
    private int m_P2ScoreValue;
    private int m_TotalScoreValue;

    private string m_ParallelFullName;

    private void OnFacebookShareClicked()
    {
        string shareStr;
        shareStr = "Completed Dual Mode with " + m_TotalScoreValue + " points.";

        FacebookServ.Instance.ShareLinkContent(
            "https://dl.dropboxusercontent.com/u/99391934/empty.html",
            "WhiteTiles Reborn",
            shareStr,
            null);
    }

    private void OnReplayClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        Application.LoadLevel("_PlayScene_Parallel_");
    }

    private void OnExitClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        Application.LoadLevel("_MainMenu_");
    }

    private void OnLBOpenClicked()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        PlayerPrefs.SetString("ToLB_GameMode", "dual");
        PlayerPrefs.SetString("ToLB_ScoreType", "point");
        PlayerPrefs.SetString("ToLB_RecentBoardName", LBSettingsManager.Instance.GetLBName("dual_recent"));
        PlayerPrefs.SetString("ToLB_AlltimeBoardName", LBSettingsManager.Instance.GetLBName("dual"));
        Application.LoadLevel("_LeaderBoard_");
    }

    private void InitializeData()
    {
        SceneToSceneData data = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        object[] dataArr = (object[])data.PayloadData;
        if (data != null)
        {
            m_P1ScoreValue = (int)dataArr[0];
            m_P2ScoreValue = (int)dataArr[1];
        }
        m_TotalScoreValue = m_P1ScoreValue + m_P2ScoreValue;
        P1ScoreValueGO.GetComponent<TextMesh>().text = m_P1ScoreValue.ToString();
        P2ScoreValueGO.GetComponent<TextMesh>().text = m_P2ScoreValue.ToString();
        TotalScoreValueGO.GetComponent<TextMesh>().text = m_TotalScoreValue.ToString();
        Object.DestroyImmediate(data.gameObject);
    }

    private void UpdateLocalData()
    {
        string allTimeKeyStr = "dual";
        string recentKeyStr = "dual_recent";

        HighscoreServ.Instance.PushAlltimeScore(allTimeKeyStr, m_TotalScoreValue);
        HighscoreServ.Instance.PushRecentScore(recentKeyStr, m_TotalScoreValue);
        HighscoreServ.Instance.PushHistory(allTimeKeyStr, m_TotalScoreValue);

        List<float> arcadeHistory = HighscoreServ.Instance.GetHistory(allTimeKeyStr);
        PerformanceGraphGO.Initialize(arcadeHistory);
        List<float> otherButSameHistory = new List<float>(arcadeHistory);// HighscoreServ.Instance.GetHistory(allTimeKeyStr);
        otherButSameHistory.Sort();
        for (int i = 0; i < otherButSameHistory.Count; i++)
        {
            if ((int)otherButSameHistory[i] == m_TotalScoreValue)
            {
                InfoLabelGO.GetComponent<TextMesh>().text =
                    string.Format("This score ranks {0} \n in {1} recent scores.",
                    otherButSameHistory.Count - i, otherButSameHistory.Count);
                break;
            }
        }
        BestScoreValueGO.GetComponent<TextMesh>().text = HighscoreServ.Instance.GetAlltimeScore(allTimeKeyStr).ToString();
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        AdsServ.Instance.ShowPopupWithCountChecking();
        InitializeData();
        UpdateLocalData();
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
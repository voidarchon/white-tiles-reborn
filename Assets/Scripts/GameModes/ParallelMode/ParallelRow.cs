﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParallelRow : MonoBehaviour {

    [Header("Rows configuration")]
    public int RowPerScreenWidth = 4;
    public int TilePerRow = 4;

    [Header("Row's properties")]
    public int BlackTilePosition;

    [Header("Tile prefabs")]
    public GameObject PrefabWhiteTile;
    public GameObject PrefabBlackTile;
    public GameObject PrefabSuccessTile;
    public GameObject PrefabFailedTile;

    private List<Tile> m_WhiteTiles;
    private Tile m_BlackTile;
    private GameObject m_SuccessTile;
    private GameObject m_FailedTile;

    // Tile's size
    private float m_TileWidth;
    public float TileWidth
    {
        get
        {
            return m_TileWidth;
        }
    }
    private float m_TileHeight;
    public float TileHeight
    {
        get
        {
            return m_TileHeight;
        }
    }
    private float m_PixelSize;
    public float PixelSize
    {
        get
        {
            return m_PixelSize;
        }
    }
    private float m_LocalMostLeft;
    public float LocalMostLeft
    {
        get
        {
            return m_LocalMostLeft;
        }
    }

    public GameObject GetBlackTile()
    {
        return m_BlackTile.gameObject;
    }

    public void SetBlackTileColor(Color color)
    {
        m_BlackTile.GetComponent<BlackTile>().SetColor(color);
    }

    public void SetWhiteTileColor(Color color)
    {
        for (int i = 0; i < TilePerRow; i++)
            m_WhiteTiles[i].GetComponent<WhiteTile>().SetColor(color);
    }

    private void ComputeTileSize()
    {
        float floatHeight = Camera.main.orthographicSize * 2.0f;
        float halfHeight = Camera.main.orthographicSize;
        float floatWidth = floatHeight * Camera.main.aspect;
        float pixelSize = floatHeight / Camera.main.pixelHeight;

        m_PixelSize = pixelSize;
        m_LocalMostLeft = - halfHeight / 2.0f;

        m_TileWidth = (halfHeight - (TilePerRow + 1) * m_PixelSize - m_PixelSize) / TilePerRow;
        m_TileHeight = (floatWidth - (RowPerScreenWidth + 1) * m_PixelSize) / RowPerScreenWidth;
    }

    private void GenerateTiles()
    {
        ComputeTileSize();
        m_WhiteTiles = new List<Tile>(TilePerRow);
        for (int i = 0; i < TilePerRow; i++)
        {
            GameObject tileObj = Instantiate(PrefabWhiteTile) as GameObject;
            tileObj.GetComponent<Tile>().Width = m_TileWidth;
            tileObj.GetComponent<Tile>().Height = m_TileHeight;
            tileObj.transform.parent = transform;
            Vector3 pos = new Vector3();
            pos.y = 0.0f;
            pos.z = 0.0f;
            pos.x = i * (m_PixelSize + m_TileWidth)
                + (m_PixelSize + m_TileWidth / 2.0f)
                + m_LocalMostLeft;
            tileObj.transform.localPosition = pos;
            tileObj.transform.localRotation = Quaternion.identity;
            m_WhiteTiles.Add(tileObj.GetComponent<Tile>());
        }

        {
            GameObject tileObj = Instantiate(PrefabBlackTile) as GameObject;
            tileObj.transform.parent = transform;
            tileObj.GetComponent<Tile>().Width = m_TileWidth;
            tileObj.GetComponent<Tile>().Height = m_TileHeight;
            tileObj.SetActive(false);
            tileObj.transform.localRotation = Quaternion.identity;
            m_BlackTile = tileObj.GetComponent<Tile>();
        }

        {
            m_SuccessTile = Instantiate(PrefabSuccessTile) as GameObject;
            m_SuccessTile.transform.parent = transform;
            m_SuccessTile.GetComponent<AnimatingTile>().Width = m_TileWidth;
            m_SuccessTile.GetComponent<AnimatingTile>().Height = m_TileHeight;
            m_SuccessTile.transform.localRotation = Quaternion.identity;
            m_SuccessTile.SetActive(false);
        }

        {
            m_FailedTile = Instantiate(PrefabFailedTile) as GameObject;
            m_FailedTile.GetComponent<AnimatingTile>().Width = m_TileWidth;
            m_FailedTile.GetComponent<AnimatingTile>().Height = m_TileHeight;
            m_FailedTile.transform.parent = transform;
            m_FailedTile.transform.localRotation = Quaternion.identity;
            m_FailedTile.SetActive(false);
        }
    }

    private void OnResetRowAppearance()
    {
        m_FailedTile.SetActive(false);
        m_SuccessTile.SetActive(false);

        for (int i = 0; i < TilePerRow; i++)
            m_WhiteTiles[i].gameObject.SetActive(true);

        // Set black tile position
        Vector3 pos = new Vector3();
        pos.y = 0;
        pos.z = -0.1f;
        pos.x = BlackTilePosition * (m_PixelSize + m_TileWidth)
            + (m_PixelSize + m_TileWidth / 2.0f)
            + m_LocalMostLeft;
        m_BlackTile.transform.localPosition = pos;
        m_BlackTile.gameObject.SetActive(true);
        m_WhiteTiles[BlackTilePosition].gameObject.SetActive(false);
    }

    public int GetBlackTileInPos(Vector3 pos, float hitboxScale)
    {
        if (!m_SuccessTile.activeInHierarchy)
        {
            float topY = m_BlackTile.transform.position.y + m_TileHeight / 2.0f * hitboxScale;
            float bottomY = topY - m_TileHeight * hitboxScale;
            float leftX = m_BlackTile.transform.position.x - m_TileWidth / 2.0f * hitboxScale;
            float rightX = leftX + m_TileWidth * hitboxScale;
            if (pos.y <= topY && pos.y >= bottomY && pos.x >= leftX && pos.x <= rightX)
            {
                return 0;       // only 1 black tile => always zero
            }
        }
        return -1;
    }

    private void OnSuccessTap()
    {
        Vector3 pos = new Vector3();
        pos.y = 0;
        pos.z = -0.2f;
        pos.x = BlackTilePosition * (m_PixelSize + m_TileWidth)
            + (m_PixelSize + m_TileWidth / 2.0f)
                + m_LocalMostLeft;

        m_SuccessTile.transform.localPosition = pos;
        m_SuccessTile.SetActive(true);
    }

    private void OnFailedTap(int positionId)
    {
        Vector3 pos = new Vector3();
        pos.y = 0;
        pos.z = -0.2f;
        pos.x = positionId * (m_PixelSize + m_TileWidth)
            + (m_PixelSize + m_TileWidth / 2.0f)
                + m_LocalMostLeft;
        m_FailedTile.transform.localPosition = pos;
        m_FailedTile.SetActive(true);
    }

    void Awake()
    {
        GenerateTiles();
        OnResetRowAppearance();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class P1StartOverlay : MonoBehaviour {

    public float SpriteWidth;
    public float SpriteHeight;

    public float PixelSize;

    void Awake()
    {
        float nHeight = Camera.main.orthographicSize;
        PixelSize = (nHeight* 2.0f) / Camera.main.pixelHeight;
        float scale = (nHeight- 2 * PixelSize) / SpriteWidth;
        transform.localScale = new Vector3(scale, scale, 1.0f);
        float tileHeight = (nHeight - 5 * PixelSize) / 4.0f;
        float y = SpriteHeight / 2.0f - tileHeight / 2.0f;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

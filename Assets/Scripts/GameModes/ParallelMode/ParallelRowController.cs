﻿using UnityEngine;
using System.Collections;

public class ParallelRowController : MonoBehaviour {

    [Header("Player's tapping properties")]
    public float HitBoxScale = 1.5f;

    [Header("Row's control properties")]
    public int RowId;
    private int m_DoneBlackTileNumber;
    private bool m_BlackTileTapped;

    // TODO: add enable/disable
    private bool m_Enabled;
    public bool Enabled
    {
        get
        {
            return m_Enabled;
        }
        set
        {
            m_Enabled = value;
        }
    }

    private bool m_IsFailed;
    public bool IsFailed
    {
        get
        {
            return m_IsFailed;
        }
    }

    private bool m_IsDone;
    public bool IsDone
    {
        get
        {
            return m_IsDone;
        }
    }

    private ParallelRow m_Row;

    public void ResetAllColor()
    {
        m_Row.SetBlackTileColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
        m_Row.SetWhiteTileColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    public void ResetWhiteTileColor()
    {
        m_Row.SetWhiteTileColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    public void ResetRowRandomBlack()
    {
        m_IsFailed = false;
        m_IsDone = false;
        m_DoneBlackTileNumber = 0;
        // beware the garbage. but there are none >.<
        Randomkun.GetInstance().Reset();
        m_BlackTileTapped = false;

        m_Row.BlackTilePosition = Randomkun.GetInstance().GetRandomInt();
        BroadcastMessage("OnResetRowAppearance");
    }

    public void ResetRow()
    {
        m_IsFailed = false;
        m_IsDone = false;
        m_DoneBlackTileNumber = 0;
        m_BlackTileTapped = false;
        BroadcastMessage("OnResetRowAppearance");
    }

    private void OnTap(Vector3 mousePosition)
    {
        if (!m_Enabled)
            return;

        if (!m_IsFailed)
        {
            if (!m_IsDone)
            {
                bool isSuccessTap = false;
                int blackIdHit = m_Row.GetComponent<ParallelRow>().GetBlackTileInPos(mousePosition, HitBoxScale);
                int positionId = -1;
                if (blackIdHit >= 0)
                {
                    if (m_BlackTileTapped)
                        return;
                    else
                    {
                        m_BlackTileTapped = true;
                        isSuccessTap = true;
                    }
                }
                else
                {	// no blacktiles, check for whitetiles
                    float topY = transform.position.x + m_Row.TileHeight / 2.0f;
                    float bottomY = topY - m_Row.TileHeight;

                    if (mousePosition.x >= bottomY && mousePosition.x <= topY)
                    {
                        // TODO: mousePosition.y can be negative, LocalMostLeft is always zero (world coordinate)
                        // positionId = (int)Mathf.Floor((mousePosition.y - m_Row.LocalMostLeft) / (m_Row.TileWidth + m_Row.PixelSize));
                        positionId = (int)Mathf.Floor((Mathf.Abs(mousePosition.y)) / (m_Row.TileWidth + m_Row.PixelSize));
                        if (mousePosition.y < 0)
                            positionId = 3 - positionId;            // lol =)))

                        if (positionId == m_Row.BlackTilePosition)
                        {
                            return;
                        }
                    }
                    else return;
                }


                if (isSuccessTap)
                {
                    BroadcastMessage("OnSuccessTap", blackIdHit);
                    NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowSuccessTap",
                        new object[] { this, m_Row.BlackTilePosition });
                    m_DoneBlackTileNumber++;
                    if (m_DoneBlackTileNumber == 1)
                    {
                        m_IsDone = true;
                        NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowDonePlay", this);
                    }
                }
                else
                {
                    BroadcastMessage("OnFailedTap", positionId);
                    NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowFailedTap", this);
                    m_IsFailed = true;
                }
            }
        }
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("RowSuccessTap");
        NotificationCenter.GetInstance().RegisterNotification("RowDonePlay");
        NotificationCenter.GetInstance().RegisterNotification("RowFailedTap");
    }

    void Awake()
    {
        RegisterAllNotifications();
        m_Enabled = false;
        m_Row = GetComponent<ParallelRow>();
        m_BlackTileTapped = false;
        m_Row.BlackTilePosition = 0;
    }

	// Use this for initialization
	void Start () {
        Randomkun.GetInstance().Initialize(m_Row.TilePerRow);
        ResetRowRandomBlack();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

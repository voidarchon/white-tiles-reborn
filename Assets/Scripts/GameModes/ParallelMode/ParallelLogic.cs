﻿using UnityEngine;
using System.Collections;

public class ParallelLogic : Singleton<ParallelLogic> {

    [Header("GO References")]
    public ParallelRowsStreamController P1RowsStreamControllerGO;
    public ParallelRowsStreamController P2RowsStreamControllerGO;
    public Transform UIContainerGO;
    public UILabel P1ScoreGO;
    public UILabel P1ScoreShadowGO;
    public UILabel P2ScoreGO;
    public UILabel P2ScoreShadowGO;
    public GameObject P1StartOverlayGO;
    public GameObject P2StartOverlayGO;
    public GameObject[] StartLabelGO;

    [Header("Gameplay's parameters")]
    [Range(0, 50)]
    public int SpeedLerpStep;
    public float TimeStep;
    public bool GameIsPause;
    public bool GameIsOver;
    public float ScrollSpeed;

    [Header("Dont touch this, NOOB!")]
    public int StartRowId;
    [Range(0, 5)]
    public int StartIdOffset;
    public int[] CurrRowId;

    [Header("__debug")]
    public bool __debug;

    private bool m_FirstTimeStartGame;
    private int[] m_Score;

    private bool m_SoundOn;
    private ArcadeMusicController m_ArcadeMusicController;

    private IEnumerator SpeedChangerRoutine()
    {
        float currentSpeed = ScrollSpeed;
        float maxSpeed = 2 * currentSpeed;
        float atomicTime = (maxSpeed - currentSpeed) / (SpeedLerpStep + 1);
        float t = 0.0f;
        while (currentSpeed < maxSpeed)
        {
            if (!GameIsPause)
            {
                t += Time.deltaTime;
                if (t > TimeStep)
                {
                    t = 0.0f;
                    currentSpeed += atomicTime;
                    if (currentSpeed > maxSpeed)
                        currentSpeed = maxSpeed;
                    P1RowsStreamControllerGO.ScrollSpeed = currentSpeed;
                    P2RowsStreamControllerGO.ScrollSpeed = currentSpeed;
                    ScrollSpeed = currentSpeed;
                }
                yield return null;
            }
            yield return null;
        }
        Debug.Log("max speed");
        yield return null;
    }

    private void OnDoneRowDelegate(GameObject sender, object data)
    {
        ParallelRowController doneRow = (ParallelRowController)data;
        if (doneRow.transform.parent == P1RowsStreamControllerGO.transform)
        {
            P1RowsStreamControllerGO.EnableNextRow(doneRow);
            CurrRowId[0]++;
        }
        else {
            P2RowsStreamControllerGO.EnableNextRow(doneRow);
            CurrRowId[1]++;
        }
        // m_RowSuccess++;
    }

    private void OnRowSuccessTapDelegate(GameObject sender, object data)
    {
        object[] dataArr = (object[])data;
        ParallelRowController theRow = (ParallelRowController)dataArr[0];
        int idHit = (int)dataArr[1];
        if (StartRowId + StartIdOffset == theRow.RowId)
        {
            GameIsPause = false;
            if (m_FirstTimeStartGame)
            {
                if (m_SoundOn)
                {
                    m_ArcadeMusicController.Shuffle();
                    m_ArcadeMusicController.PlayMusic();
                }
                StartCoroutine(SpeedChangerRoutine());
                m_FirstTimeStartGame = false;
                for (int i = 0; i < StartLabelGO.Length; i++)
                    if (StartLabelGO[i].activeInHierarchy)
                    {
                        StartLabelGO[i].transform.parent = UIContainerGO;
                        StartLabelGO[i].SetActive(false);
                    }
            }
        }

        if (theRow.transform.parent == P1RowsStreamControllerGO.transform)
        {
            m_Score[0]++;
            SetP1Score(m_Score[0]);
        }
        else
        {
            m_Score[1]++;
            SetP2Score(m_Score[1]);
        }
    }

    private void OnRowFailedTapDelegate(GameObject sender, object data)
    {
        ParallelRowController failedRow = (ParallelRowController)data;
        GameIsPause = true;
        GameIsOver = true;

        CreateDataPackage();
        if (m_SoundOn)
            m_ArcadeMusicController.StopMusic();
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void OnParallelRowMissDelegate(GameObject sender, object data)
    {
        ParallelRowController failedRow = (ParallelRowController)data;
        GameIsPause = true;
        GameIsOver = true;

        CreateDataPackage();
        if (m_SoundOn)
            m_ArcadeMusicController.StopMusic();
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void SetP1Score(int value)
    {
        string str = value.ToString();
        P1ScoreGO.SetText(str);
        P1ScoreShadowGO.SetText(str);
    }

    private void SetP2Score(int value)
    {
        string str = value.ToString();
        P2ScoreGO.SetText(str);
        P2ScoreShadowGO.SetText(str);
    }

    private void RepositionScoreLabels()
    {
        float top = Camera.main.orthographicSize * Camera.main.aspect;

        Vector3 pos = new Vector3(top - top / 10.0f, 2.5f, -1.0f);
        P1ScoreGO.transform.position = pos;
        pos.x -= 0.03f;
        pos.y -= 0.03f;
        pos.z = -0.9f;
        P1ScoreShadowGO.transform.position = pos;

        pos = new Vector3(-top + top / 10.0f, -2.5f, -1.0f);
        P2ScoreGO.transform.position = pos;
        pos.x += 0.03f;
        pos.y -= 0.03f;
        pos.z = -0.9f;
        P2ScoreShadowGO.transform.position = pos;
    }

    private void CreateDataPackage()
    {
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score[0], m_Score[1] });
    }

    private void Initialize()
    {
        if (PlayerPrefs.GetInt("game_settings_sound") == 1)
            m_SoundOn = true;
        else m_SoundOn = false;
        m_FirstTimeStartGame = true;
        GameIsOver = false;
        CurrRowId = new int[2];
        CurrRowId[0] = StartRowId + StartIdOffset;
        CurrRowId[1] = CurrRowId[0];
        m_Score = new int[] { 0, 0 };
        SetP1Score(0); SetP2Score(0);
        RepositionScoreLabels();
    }

    private void WaitThenEndGame()
    {
        Application.LoadLevel("_EndScene_Parallel_");
    }

    private void RegisterAllNotifications()
    {

    }

    private void RegisterAllListeners()
    {
        NotificationCenter.GetInstance().RegisterListener("RowDonePlay", OnDoneRowDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowSuccessTap", OnRowSuccessTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowFailedTap", OnRowFailedTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("Parallel_RowMiss", OnParallelRowMissDelegate);
    }

    void Awake()
    {
        RegisterAllNotifications();
        Randomkun.GetInstance().ForgetEverything();
        m_ArcadeMusicController = GetComponent<ArcadeMusicController>();
    }

	// Use this for initialization
	void Start () {
        Initialize();
        P1RowsStreamControllerGO.DoInit();
        P2RowsStreamControllerGO.DoInit();
        RegisterAllListeners();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MirrorRowsStreamController : MonoBehaviour {
    [Header("Objects pool references")]
    public ObjectsPool MirrorRowsPoolGO;
    public ObjectsPool MirrorReverseRowsPoolGO;

    [Header("Gameplay parameters")]
    public float ScrollSpeed;
    public float ScrollUpSpeed;
    [Range(0, 100)]
    public int MirrorProp;
    public int RowCount;

    private int m_ObjectsPoolInited;

    private LinkedList<MirrorRowController> m_Rows;
    private float m_TileHeight;
    private float m_PixelSize;
    private bool m_DoneInit;
    private Vector3 m_ScrollVector;
    private Vector3 m_RowSpawnPoint;
    private int m_NextId;

    private void InitializeFirstLook()
    {
        m_NextId = MirrorLogic.Instance.StartRowId;
        m_Rows = new LinkedList<MirrorRowController>();
        GameObject obj = MirrorRowsPoolGO.PeekNextObject();
        float tileHeight = obj.GetComponent<Row>().TileHeight;
        float pixelSize = obj.GetComponent<Row>().PixelSize;
        float screenTopY = Camera.main.orthographicSize;
        m_TileHeight = tileHeight;
        m_PixelSize = pixelSize;
        m_RowSpawnPoint = new Vector3(0.0f, screenTopY - (pixelSize + tileHeight / 2.0f) + (pixelSize + tileHeight), 0.0f);

        for (int i = 0; i < RowCount; i++)
        {
            int r = Random.Range(0, 100);
            GameObject row;
            if (i <= MirrorLogic.Instance.StartIdOffset)
            {
                row = MirrorRowsPoolGO.GetObject();
            }
            else
            {
                if (r < MirrorProp)
                    row = MirrorReverseRowsPoolGO.GetObject();
                else row = MirrorRowsPoolGO.GetObject();
            }

            Vector3 pos = new Vector3();
            pos.x = 0.0f;
            pos.z = 0.0f;
            pos.y = -screenTopY + (pixelSize + tileHeight / 2.0f) + i * (pixelSize + tileHeight);
            row.transform.position = pos;
            row.SetActive(true);
            row.GetComponent<MirrorRowController>().RowId = m_NextId;
            if (i == MirrorLogic.Instance.StartIdOffset)
            {
                row.GetComponent<MirrorRowController>().Enabled = true;
                // start label
                // int blackTileNum = row.GetComponent<MirrorRowController>().FrozenBlackTileNumber;
                // for (int j = 0; j < blackTileNum; j++)
                {
                    MirrorLogic.Instance.StartLabelGO[0].transform.parent =
                        row.GetComponent<Row>().GetBlackTileById(0).transform;
                    MirrorLogic.Instance.StartLabelGO[0].transform.localPosition =
                        new Vector3(0.0f, 0.0f, -1.0f);
                    MirrorLogic.Instance.StartLabelGO[0].gameObject.SetActive(true);
                }
            }
            if (i < MirrorLogic.Instance.StartIdOffset)
            {
                // row.GetComponent<Row>().SetBlackTileColor(new Color(0.196f, 0.086f, 0.224f, 1.0f));
                // row.GetComponent<Row>().SetWhiteTileColor(new Color(0.196f, 0.086f, 0.224f, 1.0f));
                MirrorLogic.Instance.StartOverlayGO.transform.parent = row.transform;
                MirrorLogic.Instance.StartOverlayGO.transform.localPosition = new Vector3(0.0f, 0.0f, -5.0f);
                MirrorLogic.Instance.StartOverlayGO.gameObject.SetActive(true);
            }
            m_Rows.AddFirst(row.GetComponent<MirrorRowController>());
            m_NextId++;
        }
        m_DoneInit = true;
    }

    public void EnableNextRow(MirrorRowController currRow)
    {
        currRow.Enabled = false;
        LinkedListNode<MirrorRowController> currNode = m_Rows.Find(currRow);
        currNode.Previous.Value.Enabled = true;
    }

    private bool IsLastRowOutOfScreen()
    {
        return (m_Rows.Last.Value.transform.position.y <= -Camera.main.orthographicSize - m_TileHeight / 2.0f);
    }

    private void ScrollRowDown()
    {
        for (LinkedListNode<MirrorRowController> node = m_Rows.First;
             node != null; node = node.Next)
        {
            node.Value.transform.Translate(m_ScrollVector);
        }
    }

    private IEnumerator ScrollUpRoutine()
    {
        NotificationCenter.GetInstance().InvokeNotification(gameObject, "Mirror_RowMiss", m_Rows.Last.Value);
        float dest = -Camera.main.orthographicSize + m_TileHeight / 2.0f;
        float currVal = m_Rows.Last.Value.transform.position.y;
        while (currVal < dest)
        {
            Vector3 scrollUpVector = new Vector3(0.0f, ScrollUpSpeed * Time.deltaTime, 0.0f);
            if (currVal + ScrollUpSpeed * Time.deltaTime <= dest)
            {
                for (LinkedListNode<MirrorRowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.Translate(scrollUpVector);
                }
                currVal = m_Rows.Last.Value.transform.position.y;
            }
            else
            {
                Vector3 lastScrollUpVector = new Vector3(0.0f,
                                                         dest - m_Rows.Last.Value.transform.position.y,
                                                         0.0f);
                for (LinkedListNode<MirrorRowController> node = m_Rows.First;
                     node != null; node = node.Next)
                {
                    node.Value.transform.Translate(lastScrollUpVector);
                }
                currVal = dest;
            }
            yield return null;
        }
        yield return null;
    }

    private void RenewRows()
    {
        if (IsLastRowOutOfScreen())
        {
            MirrorRowController oldRow = m_Rows.Last.Value;
            if (oldRow.RowId < MirrorLogic.Instance.StartIdOffset)
            {
                NotificationCenter.GetInstance().InvokeNotification(gameObject, "Mirror_StartRowGone", null);
            }
            if (MirrorLogic.Instance.CurrRowId == oldRow.RowId)
            {
                MirrorLogic.Instance.GameIsPause = true;
                StartCoroutine(ScrollUpRoutine());
                return;
            }

            oldRow.Enabled = false;
            oldRow.ResetWhiteTileColor();
            oldRow.ResetRowRandomBlack();
            oldRow.GetComponent<PooledObject>().ThrowBackToPool();
            m_Rows.RemoveLast();

            int r = Random.Range(0, 100);
            GameObject newRow;
            if (r < MirrorProp)
                newRow = MirrorReverseRowsPoolGO.GetObject();
            else newRow = MirrorRowsPoolGO.GetObject();

            newRow.transform.position = m_Rows.First.Value.transform.position + new Vector3(0.0f, m_PixelSize + m_TileHeight, 0.0f);
            newRow.SetActive(true);
            newRow.GetComponent<MirrorRowController>().RowId = m_NextId;
            m_Rows.AddFirst(newRow.GetComponent<MirrorRowController>());
            m_NextId++;
        }
    }

    private void UpdateRows()
    {
        if (m_DoneInit && !MirrorLogic.Instance.GameIsPause)
        {
            m_ScrollVector.y = -ScrollSpeed * Time.deltaTime;
            ScrollRowDown();
            RenewRows();
        }
    }

    private void OnObjectsPoolDoneInit()
    {
        m_ObjectsPoolInited++;
        if (m_ObjectsPoolInited == 2)
            InitializeFirstLook();
    }

    /// <summary>
    /// Initialize the gameboard, called from ArcadeLogic
    /// </summary>
    public void DoInit()
    {
        m_ObjectsPoolInited = 0;
        m_DoneInit = false;
        m_ScrollVector = new Vector3(0.0f, -ScrollSpeed * Time.deltaTime, 0.0f);
        ScrollSpeed = MirrorLogic.Instance.ScrollSpeed;

        MirrorRowsPoolGO.OnDoneInitEventListener += OnObjectsPoolDoneInit;
        MirrorReverseRowsPoolGO.OnDoneInitEventListener += OnObjectsPoolDoneInit;
        MirrorRowsPoolGO.InitializePool();
        MirrorReverseRowsPoolGO.InitializePool();
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("Mirror_RowMiss");
        NotificationCenter.GetInstance().RegisterNotification("Mirror_StartRowGone");
    }

    void Awake()
    {
        RegisterAllNotifications();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        UpdateRows();
        if (MirrorLogic.Instance.GameIsOver)
            return;
#if !UNITY_EDITOR
		for (int i = 0; i < Input.touchCount; i++) {
			if (i > 1) return;
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.GetTouch(i).position);
				for (LinkedListNode<MirrorRowController> node = m_Rows.First;
			     node != null; node = node.Next) {
					if (node.Value.Enabled)
						node.Value.SendMessage ("OnTap", touchPos);
				}
			}
		}
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (LinkedListNode<MirrorRowController> node = m_Rows.First;
                 node != null; node = node.Next)
            {
                if (node.Value.Enabled)
                    node.Value.SendMessage("OnTap", touchPos);
            }
        }
#endif
	}
}

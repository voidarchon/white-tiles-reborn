﻿using UnityEngine;
using System.Collections;

public class MirrorRowController : MonoBehaviour {

    [Header("Player's tapping properties")]
    public float HitBoxScale = 1.5f;

    [Header("Row's control properties")]
    public bool ReverseTile;
    public int RowId;
    private bool m_BlackTileTapped;

    private bool m_Enabled;
    public bool Enabled
    {
        get
        {
            return m_Enabled;
        }
        set
        {
            m_Enabled = value;
        }
    }

    private bool m_IsFailed;
    public bool IsFailed
    {
        get
        {
            return m_IsFailed;
        }
    }

    private bool m_IsDone;
    public bool IsDone
    {
        get
        {
            return m_IsDone;
        }
    }

    private Row m_Row;

    [Header("For tutorial")]
    public bool IsTutorialRow;
    public int TutBlackTileId;

    public void ResetAllColor()
    {
        m_Row.SetBlackTileColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
        m_Row.SetWhiteTileColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    public void ResetWhiteTileColor()
    {
        m_Row.SetWhiteTileColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    public void ResetRowRandomBlack()
    {
        m_IsFailed = false;
        m_IsDone = false;
        // beware the garbage. but there are none >.<
        Randomkun.GetInstance().Reset();
        m_BlackTileTapped = false;
        m_Row.BlackTilePosition[0] = Randomkun.GetInstance().GetRandomInt();
        BroadcastMessage("OnResetRowAppearance");
    }

    public void ResetRowRandomBlack(int id)
    {
        m_IsFailed = false;
        m_IsDone = false;
        // beware the garbage. but there are none >.<
        Randomkun.GetInstance().Reset();
        m_BlackTileTapped = false;
        m_Row.BlackTilePosition[0] = id;
        BroadcastMessage("OnResetRowAppearance");
    }

    public void ResetRow()
    {
        m_IsFailed = false;
        m_IsDone = false;
        m_BlackTileTapped = false;
        BroadcastMessage("OnResetRowAppearance");
    }

    private void OnTap(Vector3 mousePosition)
    {
        if (!m_Enabled)
            return;

        if (!m_IsFailed)
        {
            if (!m_IsDone)
            {
                bool isSuccessTap = false;
                int blackIdHit;
                if (ReverseTile)
                {
                    Vector3 reverseMousePos = new Vector3(-mousePosition.x,
                        mousePosition.y, mousePosition.z);
                    blackIdHit = m_Row.GetComponent<Row>().GetBlackTileInPos(reverseMousePos, HitBoxScale);
                }
                else blackIdHit = m_Row.GetComponent<Row>().GetBlackTileInPos(mousePosition, HitBoxScale);
                int positionId = -1;
                if (blackIdHit >= 0)
                {
                    if (m_BlackTileTapped)
                        return;
                    else
                    {
                        m_BlackTileTapped = true;
                        isSuccessTap = true;
                    }
                }
                else
                {	// no blacktiles, check for whitetiles
                    float topY = transform.position.y + m_Row.TileHeight / 2.0f;
                    float bottomY = topY - m_Row.TileHeight;

                    if (mousePosition.y >= bottomY && mousePosition.y <= topY)
                    {
                        positionId = (int)Mathf.Floor((mousePosition.x - m_Row.ScreenMostLeft) / (m_Row.TileWidth + m_Row.PixelSize));
                        for (int i = 0; i < m_Row.BlackTilePosition.Length; i++)
                        {
                            if (ReverseTile)
                            {
                                if (positionId == m_Row.TilePerRow - m_Row.BlackTilePosition[i] - 1)
                                {
                                    return;
                                }
                            }
                            else
                            {
                                if (positionId == m_Row.BlackTilePosition[i])
                                    return;
                            }
                        }
                    }
                    else return;
                }


                if (isSuccessTap)
                {
                    BroadcastMessage("OnSuccessTap", blackIdHit);
                    NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowSuccessTap",
                        new object[] { this, m_Row.BlackTilePosition[blackIdHit] });
                    if (m_BlackTileTapped)
                    {
                        m_IsDone = true;
                        NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowDonePlay", this);
                    }
                }
                else
                {
                    if (positionId < 0) positionId = 0;
                    if (positionId >= m_Row.TilePerRow) positionId = m_Row.TilePerRow - 1;
                    BroadcastMessage("OnFailedTap", positionId);
                    NotificationCenter.GetInstance().InvokeNotification(gameObject, "RowFailedTap", this);
                    m_IsFailed = true;
                }
            }
        }
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("RowSuccessTap");
        NotificationCenter.GetInstance().RegisterNotification("RowDonePlay");
        NotificationCenter.GetInstance().RegisterNotification("RowFailedTap");
    }

    void Awake()
    {
        RegisterAllNotifications();
        m_Enabled = false;
        m_Row = GetComponent<Row>();
        m_BlackTileTapped = false;
        m_Row.BlackTilePosition = new int[1];
    }

	// Use this for initialization
	void Start () {
        Randomkun.GetInstance().Initialize(m_Row.TilePerRow);
        if (IsTutorialRow)
            ResetRowRandomBlack(TutBlackTileId);
        else ResetRowRandomBlack();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

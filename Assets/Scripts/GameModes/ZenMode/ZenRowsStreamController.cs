﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO: tap outside the screen, sounds stupid but it can happen.
public class ZenRowsStreamController : MonoBehaviour {

    [Header("Rowspool references")]
    public ObjectsPool ZenRowsPoolGO;

    [Header("Gameplay parameters")]
    public float ScrollDownTime;
    public int RowCount;

    private LinkedList<RowController> m_Rows;

    private float m_TileHeight;
    private float m_PixelSize;

    private bool m_DoneInit;
    private Vector3 m_RowSpawnPoint;
    private int m_NextId;

    private Coroutine m_CurrentScrollRoutine;

    private void InitializeFirstLook()
    {
        m_NextId = ZenLogic.Instance.StartRowId;
        m_Rows = new LinkedList<RowController>();
        GameObject obj = ZenRowsPoolGO.PeekNextObject();
        float tileHeight = obj.GetComponent<Row>().TileHeight;
        float pixelSize = obj.GetComponent<Row>().PixelSize;
        float screenTopY = Camera.main.orthographicSize;
        m_TileHeight = tileHeight;
        m_PixelSize = pixelSize;
        m_RowSpawnPoint = new Vector3(0.0f, screenTopY - (pixelSize + tileHeight / 2.0f) + (pixelSize + tileHeight), 0.0f);

        for (int i = 0; i < RowCount; i++)
        {
            GameObject row = ZenRowsPoolGO.GetObject();
            Vector3 pos = new Vector3();
            pos.x = 0.0f;
            pos.z = 0.0f;
            pos.y = -screenTopY + (pixelSize + tileHeight / 2.0f) + (i) * (pixelSize + tileHeight);
            row.transform.position = pos;
            row.SetActive(true);
            row.GetComponent<RowController>().RowId = m_NextId;
            if (i == ZenLogic.Instance.StartIdOffset)
            {
                row.GetComponent<RowController>().Enabled = true;
                // row START label, classic mode => only 1 black tile
                ZenLogic.Instance.StartLabelGO.transform.parent =
                    row.GetComponent<Row>().GetBlackTileById(0).transform;
                ZenLogic.Instance.StartLabelGO.transform.localPosition =
                    new Vector3(0.0f, 0.0f, -1.0f);
                ZenLogic.Instance.StartLabelGO.SetActive(true);
            }
            if (i < ZenLogic.Instance.StartIdOffset)
            {
                // row.GetComponent<Row>().SetBlackTileColor(new Color(0.196f, 0.086f, 0.224f, 1.0f));
                // row.GetComponent<Row>().SetWhiteTileColor(new Color(0.196f, 0.086f, 0.224f, 1.0f));
                ZenLogic.Instance.StartOverlayGO.transform.parent = row.transform;
                ZenLogic.Instance.StartOverlayGO.transform.localPosition = new Vector3(0.0f, 0.0f, -1.0f);
                ZenLogic.Instance.StartOverlayGO.gameObject.SetActive(true);
            }
            else
            {
                // if (ColorfulMode)
                //    row.GetComponent<Row>().SetBlackTileColor(new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f));
                // set START label
            }
            m_Rows.AddFirst(row.GetComponent<RowController>());
            m_NextId++;
        }
        m_DoneInit = true;
    }

    private bool IsLastRowOutOfScreen()
    {
        return (m_Rows.Last.Value.transform.position.y <= -Camera.main.orthographicSize - m_TileHeight / 2.0f);
    }

    private void RenewRows()
    {
        if (IsLastRowOutOfScreen())
        {
            RowController oldRow = m_Rows.Last.Value;
            if (oldRow.RowId < ZenLogic.Instance.StartIdOffset)
            {
                NotificationCenter.GetInstance().InvokeNotification(gameObject, "Zen_StartRowGone", null);
            }

            oldRow.Enabled = false;
            oldRow.ResetWhiteTileColor();
            oldRow.ResetRowRandomBlack();
            oldRow.transform.position = m_Rows.First.Value.transform.position + new Vector3(0.0f, m_PixelSize + m_TileHeight, 0.0f);
            oldRow.RowId = m_NextId;
            m_NextId++;
            m_Rows.RemoveLast();
            m_Rows.AddFirst(oldRow);
        }
    }

    private IEnumerator ScrollDownRoutine(RowController currRow)
    {
        Vector3 startPos = currRow.transform.position;
        Vector3 destPos = new Vector3(startPos.x,
            -Camera.main.orthographicSize - m_TileHeight / 2.0f - m_PixelSize,
            startPos.z);
        float startTime = Time.time;
        Vector3 currPos = currRow.transform.position;
        while (currPos != destPos)
        {
            if (!ZenLogic.Instance.GameIsPause)
            {
                Vector3 translateVector = Vector3.Lerp(startPos,
                    destPos,
                    (Time.time - startTime) / ScrollDownTime) - currPos;
                for (LinkedListNode<RowController> node = m_Rows.First;
                    node != null; node = node.Next)
                {
                    node.Value.transform.Translate(translateVector);
                }
                currPos += translateVector;
                RenewRows();
            }
            yield return null;
        }
        yield return null;
    }

    public void EnableNextRow(RowController currRow)
    {
        currRow.Enabled = false;
        LinkedListNode<RowController> currNode = m_Rows.Find(currRow);
        currNode.Previous.Value.Enabled = true;
        if (m_CurrentScrollRoutine != null)
            StopCoroutine(m_CurrentScrollRoutine);
        m_CurrentScrollRoutine = StartCoroutine(ScrollDownRoutine(currRow));
    }

    private void RegisterAllNotifications()
    {
        NotificationCenter.GetInstance().RegisterNotification("Zen_StartRowGone");
    }

    void Awake()
    {
        RegisterAllNotifications();
    }

    public void Initialize()
    {
        m_DoneInit = false;
        m_CurrentScrollRoutine = null;
        ZenRowsPoolGO.OnDoneInitEventListener += InitializeFirstLook;
        ZenRowsPoolGO.InitializePool();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (ZenLogic.Instance.GameIsOver)
            return;
#if !UNITY_EDITOR
		for (int i = 0; i < Input.touchCount; i++) {
			if (i > 1) return;
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Vector3 touchPos = Camera.main.ScreenToWorldPoint (Input.GetTouch(i).position);
				for (LinkedListNode<RowController> node = m_Rows.First;
			     node != null; node = node.Next) {
					if (node.Value.Enabled)
						node.Value.SendMessage ("OnTap", touchPos);
				}
			}
		}
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (LinkedListNode<RowController> node = m_Rows.First;
                 node != null; node = node.Next)
            {
                if (node.Value.Enabled)
                    node.Value.SendMessage("OnTap", touchPos);
            }
        }
#endif
	}
}

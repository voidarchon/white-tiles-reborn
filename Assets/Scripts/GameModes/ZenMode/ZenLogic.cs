﻿using UnityEngine;
using System.Collections;

public class ZenLogic : Singleton<ZenLogic> {
    [Header("GO references")]
    public ZenRowsStreamController ZenRowsStreamControllerGO;
    public UILabel ScoreLabelGO;
    public UILabel ScoreShadowLabelGO;
    public GameObject StartLabelGO;
    public GameObject StartOverlayGO;
    public GameObject TimeIsUpLabelGO;
    public Transform UIContainerGO;
    public UIProcessBar ProcessBarGO;

    [Header("Sounds")]
    public AudioClip[] Sounds;

    [Header("Gameplay parameters")]
    public bool GameIsPause;
    public bool GameIsOver;
    public float PlayTime;

    [Header("Dont touch this, NOOB!")]
    public int StartRowId;
    [Range(0, 5)]
    public int StartIdOffset;
    public int CurrRowId;

    private bool m_FirstTimeStartGame;

    // sound effects
    private bool m_SoundOn;
    private int m_LastIdHit;
    private int m_LastSoundPitch;
    private AudioSource m_AudioSource;

    // scores
    private int m_Score;
    private Coroutine m_TimerRoutine;

    private void OutOfTime()
    {
        TimeIsUpLabelGO.SetActive(true);
        GameIsOver = true;
        GameIsPause = true;
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score });
        Invoke("WaitThenEndGame", 2.0f);
    }

    private IEnumerator TimerRoutine()
    {
        float elapsedTime = PlayTime;
        while (elapsedTime > 0)
        {
            elapsedTime -= Time.deltaTime;
            if (elapsedTime < 0) elapsedTime = 0;
            ProcessBarGO.SetPercent(elapsedTime / PlayTime);
            yield return null;
        }
        yield return null;
        OutOfTime();
    }

    private void SetScore(int score)
    {
        string scoreStr = score.ToString();
        ScoreLabelGO.SetText(scoreStr);
        ScoreShadowLabelGO.SetText(scoreStr);
    }

    private void OnDoneRowDelegate(GameObject sender, object data)
    {
        RowController doneRow = (RowController)data;
        ZenRowsStreamControllerGO.EnableNextRow(doneRow);
    }

    private void OnRowSuccessTapDelegate(GameObject sender, object data)
    {
        object[] dataArr = (object[])data;
        RowController theRow = (RowController)dataArr[0];
        int idHit = (int)dataArr[1];
        if (StartRowId + StartIdOffset == theRow.RowId)
        {
            GameIsPause = false;
            if (m_FirstTimeStartGame)
            {
                // Invoke("OutOfTime", PlayTime);
                m_TimerRoutine = StartCoroutine(TimerRoutine());
                m_FirstTimeStartGame = false;
                StartLabelGO.transform.parent = UIContainerGO;
                StartLabelGO.SetActive(false);
            }
        }
        m_Score++;
        SetScore(m_Score);
        if (m_SoundOn)
            PlaySoundEffect(idHit);
    }

    private void OnRowFailedTapDelegate(GameObject sender, object data)
    {
        RowController failedRow = (RowController)data;
        GameIsPause = true;
        GameIsOver = true;
        if (m_TimerRoutine != null)
            StopCoroutine(m_TimerRoutine);
        SceneToSceneData dataGO = (SceneToSceneData)Object.FindObjectOfType(typeof(SceneToSceneData));
        dataGO.SetPayLoadData(new object[] { m_Score });
        Invoke("WaitThenEndGame", 2.0f);
    }

    private void OnStartRowGoneDelegate(GameObject sender, object data)
    {
        StartOverlayGO.transform.parent = UIContainerGO;
        StartOverlayGO.SetActive(false);
    }

    private void WaitThenEndGame()
    {
        NotificationCenter.GetInstance().ClearAllNotificationsAndListeners();
        PlayerPrefs.SetInt("HasLightning", 0);
        PlayerPrefs.SetInt("ReverseActivated", 0);
        PlayerPrefs.SetString("ArcadeMode", "zen");
        Application.LoadLevel("_EndScene_ArcadeMode_Universal_");
    }

    private void RegisterAllListeners()
    {
        NotificationCenter.GetInstance().RegisterListener("RowDonePlay", OnDoneRowDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowSuccessTap", OnRowSuccessTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("RowFailedTap", OnRowFailedTapDelegate);
        NotificationCenter.GetInstance().RegisterListener("Zen_StartRowGone", OnStartRowGoneDelegate);
    }

    private void PlaySoundEffect(int idHit)
    {
        if (m_LastIdHit < 0)
        {
            m_LastSoundPitch = Random.Range(0, 26);
        }
        else
        {
            if (idHit > m_LastIdHit)
            {
                m_LastSoundPitch = Random.Range(m_LastSoundPitch, 26);
            }
            else if (idHit < m_LastIdHit)
            {
                m_LastSoundPitch = Random.Range(0, m_LastSoundPitch);
            }
        }
        m_LastIdHit = idHit;
        m_AudioSource.PlayOneShot(Sounds[m_LastSoundPitch]);
    }

    void Awake()
    {
        Randomkun.GetInstance().ForgetEverything();
        ProcessBarGO.Length = Camera.main.orthographicSize * Camera.main.aspect * 2.0f;
        ProcessBarGO.SetPercent(1.0f);
        m_AudioSource = GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
        Application.targetFrameRate = 60;
        m_Score = 0;
        SetScore(m_Score);
        m_FirstTimeStartGame = true;
        CurrRowId = StartRowId + StartIdOffset;
        m_LastIdHit = -1;
        PlayTime = PlayerPrefs.GetInt("ZenPlayTime");
        if (PlayerPrefs.GetInt("game_settings_sound") == 1)
            m_SoundOn = true;
        else m_SoundOn = false;
        StartOverlayGO.GetComponent<StartOverlay>().Initialize(4);
        ZenRowsStreamControllerGO.Initialize();

        RegisterAllListeners();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
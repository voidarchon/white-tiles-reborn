﻿Shader "Custom/Archivement" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Percent ("Percent", Float) = 0.0
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			uniform float _Percent;
			uniform float4 _Color;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

	        struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
	            vertexOutput output;
				output.tex = input.texcoord;
	
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
			float4 frag(vertexOutput input) : COLOR
			{
				float4 texColor = tex2D(_MainTex, input.tex.xy).rgba;
				float4 outColor = lerp(texColor, _Color, _Percent);

				// return float4(outColor.rgb, texColor.a);
				float r = step(0.5, texColor.r);
				return float4(r, r, r, texColor.a);
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

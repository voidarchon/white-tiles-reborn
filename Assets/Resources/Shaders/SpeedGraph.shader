﻿Shader "Custom/SpeedGraph" {
	Properties {
		_ColorBottom ("Color Bottom", Color) = (1,1,1,1)
		_ColorTop ("Color Top", Color) = (0, 0, 0, 0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
	Tags { "Queue"="Transparent" }
	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma vertex vert
        #pragma fragment frag

		uniform sampler2D _MainTex;
		uniform float4 _ColorBottom;
		uniform float4 _ColorTop;

		struct vertexInput {
			float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
        };

        struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
        };

        vertexOutput vert(vertexInput input)
        {
            vertexOutput output;
            output.tex = input.texcoord;

            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
        }
	 
            
        float4 frag(vertexOutput input) : COLOR
        {
			float r = 1.0 - input.tex.y / 3.0f;
			float4 out_color = _ColorBottom * r + _ColorTop * (1.0 - r);
			return (out_color);
        }

		ENDCG
	} 
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/RankingLeadIn" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Percent ("Percent", Float) = 1.0
	}
	SubShader {
	Tags { "Queue"="Transparent" }
	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma vertex vert
        #pragma fragment frag

		uniform sampler2D _MainTex;
		uniform float4 _Color;
		uniform float _Percent;

		struct vertexInput {
			float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
        };

        struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
        };

        vertexOutput vert(vertexInput input)
        {
            vertexOutput output;
            output.tex = input.texcoord;

            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
        }

        float4 frag(vertexOutput input) : COLOR
        {
			float alpha = tex2D(_MainTex, input.tex.xy).a;
			float4 outColor = _Color;
			// float range = 1.0 - _Percent;
			outColor.a = smoothstep(_Percent, _Percent + 0.5, alpha);
			// outColor.a = (alpha - _Percent) / range;
			return (outColor);
        }

		ENDCG
	} 
	}
	FallBack "Diffuse"
}

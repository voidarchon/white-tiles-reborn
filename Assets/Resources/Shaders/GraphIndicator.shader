﻿Shader "Custom/GraphIndicator" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
	Tags { "Queue"="Transparent" }
	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma vertex vert
        #pragma fragment frag

		uniform sampler2D _MainTex;
		uniform float4 _Color;

		struct vertexInput {
			float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
        };

        struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
        };

        vertexOutput vert(vertexInput input)
        {
            vertexOutput output;
            output.tex = input.texcoord;

            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
        }
	 
            
        float4 frag(vertexOutput input) : COLOR
        {
			float2 center = float2(0.5, 0.5);
            float2 c = (input.tex.xy - center) / center;

			float l = length(c);
			float r = 1.0 - smoothstep(1.0 - 0.4, 1.0, l);

			return (float4(_Color.r, _Color.g, _Color.b, _Color.a * r));
        }

		ENDCG
	} 
	}
	FallBack "Diffuse"
}

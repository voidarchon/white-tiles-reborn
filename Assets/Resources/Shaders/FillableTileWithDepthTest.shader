﻿Shader "Custom/FillableTileWithDepthTest" {
	Properties {
		_FillColor ("Color", Color) = (1,1,1,1)
		_FillPercent ("Fill Percent", Float) = 0.5
	}
	SubShader {
		Tags { "Queue"="Transparent+2" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float4 _FillColor;
			uniform float _FillPercent;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.tex = input.texcoord;

				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float2 center = float2(0.5, 0.5);
				float2 c = (input.tex.xy - center) / center;

				float l = length(c);
				float r = max(step(_FillPercent, abs(c.x) - 0.01), step(_FillPercent, abs(c.y) - 0.01));
				float4 outColor = (1.0 - r) * _FillColor;

				return float4(outColor.r, outColor.g, outColor.b, outColor.a);
			}

			ENDCG
		} 
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/DepthWrite" {
  SubShader {
    // draw after all opaque objects (queue = 2001)
    Tags { "Queue"="Transparent+1" }
    Pass {
		ZWrite On
		Blend Zero One // keep the image behind it
    }
  } 
}

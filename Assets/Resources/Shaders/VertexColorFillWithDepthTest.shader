﻿Shader "Custom/VertexColorFillWithDepthTest" {
	Properties {
	}
	SubShader {
		Tags { "Queue"="Transparent+2"}
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct vertexInput {
				float4 vertex : POSITION;
				float4 vertexColor: COLOR;
			};

	        struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
			};

			vertexOutput vert(vertexInput input)
			{
	            vertexOutput output;
				output.color = input.vertexColor;
	
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				return (input.color);
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/Tile" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Fill ("Fill", Float) = 0.5
		_FillColor ("Fill Color", Color) = (0, 0, 0, 1)
		[MaterialToggle] _IsGood ("Is Good?", Float) = 0.0
		_GoodColor ("Good Color", Color) = (0, 0, 0, 1)
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float4 _Color;
			uniform float _Fill;
			uniform float4 _FillColor;
			uniform float _IsGood;
			uniform float4 _GoodColor;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.tex = input.texcoord;

				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float2 center = float2(0.5, 0.5);
				float2 c = (input.tex.xy - center) / center;

				float l = length(c);
				float r = max(step(_Fill, abs(c.x)), step(_Fill, abs(c.y)));
				float4 outColor = _Color * r + (1.0 - r) * _FillColor;
				if (_IsGood > 0.5)
					outColor = _GoodColor;

				return outColor;
			}

			ENDCG
		} 
	}
	FallBack "Diffuse"
}

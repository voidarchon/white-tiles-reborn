﻿Shader "Custom/RoundedRectWithHeader" {
	Properties {
		_AntiAliasingThreshold ("AA Threshold", Float) = 0.02
		_Radius ("Radius", Float) = 0.5
		_HeaderColor ("Header Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float _AntiAliasingThreshold;
			uniform float _Radius;
			uniform float4 _HeaderColor;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 vertexColor: COLOR;
				float4 texcoord : TEXCOORD0;
			};

	        struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
	            vertexOutput output;
				output.color = input.vertexColor;
				output.tex = input.texcoord;
	
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float2 center = float2(0.5, 0.5);
				float2 c = (input.tex.xy - center) / center;

				float d = length(float2(max(0.0, abs(c.x) + _Radius - 1.0),
										max(0.0, abs(c.y) + _Radius - 1.0)
									   )
								);

				float r = smoothstep(_Radius, _Radius - _AntiAliasingThreshold, d);

				float t = step(0.5, c.y);

				float4 outColor = float4(input.color.rgb, input.color.a * r) * (1.0-t) 
									+ t * float4(_HeaderColor.rgb, _HeaderColor.a * r);

				return outColor;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/Lightning" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
	Tags { "Queue"="Transparent+2" }
	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma vertex vert
        #pragma fragment frag

		uniform sampler2D _MainTex;
		uniform float4 _Color;

		struct vertexInput {
			float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
        };

        struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
        };

        vertexOutput vert(vertexInput input)
        {
            vertexOutput output;
            output.tex = input.texcoord;

            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
        }
	 
            
        float4 frag(vertexOutput input) : COLOR
        {
			return (_Color);
        }

		ENDCG
	} 
	}
	FallBack "Diffuse"
}
﻿Shader "Custom/ProcessBar" {
	Properties {
		_FGRColor ("Foreground Color", Color) = (1,1,1,1)
		_BGRColor ("Background Color", Color) = (0.3, 0.3, 0.3, 1.0)
		_Percent ("Fill Percent", Float) = 0.0
	}
	SubShader {
	Tags { "Queue"="Transparent" }
	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma vertex vert
        #pragma fragment frag

		uniform float _Percent;
		uniform float4 _FGRColor;
		uniform float4 _BGRColor;

		struct vertexInput {
			float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
        };

        struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
        };

        vertexOutput vert(vertexInput input)
        {
            vertexOutput output;
            output.tex = input.texcoord;

            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
        }
	 
            
        float4 frag(vertexOutput input) : COLOR
        {
			float r = step(_Percent, input.tex.x);
			return (_FGRColor * (1.0 - r) + r * _BGRColor);
        }

		ENDCG
	} 
	}
	FallBack "Diffuse"
}

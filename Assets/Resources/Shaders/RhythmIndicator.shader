﻿Shader "Custom/RhythmIndicator" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_TheMagicNumber ("Magic Number", Float) = 0.0
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform float4 _Color;
			uniform float _TheMagicNumber;

			struct vertexInput {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

	        struct vertexOutput {
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
	            vertexOutput output;
				output.tex = input.texcoord;
	
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float2 c = (input.tex - float2(0.5, 0.5)) / float2(0.5, 0.5);
				float width = 0.4;
				float height = 0.01;
				float radius = 0.5 + 0.1 * sin(_TheMagicNumber * 20.0);
				
				float d = length(float2(max(0.0, abs(c.x) - width), 
                          max(0.0, abs(c.y * 2.0) - height)));
    			float r = 1.0 - smoothstep(0.0, radius, d);
    			
				return float4(_Color.r * r, _Color.g * r, _Color.b * r, r);
				// return float4(-c.x, 0.0, 0.0, 1.0);
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

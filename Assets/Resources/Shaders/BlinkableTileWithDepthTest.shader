﻿Shader "Custom/BlinkableTileWithDepthTest" {
	Properties {
		_FillColor ("Color", Color) = (1,1,1,1)
		_FillColorAlpha ("Color Alpha", Float) = 1.0
	}
	SubShader {
		Tags { "Queue"="Transparent+2" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float4 _FillColor;
			uniform float _FillPercent;
			uniform float _FillColorAlpha;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.tex = input.texcoord;

				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float4 outColor = _FillColor;

				return float4(outColor.r, outColor.g, outColor.b, _FillColorAlpha);
			}

			ENDCG
		} 
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/TouchWave" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Percent ("Percent", Float) = 0.0
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float4 _Color;
			uniform float _Percent;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.tex = input.texcoord;

				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float2 c = (input.tex.xy - float2(0.5, 0.5)) / float2(0.5, 0.5);
				float radius = 0.2 + 0.8 * _Percent;

				float d = length(c);
			    float w = 0.01;
			    float circle = smoothstep(radius, radius - w, d);
				
			    float inner = radius - 0.03;
				circle -= smoothstep(inner, inner - w, d);
				
				float alpha = _Color.a * circle * smoothstep(1.0, 0.5, radius);

				return float4(_Color.r, _Color.g, _Color.b, alpha);
			}

			ENDCG
		} 
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/AvatarMask" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_AntiAliasingThreshold ("AA Threshold", Float) = 0.02
		_Radius ("Radius", Float) = 0.5
	}
	SubShader {
		Tags { "Queue"="Transparent+2" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			uniform float _AntiAliasingThreshold;
			uniform float _Radius;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

	        struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
	            vertexOutput output;
				output.tex = input.texcoord;
	
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float2 center = float2(0.5, 0.5);
				float2 c = (input.tex.xy - center) / center;

				float d = length(float2(max(0.0, abs(c.x) + _Radius - 1.0),
										max(0.0, abs(c.y) + _Radius - 1.0)
									   )
								);

				float r = smoothstep(_Radius, _Radius - _AntiAliasingThreshold, d);
				
				float4 outColor = tex2D(_MainTex, input.tex.xy).rgba;
				outColor.a = outColor.a * r;
				return outColor;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

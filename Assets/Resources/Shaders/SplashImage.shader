﻿Shader "Custom/SplashImage" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Fade ("Fade Strength", Float) = 1.0
	}
	SubShader {
		Tags { "Queue"="Transparent" }
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			uniform float _Fade;

			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

	        struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
	            vertexOutput output;
				output.tex = input.texcoord;
	
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
	 
            
			float4 frag(vertexOutput input) : COLOR
			{
				float4 outColor = tex2D(_MainTex, input.tex.xy).rgba;
				outColor.a = outColor.a * _Fade;
				return outColor;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

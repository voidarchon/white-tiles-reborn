﻿Shader "GUI/3D Text Shader" { 
	Properties {
		_MainTex ("Font Texture", 2D) = "white" {}
		_Color ("Text Color", Color) = (1,1,1,1)
	}
 
	SubShader {
		Tags { "Queue"="Transparent+2" }
		Lighting Off Cull Off ZWrite Off Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				uniform sampler2D _MainTex;
				uniform float4 _Color;

				struct vertexInput {
					float4 vertex : POSITION;
					float4 texcoord : TEXCOORD0;
				};

				struct vertexOutput {
					float4 pos : SV_POSITION;
					float4 tex : TEXCOORD0;
				};

				vertexOutput vert(vertexInput input)
				{
					vertexOutput output;
					output.tex = input.texcoord;

					output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
					return output;
				}
	 
            
				float4 frag(vertexOutput input) : COLOR
				{
					return float4(_Color.rgb, tex2D(_MainTex, input.tex.xy).a);
				}

				ENDCG
		}
	}
}
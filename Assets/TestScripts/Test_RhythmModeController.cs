﻿using UnityEngine;
using System.Collections;

public class Test_RhythmModeController : MonoBehaviour {

	public GameObject RhythmTileGO;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space))
			RhythmTileGO.SendMessage ("OnTapDown", -5.0f, SendMessageOptions.DontRequireReceiver);
		if (Input.GetKeyUp (KeyCode.Space))
			RhythmTileGO.SendMessage ("OnTapUp", -5.0f, SendMessageOptions.DontRequireReceiver);
	}
}

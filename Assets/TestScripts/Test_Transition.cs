﻿using UnityEngine;
using System.Collections;

public class Test_Transition : MonoBehaviour {

    public GameObject RootGO;

    private void TestFunc()
    {
        Object.Destroy(RootGO);
        Application.LoadLevelAdditive("_EndScene_ClassicMode_");
    }

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
        GetComponent<MeshRenderer>().material.SetColor("_Color", Color.black);
        GetComponent<SceneTransition>().DoneLeadInSelfDestruct = true;
        GetComponent<SceneTransition>().OnDoneLeadInListener = TestFunc;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

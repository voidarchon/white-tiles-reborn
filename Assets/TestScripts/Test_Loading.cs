﻿using UnityEngine;
using System.Collections;

public class Test_Loading : MonoBehaviour {

    public GameObject LoadingIndicatorGO;
    public IconRegionController IconGO;

    private void LeadIn()
    {
        LoadingIndicatorGO.SetActive(false);
        IconGO.ShiftUp(2.0f);
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            LoadingIndicatorGO.SetActive(true);
            Invoke("LeadIn", 3.0f);
        }
	}
}

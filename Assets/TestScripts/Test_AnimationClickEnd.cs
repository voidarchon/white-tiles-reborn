﻿using UnityEngine;
using System.Collections;

public class Test_AnimationClickEnd : MonoBehaviour {

    private Animator m_Animator;

    private void EndAnimation()
    {
        m_Animator.enabled = false;
    }

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_Animator.enabled = false;
            transform.position = new Vector3(0.0f, -3.0f, 0.0f);
        }
	}
}

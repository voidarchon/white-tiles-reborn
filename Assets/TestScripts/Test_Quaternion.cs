﻿using UnityEngine;
using System.Collections;

public class Test_Quaternion : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Quaternion q = Quaternion.FromToRotation(
        new Vector3(1.0f, 0.0f, 0.0f),
        new Vector3(2.0f, 2.0f, 0.0f));
        Debug.Log(q);
        transform.rotation = q;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TestCls : ICloneable
{
    public int A;
    public string B;

    public object Clone()
    {
        return new TestCls(A, B);
    }

    public TestCls(TestCls other)
    {
        A = other.A;
        B = other.B;
    }

    public TestCls(int a, string b)
    {
        A = a;
        B = b;
    }

    public void Print()
    {
        Debug.Log("A = " + A + " | B = " + B);
    }
}

public class Test_List : MonoBehaviour {

    private List<Vector3> m_vct;
    private List<TestCls> m_cls;

	// Use this for initialization
	void Start () {
        m_vct = new List<Vector3>(10);
        Vector3 v = new Vector3(0, 1, 2);
        m_vct.Add(v);
        Debug.Log(m_vct[0]);
        v.x = 10;
        Debug.Log(v);
        Debug.Log(m_vct[0]);

        m_cls = new List<TestCls>(10);
        TestCls cls = new TestCls(1, "one");
        m_cls.Add((TestCls)cls.Clone());
        m_cls[0].Print();
        cls.A = 10;
        m_cls[0].Print();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
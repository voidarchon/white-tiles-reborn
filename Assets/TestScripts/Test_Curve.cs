﻿using UnityEngine;
using System.Collections;

public class Test_Curve : MonoBehaviour {

    public AnimationCurve TheCurve;

	// Use this for initialization
	void Start () {
        Debug.Log(TheCurve.Evaluate(0.0f));
        Debug.Log(TheCurve.Evaluate(0.5f));
        Debug.Log(TheCurve.Evaluate(1.0f));
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(TheCurve.Evaluate(Time.time));
	}
}

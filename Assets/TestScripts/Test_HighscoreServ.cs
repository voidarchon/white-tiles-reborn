﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Test_HighscoreServ : MonoBehaviour {

    public string HighscoreDataPath;
    [TextArea(5, 10)]
    public string DefaultContent;
    public string Version;

	// Use this for initialization
	void Start () {
        HighscoreDataPath = Application.persistentDataPath + "/highscore.xml";
        HighscoreServ.Instance.Initialize(HighscoreDataPath, DefaultContent, Version);
	}

    private void DoneRequest(string returnJson, bool error, string errorName)
    {
        if (!error)
            Debug.Log(returnJson);
        else
        {
            Debug.Log(errorName);
        }
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            HighscoreServ.Instance.PushHistory("arcade", Random.Range(0.0f, 5.0f));
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("T");
            ServerCommunicator.Instance.DoRequest("http://5play.mobi:8888/leader_board_record-1.0/v0.3/score/topscore",
                LBJsonStringBuilder.Instance.BuildGetScoreStr("android", "-1", "Arcade Recent", "-1", "whitetiles", 15),
                DoneRequest);
        }
	}
}

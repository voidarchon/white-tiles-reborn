﻿using UnityEngine;
using System.Collections;

public class Test_KineticListView : MonoBehaviour {

    public GameObject PrefabItem;
    public UIKineticListView2 UIKineticListViewGO;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < 10; i++)
            {
                GameObject obj = Instantiate(PrefabItem) as GameObject;
                UIKineticListViewGO.AddItem(obj);
            }
            UIKineticListViewGO.Show();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            UIKineticListViewGO.SetEnable(false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            UIKineticListViewGO.SetEnable(true);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            UIKineticListViewGO.ResetView();
        }
	}
}

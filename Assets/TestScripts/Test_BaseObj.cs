﻿using UnityEngine;
using System.Collections;

public class Test_BaseObj : MonoBehaviour {

    private void TestFunc()
    {
        Debug.Log("Base.TestFunc()");
    }

    public void TestFunc2()
    {
        TestFunc();
    }

	// Use this for initialization
	void Start () {
        TestFunc2();
        // InvokeRepeating("TestFunc", 0.0f, 3.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
